/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unife.edge;

import com.clarkparsia.owlapiv3.OWL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import mpi.MPI;
import mpi.MPIException;
import mpi.Status;
import net.sf.javabdd.BDD;
import net.sf.javabdd.BDDFactory;
import net.sf.javabdd.BuDDyFactory;
import org.apache.log4j.Logger;
import org.mindswap.pellet.utils.Timer;
import org.mindswap.pellet.utils.Timers;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClassAssertionAxiom;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLException;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import unife.bundle.Bundle;
import unife.bundle.QueryResult;
import unife.bundle.logging.BundleLoggerFactory;
import unife.bundle.utilities.BundleUtilities;
import unife.edge.mpi.MPIUtilities;
import unife.edge.utilities.EDGEUtilities;

/**
 *
 * @author Riccardo Zese <riccardo.zese@unife.it>, Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class EDGE {

    /*
     * =*********************************= 
     * User arguments
     * =*********************************=
     */
    private int maxExplanations = Integer.MAX_VALUE;
    private String timeOut = null;
    private int BDDFStartNodeNum = 100;
    private int BDDFStartCache = 10000;
    private OWLOntology ontology = null;
    private OWLOntology probOntology = null;
    private String ontologyFileName = null;
    private String probOntologyFileName = null;
    private List<OWLAxiom> positiveExamples = null;
    private List<OWLAxiom> negativeExamples = null;
    private int seed = 0;
    private boolean randomize = false;
    private boolean randomizeAll = false;
    private char showAll = 's'; // 't' show all, 'f' show nothing, 's' ask user
    //private boolean learnParametersFromScratch = false;
    private double diffLL = 0.000000000028; // default value of diff between the old LL and new LL
    private double ratioLL = 0.000000000028; // default value of ratio, dependent on old LL and new LL
    private long maxIterations = 100; // default num of EM iterations
    private boolean merge = false;

    /*
     * =***********************************= 
     * Internal parameters
     * =***********************************=
     */
    final double LOGZERO = Math.log(0.000001);

    private boolean randomSeed = false;

    private int myRank = -1;
    
    private int slaves;
    
    /*
     * Used objects
     */
    private BDDFactory mgr;
    private static final Logger logger = Logger.getLogger(EDGE.class.getName(), new BundleLoggerFactory());
    private List<String> rulesNameString;
    private boolean[] usedAxioms;           // assiomi usati
    private List<List<Integer>> vars_ex;    // Lista di liste di OWLAxiom (per ogni esempio tengo corrispondenza tra indice variabile e assioma, per ogni esempio tengo le variabili)
    private List<Integer> boolVars_ex;      // numero di variabili per ogni esempio
    private List<List<Double>> probs_ex;    // per ogni esempio, per ogni variabile, probabilità
    private List<OWLAxiom> rulesName;
    private List<Double> arrayProb;
    int numberOfExamples;
    int[] numberOfExamples4Slaves;
    //List<double[]> eta;
    //private List<Double> nodes_probs_ex; 

    //double[] example_prob;
    private OWLOntologyManager owlManager;
    private int[] queryTags;

    public EDGE() {
        owlManager = OWLManager.createOWLOntologyManager();
        
        try {
            myRank = MPI.COMM_WORLD.getRank();
            slaves = MPI.COMM_WORLD.getSize() - 1;
        } catch (MPIException ex) {
            System.err.println("Impossible to define process rank: " + ex.getMessage());
            logger.error("Impossible to define process rank: " + ex.getMessage());
            System.exit(-3);
        }
    }

    public EDGEStat computeLearning() {
        
        if (myRank == 0) {
            return master();
        } else {
            return slave();
        }
        
    }
    
    private EDGEStat master() {
        Timers timers = new Timers();

        // serve al master??
        //List<BDD> BDDs = new ArrayList<>();

        numberOfExamples = getPositiveExamples().size() + getNegativeExamples().size();
        double[] probs = new double[numberOfExamples];

        // creation of probability map?? Serve al master??
        logger.debug("Preparing Probability map...");
        Timer timePMap = timers.startTimer("PMap");
        Map<OWLAxiom, List<Double>> PMap = preparePMap(timePMap);
        timePMap.stop();
        logger.debug("Probability Map computed");

        // Inizializzazione
        logger.debug("Initializing...");
        init();
        logger.debug("Initialization completed");

        //serve al master??
        mgr = BuDDyFactory.init(BDDFStartNodeNum, BDDFStartCache);

        logger.info("Start finding explanations for every example...");
        Timer timeBundle = timers.startTimer("Bundle");

        // Invio esempi in blocchi
        sendExamples();
        // ricevo i risultati
        receiveExamplesResults(probs);

        timeBundle.stop();

        logger.info("Start EM Algorithm\n\t- n. of probabilistic axioms:\t" + rulesName.size()
                + "\n\t- n. of examples:\t\t" + (numberOfExamples));

        Timer timeEM = timers.startTimer("EM");
        
        // SIncronizzo con gli slaves???
        MPIUtilities.sendSignal(MPIUtilities.TO_ALL, MPIUtilities.STOP);
        // Expectation-Maximization algorithm
        EDGEStatImpl result = EMMaster(probs);

//        for (int idxBDD = 0; idxBDD < BDDs.size(); idxBDD++) {
//            BDDs.get(idxBDD).free();
//            BDDs.set(idxBDD, null);
//        }
//        BDDs.clear();

        timeEM.stop();

        // alla fine di tutto l'ontologia contenente gli assiomi probabilistici 
        // target, ossia probOntology, deve essere modificata, in maniera tale
        // da aggiornare i parametri dei suoi assiomi probabilistici
        printLearningResult(timers);
        

        result.setTimers(timers.getTimers());

        return result;
    }
    
    private EDGEStat slave() {
        //Timers timers = new Timers();

        List<BDD> BDDs = new ArrayList<>();

        numberOfExamples = getPositiveExamples().size() + getNegativeExamples().size();
        double[] probs = new double[numberOfExamples];

        // creation of probability map
        logger.debug("Preparing Pobability map...");
        //Timer timePMap = timers.startTimer("PMap");
        Map<OWLAxiom, List<Double>> PMap = preparePMap(null);
        //timePMap.stop();
        logger.debug("Probability Map computed");

        logger.debug("Initializing...");
        init();
        logger.debug("Initialization completed");

        mgr = BuDDyFactory.init(BDDFStartNodeNum, BDDFStartCache);

        logger.info("Start finding explanations for every example...");
        //Timer timeBundle = timers.startTimer("Bundle");

        receiveExamples();
        computeExamples(PMap, BDDs, probs);
        sendExamplesResults(probs);

        //timeBundle.stop();

        logger.info("Start EM Algorithm\n\t- n. of probabilistic axioms:\t" + rulesName.size()
                + "\n\t- n. of examples:\t\t" + (numberOfExamples));

        //Timer timeEM = timers.startTimer("EM");
        // Expectation-Maximization algorithm
        EDGEStatImpl result = EMSlave(BDDs, probs);

        for (int idxBDD = 0; idxBDD < BDDs.size(); idxBDD++) {
            BDDs.get(idxBDD).free();
            BDDs.set(idxBDD, null);
        }
        BDDs.clear();

        //timeEM.stop();

        // alla fine di tutto l'ontologia contenente gli assiomi probabilistici 
        // target, ossia probOntology, deve essere modificata, in maniera tale
        // da aggiornare i parametri dei suoi assiomi probabilistici
        //printLearningResult(timers);
        

        //result.setTimers(timers.getTimers());

        return result;
    }

    protected void computeExamples(Map<OWLAxiom, List<Double>> PMap, List<BDD> BDDs, double[] probs) {
        computeExamples(PMap, BDDs, probs, false);
    }
    
    protected void computeExamples(Map<OWLAxiom, List<Double>> PMap, List<BDD> BDDs, double[] probs, boolean test) {
        int index = 0;
        // settare PMap in bundle???
        for (OWLAxiom query : getPositiveExamples()) {
            logger.info("Query " + (index + 1) + " of " + numberOfExamples + " (" + (int) (((index + 1) * 100) / numberOfExamples) + "%)");
            logger.info("Positive Example: " + query);

            // Creare qui un'istanza di BUNDLE o crearla prima e qui semplicemente settarla???BundleDeprecate       // setting di Bundle
//            bundle.setBddF(this.mgr);
//            bundle.setMaxExplanations(this.maxExplanations);
//            bundle.setMaxTime(this.timeOut);
//            bundle.setPMap(PMap);
//            // bisogna dare a bundle inoltre la Pmap, l'ontologia non probabilistica e l'ontologia BundleDeprecateilistica 
            // clear Bundle
            Bundle bundle = startBundle();
            bundle.setPMap(PMap);
           
            // get Bundle results
            QueryResult queryResult = null;
            try {
                queryResult = bundle.computeQuery(query);
            } catch (OWLException owle) {
                logger.error("It was impossible to compute the probability of the query");
                logger.error(owle.getMessage());
                logger.trace(owle.getStackTrace());
                
                System.err.println("Terminated because of an internal error. See the log for details");
                throw new RuntimeException(owle);
            }

            if (queryResult == null) {
                logger.fatal("The result of BUNDLE algorithm is null");
                // throw an exception;
                //throw;
                throw new RuntimeException("Impossible to compute the query");
            }
            if (!test) {
                // get the BDD of the query/example
                BDDs.add(queryResult.getBDD());

                init_bdd(queryResult);
            }

            probs[index] = queryResult.getQueryProbability();
            index++;

            bundle.finish();
            bundle = null;

        }
        for (OWLAxiom query : getNegativeExamples()) {
            OWLAxiom notQuery = OWL.classAssertion(((OWLClassAssertionAxiom) query).getIndividual(),
                    OWL.not(((OWLClassAssertionAxiom) query).getClassExpression()));
            logger.info("Query " + (index + 1) + " of " + numberOfExamples + " (" + (int) (((index + 1) * 100) / numberOfExamples) + "%)");
            logger.info("Negative Example: " + notQuery);

            // Creare qui un'istanza di BUNDLE o crearla prima e quBundleDeprecatelicemente sBundleDeprecatea???
            // setting di Bundle
            // clear Bundle
            Bundle bundle = startBundle();
            bundle.setPMap(PMap);
            
            // get Bundle results
            QueryResult queryResult = null;
            try {
                queryResult = bundle.computeQuery(notQuery);
            } catch (OWLException owle) {
                logger.fatal("It was impossible to compute the probability of the query");
                logger.fatal(owle.getMessage());
                logger.trace(owle.getStackTrace());
                
                System.err.println("Terminated because of an internal error. See the log for details");
                throw new RuntimeException(owle);
            }

            if (queryResult != null && !queryResult.getExplanations().isEmpty()) {
                if (!test)
                    // get the BDD of the query/example
                    BDDs.add(queryResult.getBDD());
                probs[index] = queryResult.getQueryProbability();
            } else {
                logger.info("Trying the second method...");
                try {
                    queryResult = bundle.computeQuery(query);
                } catch (OWLException owle) {
                    logger.fatal("It was impossible to compute the probability of the query");
                    logger.fatal(owle.getMessage());
                    logger.trace(owle.getStackTrace());
                    
                    System.err.println("Terminated because of an internal error. See the log for details");
                    throw new RuntimeException(owle);
                }

                if (queryResult == null) {
                    logger.fatal("The result of BUNDLE algorithm is null");
                    throw new RuntimeException("Impossible to compute the query");
                }
                if (!test)
                    BDDs.add(queryResult.getBDD().not());
                probs[index] = 1 - queryResult.getQueryProbability();
            }

            if (!test)
                init_bdd(queryResult);

            index++;

            bundle.finish();
            bundle = null;

        }
    }

    private Bundle startBundle(){
        
        Bundle bundle = new Bundle();
            bundle.setBddF(this.mgr);
            bundle.setMaxExplanations(this.getMaxExplanations());
            bundle.setMaxTime(this.timeOut);
            bundle.setLog(true);
            bundle.loadOntologies(ontologyFileName);
            //bundle.loadOntologies(ontology);
            return bundle;        
    }
    
    public OWLOntology getLearnedOntology() {
        // da debuggare e testare
        
        logger.info("");
        logger.info("Creation of the learned ontology...");
        
        Timers timers = new Timers();
        Timer timer = timers.createTimer("OntologyCreation");
        
        timer.start();
        OWLOntology resultOntology = null;
        try {
            Set<OWLAxiom> ontologyAxioms;
            OWLDataFactory owlFactory = owlManager.getOWLDataFactory();
            if (merge) {
                //OWLOntology ontology = owlManager.createOntology(IRI.create(ontologyFileName));
                if (probOntology != null)
                    owlManager.removeOntology(probOntology);
                
                // è cosa buona e giusta duplicare l'ontologia? Occupa memoria
                resultOntology = BundleUtilities.copyOntology(ontology);
                ontologyAxioms = resultOntology.getAxioms();
                //owlManager.removeOntology(ontology);
            } else {
                ontologyAxioms = getProbOntology().getAxioms();
                owlManager.removeOntology(probOntology);
                resultOntology = owlManager.createOntology(getProbOntology().getOntologyID());

            }

            //Set<OWLAxiom> filteredAxioms = EDGEUtilities.get_ax_filtered(probOntology);
            for (OWLAxiom ax : ontologyAxioms) {

                for (int i = 0; i < rulesName.size(); i++) {
                    OWLAxiom pax = rulesName.get(i);

                    //i++;
                    if (pax.equalsIgnoreAnnotations(ax)) {
                        Set<OWLAnnotation> axAnnotations = new HashSet(ax.getAnnotations());
                        if (ax.getAnnotations(BundleUtilities.PROBABILISTIC_ANNOTATION_PROPERTY).size() > 0) {
                            for (OWLAnnotation ann : axAnnotations) {
                                if (ann.getProperty().equals(BundleUtilities.PROBABILISTIC_ANNOTATION_PROPERTY)) {
                                    axAnnotations.remove(ann);
                                    break;
                                }
                            }
                        }
                        if (merge) {
                            owlManager.removeAxiom(resultOntology, ax);
                        }
                        axAnnotations.add(owlFactory.getOWLAnnotation(BundleUtilities.PROBABILISTIC_ANNOTATION_PROPERTY, owlFactory.getOWLLiteral(arrayProb.get(i))));
                        owlManager.addAxiom(resultOntology, pax.getAnnotatedAxiom(axAnnotations));
                        //ontologyAxioms.remove(pax);
                        break;
                    }
                }
            }

        } catch (OWLOntologyCreationException ex) {
            logger.error("It was impossible to creating the resulting ontology");
            logger.error(ex.getMessage());
            logger.trace(ex.getStackTrace());
            // lanciare un'eccezione
            System.err.println("Unsuccessful creation of the learned ontology. See the log for details");
            return null;
        }

        timer.stop();
        
        logger.info("Successful creation of the learned ontology");
        logger.info("Ontology created in "+ timer.getElapsed() + " (ms)");
        
        return resultOntology;

    }

    /**
     * Initializes the PMap to use for find the explanations.
     *
     * @param input the input ontology
     * @param randomize true if randomize option is set
     * @param randomizeAll true if randomizeAll option is set
     * @param randomSeed true if the seed is given in input
     * @param seed the random seed if set
     * @return the PMap containing the probabilities of the probabilistic axioms
     * of the input ontology
     */
    private Map<OWLAxiom, List<Double>> preparePMap(Timer timer) {

        rulesName = new ArrayList<>();
        rulesNameString = new ArrayList<>();
        arrayProb = new ArrayList<>();

        Map<OWLAxiom, List<Double>> PMap = new HashMap<>();

        Random probGenerator = new Random();

        if (randomize) {

            if (randomSeed) {
                probGenerator.setSeed(seed);
                logger.debug("Random Seed setted to: " + seed);
            }
        }

        // SET ONTOLOGY OR ALREADY SET??????
        try {
            if (probOntologyFileName == null) {

                //probOntology = owlManager.loadOntologyFromOntologyDocument(IRI.create(ontologyFileName));

            } else {
                probOntology = owlManager.loadOntologyFromOntologyDocument(IRI.create(probOntologyFileName));
            }
        } catch (OWLOntologyCreationException ex) {
            logger.fatal("It was impossible to create the map of the probabilistic axioms");
            logger.fatal(ex.getMessage());
            logger.trace(ex.getStackTrace());

            System.err.println("Terminated because of an internal error. See the log for details");
            throw new RuntimeException(ex);
        }
        List<OWLAxiom> axioms;
        if (probOntology != null) {
            axioms = EDGEUtilities.get_listOf_ax_filtered(probOntology);
        } else {
            axioms = EDGEUtilities.get_listOf_ax_filtered(BundleUtilities.getProbabilisticAxioms(ontology));
        }

        boolean show = true;
        if (showAll != 'f' && !randomizeAll) {
            if (axioms.size() > 100) {
                if (timer != null) {
                    timer.stop();
                }
                if (showAll == 's') {
                    System.out.print("Number of probabilistic axiom greater then 100 (" + axioms.size() + "), do you want to see them all? ('y' or 'n'): ");
                    logger.info("Number of probabilistic axiom greater then 100 (" + axioms.size() + "). Waiting for user's answer...");
                    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                    try {
                        String in = br.readLine();
                        if (in.charAt(0) == 'n') {
                            logger.debug("User's choice is 'n'");
                            show = false;
                        } else if (in.charAt(0) != 'y') {
                            System.out.println("Wrong answer! I will not show the results!");
                            logger.info("Wrong answer! I will not show the results!");
                            show = false;
                        } else {
                            logger.debug("User's choice is 'y'");
                        }
                    } catch (Exception ex) {
                        System.err.println("Error in answer! I will not show the results!");
                        logger.error("Error in answer! I will not show the results!");
                        show = false;
                    }
                    if (timer != null) {
                        timer.start();
                    }
                }
            }
        }
        if (showAll == 'f') {
            show = false;
        }

        for (OWLAxiom axiom : axioms)//prob
        {
            List<Double> probList = new ArrayList<>();
            String axiomName = BundleUtilities.getManchesterSyntaxString(axiom);

            if (randomize) {

                if (randomizeAll) {
                    //System.out.print(axiomName);
                    probList.add(probGenerator.nextDouble());
                    //System.out.println(" => " + generatedProb);
                } else {// Randomize
                    for (OWLAnnotation annotation : axiom.getAnnotations()) {

                        if (annotation.getValue() != null) {

                            if (annotation.getProperty().equals(BundleUtilities.PROBABILISTIC_ANNOTATION_PROPERTY)) {

                                double generatedProb = probGenerator.nextDouble();

                                probList.add(probGenerator.nextDouble());

                                if (show) {
                                    System.out.print(axiomName);
                                    System.out.println(" => " + generatedProb);
                                }
                            }
                        }
                    }
                }
            } else {
                for (OWLAnnotation annotation : axiom.getAnnotations()) {

                    // metodo per aggiungere coppia assioma/probabilita alla Map
                    if (annotation.getValue() != null) {

                        if (annotation.getProperty().equals(BundleUtilities.PROBABILISTIC_ANNOTATION_PROPERTY)) {
                            if (show) {
                                System.out.print(axiomName);
                            }

                            String annotazione = annotation.getValue().toString();
                            String annotazione1 = annotazione.replaceAll("\"", "");
                            if (annotazione1.contains("^^")) {
                                annotazione1 = annotazione1.substring(0, annotazione1.indexOf("^^"));
                            }

                            Double annProbability = Double.valueOf(annotazione1);
                            probList.add(annProbability);
                            if (show) {
                                System.out.println(" => epistemic (" + annProbability + ")");
                            }

                        }
                    }
                }
            }
            if (probList.size() > 0) {
                if (PMap.containsKey(axiom)) {
                    List<Double> varProbTemp = PMap.get(axiom);
                    varProbTemp.addAll(probList);

                } else {
                    PMap.put(axiom.getAxiomWithoutAnnotations(), probList);
                    rulesName.add(axiom.getAxiomWithoutAnnotations());
                    rulesNameString.add(axiomName);
                    arrayProb.add(EDGEUtilities.avgProbs(probList));
                }

            }

        }

        if (randomizeAll) {
            System.out.println("Created " + axioms.size() + " probabilistic axiom");
        }

        if (PMap.size() > 0) {
            return PMap;
        }
        return null;
    }

    /*
     * ************************************
     * Setters and getters 
     * ************************************
     */
    /**
     * @param maxExplanations the maxExplanations to set
     */
    public void setMaxExplanations(int maxExplanations) {
        this.maxExplanations = maxExplanations;
    }

    /**
     * @param timeOut the timeOut to set
     */
    public void setTimeOut(String timeOut) {
        this.timeOut = timeOut;
    }

//    public void setTimeOut(int timeOut) {
//        bundle.setMaxTime(timeOut);
//    }
    /**
     * @param BDDFStartNodeNum the BDDFStartNodeNum to set
     */
    public void setBDDFStartNodeNum(int BDDFStartNodeNum) {
        this.BDDFStartNodeNum = BDDFStartNodeNum;
    }

    /**
     * @param BDDFStartCache the BDDFStartCache to set
     */
    public void setBDDFStartCache(int BDDFStartCache) {
        this.BDDFStartCache = BDDFStartCache;
    }

    /**
     * @param ontology the ontology to set
     */
    public void setOntology(OWLOntology ontology) {
        this.ontology = ontology;
    }

    /**
     * @param ontology the ontology to set, if the probabilistic ontology is not
     * setted yet, this method set it to ontology
     */
    public void setOntologies(OWLOntology ontology) {
        this.ontology = ontology;
        if (getProbOntology() == null) {
            probOntology = ontology;
        }
    }

    /**
     * @param probOntology the probOntology to set
     */
    public void setProbOntology(OWLOntology probOntology) {
        this.probOntology = probOntology;
    }

    /**
     * @param positiveExamples the positiveExamples to set
     */
    public void setPositiveExamples(List<OWLAxiom> positiveExamples) {
        this.positiveExamples = positiveExamples;
    }

    /**
     * @param negativeExamples the negativeExamples to set
     */
    public void setNegativeExamples(List<OWLAxiom> negativeExamples) {
        this.negativeExamples = negativeExamples;
    }

    /**
     * @param seed the seed to set
     */
    public void setSeed(int seed) {
        this.seed = seed;
        randomSeed = true;
    }

    /**
     * @param randomize the randomize to set
     */
    public void setRandomize(boolean randomize) {
        this.randomize = randomize;
    }

    /**
     * @param randomizeAll the randomizeAll to set
     */
    public void setRandomizeAll(boolean randomizeAll) {
        this.randomizeAll = randomizeAll;
    }

    /**
     * @param showAll the showAll to set
     */
    public void setShowAll(boolean showAll) {
        if (showAll) {
            this.showAll = 't';
        } else {
            this.showAll = 'f';
        }
    }

    /**
     * @param showAll the showAll to set
     */
    public void setShowAll(char showAll) {
        this.showAll = showAll;
    }

    /**
     * @param learnParametersFromScratch the ldiffLLrnParametratioLLsFromScratch
     * to set
     */
//    public void setLearnParametersFromScratch(boolean learnParametersFromScratch) {
//        this.learnParametersFromScratch = learnParametersFromScratch;
//    }
    /**
     * @param ea the diffLL to set
     */
    public void setDiffLL(double ea) {
        this.diffLL = ea;
    }

    /**
     * @param er the ratioLL to set
     */
    public void setRatioLL(double er) {
        this.ratioLL = er;
    }

    private void init_bdd(QueryResult queryResult) {
        // Insert in vars_ex the index of the axioms used in the current example 
        // ans set to true the corresponding boolean.
        List<Integer> to_add = new ArrayList<Integer>();
        for (int i = 0; i < queryResult.getProbAxioms().size(); i++) {
            int pos = rulesNameString.indexOf(BundleUtilities.getManchesterSyntaxString(queryResult.getProbAxioms().get(i)));

            // if (!usedAxioms[pos])
            usedAxioms[pos] = true;

            to_add.add(pos);

        }
//        for (int i = 0; i < axioms.size(); i++){
//            to_add.add(rulesName.indexOf(axioms.get(i)));
//        }
        vars_ex.add(to_add);
        to_add = null;

        //nVars_ex.add(nAx);
        boolVars_ex.add(queryResult.getProbAxioms().size());

        probs_ex.add(queryResult.getProbOfAxioms());

        //nodes_probs_ex.add(0.0);
    }
    
    public void computeTest() {
        Timers timers = new Timers();

        List<BDD> BDDs = new ArrayList<>();

        numberOfExamples = getPositiveExamples().size() + getNegativeExamples().size();
        double[] probs = new double[numberOfExamples];

        // creation of probability map
        logger.debug("Preparing Pobability map...");
        Timer timePMap = timers.startTimer("PMap");
        Map<OWLAxiom, List<Double>> PMap = preparePMap(timePMap);
        timePMap.stop();
        logger.debug("Probability Map computed");

//        logger.debug("Initializing...");
//        init();
//        logger.debug("Initialization completed");

        mgr = BuDDyFactory.init(BDDFStartNodeNum, BDDFStartCache);

        logger.info("Start test\n\t- n. of queries:\t\t" + (numberOfExamples));
        logger.info("");
        logger.info("");
        
        Timer timeBundle = timers.startTimer("Test");

        computeExamples(PMap, BDDs, probs, true);

        timeBundle.stop();

        // alla fine di tutto l'ontologia contenente gli assiomi probabilistici 
        // target, ossia probOntology, deve essere modificata, in maniera tale
        // da aggiornare i parametri dei suoi assiomi probabilistici
        
        logger.info("");
        logger.info("============ Result ============");
        logger.info("");
        
        int index = 0;
        for (OWLAxiom ax : getPositiveExamples()){

            logger.info("Query " + (index + 1) + " of " + numberOfExamples 
                    + " (" + (int)(((index + 1) * 100) / numberOfExamples) + "%)");
            logger.info("Positive Example: " + ax);
            logger.info("Prob: " + probs[index]);
            index++;
        }
        for (OWLAxiom ax : getNegativeExamples()){

            logger.info("Query " + (index + 1) + " of " + numberOfExamples
                    + " (" + (int)(((index + 1) * 100) / numberOfExamples) + "%)");
            logger.info("Negative Example: " + ax);
            logger.info("Prob: " + probs[index]);
            index++;
        }
        
        System.out.println();

        logger.info("");
        logger.info("================================");
        logger.info("");
        StringWriter sw = new StringWriter();
        timers.print( sw, true, null );
        logger.info(sw);
     
    }

    private void printLearningResult(Timers timers) {
        String axiom;

        int nRules = rulesName.size();
        boolean show = true;
        if (showAll != 'f') {
            if (nRules > 100) {
                if (showAll == 's') {
                    System.out.print("Number of probabilistic axiom greater then 100 (" + nRules + "), do you want to see them all? ('y' or 'n'): ");
                    logger.info("Number of probabilistic axiom greater then 100 (" + nRules + "). Waiting for user's answer...");
                    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                    try {
                        String in = br.readLine();
                        if (in.charAt(0) == 'n') {
                            logger.debug("User's choice is 'n'");
                            show = false;
                        } else if (in.charAt(0) != 'y') {
                            System.out.println("Wrong answer! I will not show the results!");
                            logger.info("Wrong answer! I will not show the results!");
                            show = false;
                        } else {
                            logger.debug("User's choice is 'y'");
                        }
                    } catch (Exception ex) {
                        System.err.println("Error in answer! I will not show the results!");
                        logger.error("Error in answer! I will not show the results!");
                        show = false;
                    }
                }

            }
        } else {
            show = false;
        }

        if (show) {
            logger.info("");
            logger.info("============ Result ============");
            logger.info("");
            
            for (int j = 0; j < nRules; j++) {
//                    if (arrayprob.get(j) > 0.0){

                axiom = rulesNameString.get(j).toString();
                logger.info("ax " + (j + 1) + "/" + nRules + " : " + axiom + "\tarrayprob: " + arrayProb.get(j) + "\n");
//                    }
            }
            logger.info("");
            logger.info("================================");
        
        }
        
        logger.info("");
        StringWriter sw = new StringWriter();
        timers.print( sw, true, null );
        logger.info(sw);
    }

    private void init() {

        vars_ex = new ArrayList<>(); // Le variabili per esempio

        int nRules = rulesName.size();
        usedAxioms = new boolean[nRules];

        probs_ex = new ArrayList<>();
        boolVars_ex = new ArrayList<>(); // corrisponde a nVars_ex

        for (int j = 0; j < nRules; j++) {
            usedAxioms[j] = false;

        }

    }

    private EDGEStatImpl EMMaster(double[] probs) {

        int lenNodes = probs.length, nRules = rulesName.size();
        double CLL0 = -2.2 * Math.pow(10, 10);  // -inf
        double CLL1 = -1.7 * Math.pow(10, 8);    // +inf
        double ratio, diff;
        int cycle = 0;
        boolean runFlag = true;

        List<double[]> example_weight = new ArrayList<>();

        List<double[]> eta = new ArrayList<double[]>();
        for (int i = 0; i < nRules; i++) {
            double[] eta_int_temp = new double[2];
            eta_int_temp[0] = 0.0;
            eta_int_temp[1] = 0.0;
            eta.add(eta_int_temp);

        }
        

        //vettore di BDD (nodi radice) e sua lunghezza
        for (int j = 0; j < slaves; j++) {
            double[] example_weightInt = new double[lenNodes];
            for (int i = 0; i < numberOfExamples4Slaves[j]; i++) {
                example_weightInt[i] = 1;
            }
            example_weight.add(example_weightInt);
        }
        //System.arraycopy(example_prob_calc, 0, example_prob, 0, lenNodes);

        diff = CLL1 - CLL0;
        ratio = diff / Math.abs(CLL0);

        while (true) {
            cycle++;

            CLL0 = CLL1;
            CLL1 = expectationMaster(example_weight, eta, lenNodes, (cycle == 0));
            maximizationMaster(eta);
            diff = CLL1 - CLL0;
            ratio = diff / Math.abs(CLL0);

            //System.out.println("Log-Likelyhood ciclo " + cycle + " : " + CLL1);
            if ((diff > diffLL) && (ratio > ratioLL) && (cycle < getMaxIterations()))
                MPIUtilities.sendSignal(MPIUtilities.TO_ALL, MPIUtilities.CONTINUE);
            else {
                MPIUtilities.sendSignal(MPIUtilities.TO_ALL, MPIUtilities.STOP);
                break;
            }
        }

        logger.info("\n  Final Log-Likelihood: " + CLL1);

        return new EDGEStatImpl(cycle, CLL1, rulesName, arrayProb);

    }
    
    
    private EDGEStatImpl EMSlave(List<BDD> BDDs, double[] probs) {

        int lenNodes = BDDs.size(), nRules = rulesName.size();
//        double CLL0 = -2.2 * Math.pow(10, 10);  // -inf
//        double CLL1 = -1.7 * Math.pow(10, 8);    // +inf
//        double ratio, diff;
//        int cycle = 0;

        double[] example_weight;

        List<double[]> eta = new ArrayList<double[]>();
        for (int i = 0; i < nRules; i++) {
            double[] eta_int_temp = new double[2];
            eta_int_temp[0] = 0.0;
            eta_int_temp[1] = 0.0;
            eta.add(eta_int_temp);

        }

//        //vettore di BDD (nodi radice) e sua lunghezza
//        example_weight = new double[lenNodes];
//        for (int i = 0; i < lenNodes; i++) {
//            example_weight[i] = 1;
//        }
        //System.arraycopy(example_prob_calc, 0, example_prob, 0, lenNodes);

//        diff = CLL1 - CLL0;
//        ratio = diff / Math.abs(CLL0);

        boolean runFlag = true;
        do {
            expectationSlave(BDDs, eta, lenNodes);
            
            int[] signal = new int[1];
            try {
                MPI.COMM_WORLD.recv(signal, 1, MPI.INT, 0, 0);
            } catch (MPIException ex) {
                java.util.logging.Logger.getLogger(EDGE.class.getName()).log(Level.SEVERE, null, ex);
            }
            runFlag = (signal[0] == 1);
            //System.out.println("Log-Likelyhood ciclo " + cycle + " : " + CLL1);
        } while(runFlag);

        //logger.info("\n  Final Log-Likelihood: " + CLL1);

        //TO FIX
        return new EDGEStatImpl(1, 1, rulesName, arrayProb);

    }
    
    private EDGEStatImpl EM(List<BDD> BDDs, double[] probs) {

        int lenNodes = BDDs.size(), nRules = rulesName.size();
        double CLL0 = -2.2 * Math.pow(10, 10);  // -inf
        double CLL1 = -1.7 * Math.pow(10, 8);    // +inf
        double ratio, diff;
        int cycle = 0;

        double[] example_weight;

        List<double[]> eta = new ArrayList<double[]>();
        for (int i = 0; i < nRules; i++) {
            double[] eta_int_temp = new double[2];
            eta_int_temp[0] = 0.0;
            eta_int_temp[1] = 0.0;
            eta.add(eta_int_temp);

        }

        //vettore di BDD (nodi radice) e sua lunghezza
        example_weight = new double[lenNodes];
        for (int i = 0; i < lenNodes; i++) {
            example_weight[i] = 1;
        }
        //System.arraycopy(example_prob_calc, 0, example_prob, 0, lenNodes);

        diff = CLL1 - CLL0;
        ratio = diff / Math.abs(CLL0);

        while ((diff > diffLL) && (ratio > ratioLL) && (cycle < getMaxIterations())) {
            cycle++;

            CLL0 = CLL1;
            CLL1 = expectation(BDDs, example_weight, eta, lenNodes);
            maximization(eta);
            diff = CLL1 - CLL0;
            ratio = diff / Math.abs(CLL0);

            //System.out.println("Log-Likelyhood ciclo " + cycle + " : " + CLL1);
        }

        logger.info("\n  Final Log-Likelihood: " + CLL1);

        return new EDGEStatImpl(cycle, CLL1, rulesName, arrayProb);

    }

    /**
     * Calculates the expectation of a list of examples.
     *
     * @param nodes_ex a list of BDD corresponding to the examples
     * @param lenNodes number of BDD contained in nodes_ex
     * @return the calculated Log-Likelihood
     */
    private double expectation(List<BDD> nodes_ex, double[] example_prob,
            List<double[]> eta, int lenNodes) {
        double rootProb, CLL = 0;

        for (int i = 0; i < lenNodes; i++) {
            if (!(nodes_ex.get(i).isOne() || nodes_ex.get(i).isZero())) {

                EMTable nodesB = new EMTable(boolVars_ex.get(i));
                EMTable nodesF = new EMTable(boolVars_ex.get(i));

                forward(nodes_ex.get(i), nodesF, i);

                rootProb = getOutsideExpe(nodes_ex.get(i), example_prob[i], nodesF, nodesB, eta, i);

                if (rootProb <= 0.0) {
                    System.out.println("Example " + i + ": zero or negative probability -> " + rootProb + "\n");
                    CLL = CLL + LOGZERO * example_prob[i];
                } else {
                    CLL = CLL + Math.log(rootProb) * example_prob[i];
                }

                //nodes_probs_ex.set(i, rootProb);
                // destroy_table
                nodesB.clear();
                nodesF.clear();
                nodesB = null;
                nodesF = null;
            } else {
                if (nodes_ex.get(i).isZero()) {
                    CLL = CLL + LOGZERO * example_prob[i];
                }
            }
        }

        return CLL;
    }
    
    private double expectationSlave(List<BDD> nodes_ex, 
            List<double[]> eta, int lenNodes) {
        double rootProb, CLL = 0;
        
        //TO READ FROM MASTER
        double[] example_prob = new double[numberOfExamples];
        try {
            MPI.COMM_WORLD.recv(example_prob, numberOfExamples, MPI.DOUBLE, 0, 0);
        } catch (MPIException ex) {
            java.util.logging.Logger.getLogger(EDGE.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (int i = 0; i < lenNodes; i++) {
            if (!(nodes_ex.get(i).isOne() || nodes_ex.get(i).isZero())) {

                EMTable nodesB = new EMTable(boolVars_ex.get(i));
                EMTable nodesF = new EMTable(boolVars_ex.get(i));

                forward(nodes_ex.get(i), nodesF, i);

                rootProb = getOutsideExpe(nodes_ex.get(i), example_prob[i], nodesF, nodesB, eta, i);

                if (rootProb <= 0.0) {
                    System.out.println("Example " + i + ": zero or negative probability -> " + rootProb + "\n");
                    CLL = CLL + LOGZERO * example_prob[i];
                } else {
                    CLL = CLL + Math.log(rootProb) * example_prob[i];
                }

                //nodes_probs_ex.set(i, rootProb);
                // destroy_table
                nodesB.clear();
                nodesF.clear();
                nodesB = null;
                nodesF = null;
            } else {
                if (nodes_ex.get(i).isZero()) {
                    CLL = CLL + LOGZERO * example_prob[i];
                }
            }
        }
        
        double[] doubleBuffer = new double[(2 * numberOfExamples) + 1];
        
        for (int i = 0, idxDBuff = 0; i < numberOfExamples; i++, idxDBuff++) {
            doubleBuffer[idxDBuff] = eta.get(i)[0];
            idxDBuff++;
            doubleBuffer[idxDBuff] = eta.get(i)[1];            
        }
        doubleBuffer[(2 * numberOfExamples) + 1] = CLL;
        try {
            MPI.COMM_WORLD.send(doubleBuffer, (2 * numberOfExamples) + 1, MPI.DOUBLE, 0, 0);
        } catch (MPIException ex) {
            java.util.logging.Logger.getLogger(EDGE.class.getName()).log(Level.SEVERE, null, ex);
        }

        return CLL;
    }

    /**
     * Calculates the forward probabilities of nodes of a BDD. Builds the nodesF
     * tables containing the forward probabilities of all nodes of the given BDD
     *
     * @param root the root of a BDD
     * @param nEx number of the current example
     */
    private void forward(BDD root, EMTable nodesF, int nEx) {

        if (boolVars_ex.get(nEx) > 0) {
            List<List<BDD>> nodesToVisit = new ArrayList<List<BDD>>(); // da sostituire con clear ?????
            List<Integer> NnodesToVisit = new ArrayList<Integer>();
            List<BDD> to_add = new ArrayList<BDD>();
            to_add.add(root);
            nodesToVisit.add(to_add);
            NnodesToVisit.add(1);

            for (int i = 1; i < boolVars_ex.get(nEx); i++) {
                nodesToVisit.add(new ArrayList<BDD>());
                NnodesToVisit.add(0);
            }

            nodesF.add_node(root, 1);

            for (int i = 0; i < boolVars_ex.get(nEx); i++) {
                for (int j = 0; j < NnodesToVisit.get(i); j++) {
                    updateForward(nodesToVisit, NnodesToVisit, i, j, nodesF, nEx);
                }
            }

            for (List<BDD> ithList : nodesToVisit) {
                for (int j = 0; j < ithList.size(); j++) {
                    ithList.set(j, null);
                }
            }
            nodesToVisit.clear();
            nodesToVisit = null;

        } else {
            nodesF.add_node(root, 1);
        }
    }

    /**
     * Updates the forward of a node in a BDD.
     *
     * @param node node of a BDD to calculates the forward probability
     * @param nEx number of the current example
     */
    private void updateForward(List<List<BDD>> nodesToVisit,
            List<Integer> NnodesToVisit,
            int iNode, int jNode, EMTable nodesF, int nEx) {

        int index, position;
        BDD T, E, node;//, nodereg;

        Double value_p, value_p_T, value_p_F, p;

        node = nodesToVisit.get(iNode).get(jNode);

        if (!node.isOne() && !node.isZero()) {

            index = node.var();

            p = probs_ex.get(nEx).get(index);

            //nodereg = node;
            value_p = nodesF.get_value(node);//(nodereg);

            if (value_p == null) {
                System.out.println("Error");
                //return;
            } else {
                T = node.high();
                E = node.low();

                if (!(T.isOne() || T.isZero())) {
                    value_p_T = nodesF.get_value(T);

                    if (value_p_T != null) {
                        value_p_T = value_p_T + (value_p * p);
                        nodesF.set_value(T, value_p_T);
                    } else {
                        nodesF.add_or_replace_node(T, (value_p * p));
                        index = T.var();
                        position = mgr.var2Level(index); //T.level(); //mgr.ithVar(index).level();
                        nodesToVisit.get(position).add(T);
                        NnodesToVisit.set(position, NnodesToVisit.get(position) + 1);
                    }
                }

                if (!(E.isOne() || E.isZero())) {
                    value_p_F = nodesF.get_value(E);

                    if (value_p_F != null) {
                        value_p_F = value_p_F + (value_p * (1 - p));
                        nodesF.set_value(E, value_p_F);
                    } else {
                        nodesF.add_or_replace_node(E, (value_p * (1 - p)));
                        index = E.var();
                        position = mgr.var2Level(index);//ithVar(index).level();
                        nodesToVisit.get(position).add(E);
                        NnodesToVisit.set(position, NnodesToVisit.get(position) + 1);
                    }
                }
            }
        }
    }

    /**
     * Calculates the expectation of a single BDD.
     *
     * @param root the root node of the BDD
     * @param ex_prob probability of the BDD in the current example
     * @param nex number of the current example
     * @return expectation of the BDD (the probability of BDD)
     */
    private double getOutsideExpe(BDD root, double ex_prob,
            EMTable nodesF, EMTable nodesB, List<double[]> eta, int nex) {
        int bVarIndex, nRules = rulesName.size();
        double[] eta_rule;
        double theta, rootProb, T = 0;

        List<double[]> eta_temp = new ArrayList<double[]>(nRules);
        double[] sigma = new double[boolVars_ex.get(nex)];

        for (int i = 0; i < boolVars_ex.get(nex); i++) {
            sigma[i] = 0;
        }

        for (int j = 0; j < nRules; j++) {
            double[] to_add = new double[2];
            to_add[0] = 0;
            to_add[1] = 0;
            eta_temp.add(to_add);
        }

        rootProb = probPath(root, sigma, eta_temp, nodesF, nodesB, nex);

        if (rootProb > 0.0) {
            for (int j = 0; j < boolVars_ex.get(nex); j++) {
                T += sigma[j];
                bVarIndex = mgr.ithVar(j).var();
                if (bVarIndex == -1) {
                    bVarIndex = j;
                }
//                boolAxiom[bVarIndex] = true;

                // Nel caso usa eta_temp invece che eta_rule
                eta_rule = eta_temp.get(vars_ex.get(nex).get(bVarIndex));

                theta = probs_ex.get(nex).get(bVarIndex);
                eta_rule[0] = eta_rule[0] + T * (1 - theta);
                eta_rule[1] = eta_rule[1] + T * theta;
            }

            for (int j = 0; j < nRules; j++) {
                eta.get(j)[0] = eta.get(j)[0] + eta_temp.get(j)[0] * ex_prob / rootProb;
                eta.get(j)[1] = eta.get(j)[1] + eta_temp.get(j)[1] * ex_prob / rootProb;
            }

        }

        sigma = null;

        return rootProb;

    }

    /**
     * Returns the probability of a BDD. During the execution it updates the
     * values contained in sigma
     *
     * @param node the input BDD
     * @param nex number of the current example
     * @return the probability of BDD
     */
    private double probPath(BDD node, double[] sigma,
            List<double[]> eta_temp, EMTable nodesF, EMTable nodesB, int nex) {
        int index, position;
        double res;
        double p, pt, pf, BChild0, BChild1, e0, e1;
        Double value_p;
        double[] eta_rule;
        BDD T, F;

        if (node.isOne()) {
            return 1.0;
        } else if (node.isZero()) {
            return 0.0;
        } else {
            value_p = nodesB.get_value(node);

            if (value_p != null) {
                return value_p;
            } else {
                index = node.var();
                p = probs_ex.get(nex).get(index);

                T = node.high();
                F = node.low();

                pf = probPath(F, sigma, eta_temp, nodesF, nodesB, nex);
                pt = probPath(T, sigma, eta_temp, nodesF, nodesB, nex);

                BChild0 = pf * (1 - p);
                BChild1 = pt * p;

                value_p = nodesF.get_value(node);

                e0 = value_p * BChild0;
                e1 = value_p * BChild1;

                eta_rule = eta_temp.get(vars_ex.get(nex).get(index));
                eta_rule[0] = eta_rule[0] + e0;
                eta_rule[1] = eta_rule[1] + e1;

                res = BChild0 + BChild1;

                nodesB.add_node(node, res);

                position = mgr.var2Level(index);//ithVar(index).level();
                position++;
                if (position < boolVars_ex.get(nex)) {
                    sigma[position] = sigma[position] + e0 + e1;
                }

                if (!(T.isOne() || T.isZero())) {
                    index = T.var();
                    position = mgr.var2Level(index);//ithVar(index).level();
                    sigma[position] = sigma[position] - e1;
                }

                if (!(F.isOne() || F.isZero())) {
                    index = F.var();
                    position = mgr.var2Level(index);//ithVar(index).var();
                    sigma[position] = sigma[position] - e0;
                }

                return res;
            }
        }
    }

    /**
     * @param merge the merge to set
     */
    public void setMerge(boolean merge) {
        this.merge = merge;
    }

    /**
     * Updates the parameters of the probabilistic axioms using the values
     * calculated by the expectation step.
     */
    private void maximization(List<double[]> eta) {

        double sum = 0;
        Double probs_rule;
        double[] eta_rule;
        int nRules = rulesName.size();

        for (int r = 0; r < nRules; r++) {
            if (usedAxioms[r]) {
                eta_rule = eta.get(r);

                sum = eta_rule[0] + eta_rule[1];

                if (sum == 0.0) {
                    arrayProb.set(r, sum);
                } else {
                    arrayProb.set(r, eta_rule[1] / sum);
                }
            }
        }

        for (int e = 0; e < numberOfExamples; e++) {

            //for (int j = 0; j < nVars_ex.get(e); j++){
            for (int j = 0; j < boolVars_ex.get(e); j++) {
                probs_rule = arrayProb.get(vars_ex.get(e).get(j));
                probs_ex.get(e).set(j, probs_rule);
            }
        }
    }
    
    
    
    private void maximizationMaster(List<double[]> eta) {

        double sum = 0;
        Double probs_rule;
        double[] eta_rule;
        int nRules = rulesName.size();

        for (int r = 0; r < nRules; r++) {
            if (usedAxioms[r]) {
                eta_rule = eta.get(r);

                sum = eta_rule[0] + eta_rule[1];

                if (sum == 0.0) {
                    arrayProb.set(r, sum);
                } else {
                    arrayProb.set(r, eta_rule[1] / sum);
                }
            }
        }

        for (int e = 0; e < numberOfExamples; e++) {

            //for (int j = 0; j < nVars_ex.get(e); j++){
            for (int j = 0; j < boolVars_ex.get(e); j++) {
                probs_rule = arrayProb.get(vars_ex.get(e).get(j));
                probs_ex.get(e).set(j, probs_rule);
            }
        }
    }

    /**
     * @return the ontologyFileName
     */
    public String getOntologyFileName() {
        return ontologyFileName;
    }

    /**
     * @param ontologyFileName the ontologyFileName to set
     */
    public void setOntologyFileName(String ontologyFileName) {
        this.ontologyFileName = ontologyFileName;
    }

    /**
     * @return the probOntologyFileName
     */
    public String getProbOntologyFileName() {
        return probOntologyFileName;
    }

    /**
     * @param probOntologyFileName the probOntologyFileName to set
     */
    public void setProbOntologyFileName(String probOntologyFileName) {
        this.probOntologyFileName = probOntologyFileName;
    }

    private String[] createFileNames() {
        String[] files;
        if (probOntologyFileName == null) {
            files = new String[1];
            files[0] = ontologyFileName;
        } else {
            files = new String[2];
            files[0] = ontologyFileName;
            files[1] = probOntologyFileName;
        }
        return files;
    }

    /**
     * @return the ontology
     */
    public OWLOntology getOntology() {
        return ontology;
    }

    /**
     * @param maxIterations the maxIterations to set
     */
    public void setMaxIterations(long maxIterations) {
        this.maxIterations = maxIterations;
    }

    /**
     * @return the maxIterations
     */
    public long getMaxIterations() {
        return maxIterations;
    }

    /**
     * @return the maxExplanations
     */
    public int getMaxExplanations() {
        return maxExplanations;
    }

    /**
     * @return the probOntology
     */
    public OWLOntology getProbOntology() {
        return probOntology;
    }

    /**
     * @return the positiveExamples
     */
    public List<OWLAxiom> getPositiveExamples() {
        return positiveExamples;
    }

    /**
     * @return the negativeExamples
     */
    public List<OWLAxiom> getNegativeExamples() {
        return negativeExamples;
    }

    private void sendExamples() {
        try {
            int chunkDim = numberOfExamples / slaves;
            numberOfExamples4Slaves = new int[slaves];
            for (int i = 0; i < slaves - 1; i++) {
                numberOfExamples4Slaves[i] = chunkDim;
            }
            numberOfExamples4Slaves[slaves - 1] = chunkDim + (numberOfExamples % chunkDim);
            
            int posExamples = positiveExamples.size();
            int pos = 0;
            for (int j = 0; j < slaves; j++) {
                int[] dimt = {numberOfExamples4Slaves[j]};
                MPI.COMM_WORLD.send(dimt, 1, MPI.INT, j, 0);
                for (int i = 0; i < numberOfExamples4Slaves[j]; i++) {

                    pos++;
                    if (pos < posExamples) {
                        MPIUtilities.sendObject(positiveExamples.get(pos), j, pos + 1);
                    } else {
                        MPIUtilities.sendObject(negativeExamples.get(pos - posExamples), j, (-1 * pos) + 1);
                    }
                    
                    vars_ex.add(new ArrayList<Integer>());
                    probs_ex.add(new ArrayList<Double>());
                    
                }
                
            }
            
        } catch (MPIException ex) {
            System.exit(-1);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(EDGE.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
        
    }

    private void receiveExamplesResults(double[] probs) {
        
        int received = 0;
        byte[] buffer = new byte[Double.SIZE + Integer.SIZE];
        double[] doubleBuffer = new double[1];
        int[] intBuffer = new int[1];
        while (received < numberOfExamples) {
            try {
                // Forse basta recv di double
                Status recvStat = MPI.COMM_WORLD.recv(buffer, buffer.length, MPI.PACKED, MPI.ANY_SOURCE, MPI.ANY_TAG);
                int pos = MPI.COMM_WORLD.unpack(buffer, 0, doubleBuffer, 1, MPI.DOUBLE);
                MPI.COMM_WORLD.unpack(buffer, pos, intBuffer, 1, MPI.INT);
                
                pos = recvStat.getTag();
                if (pos < 0) pos = pos * -1;
                pos--;
                probs[pos] = doubleBuffer[0];
                
                buffer = new byte[(Double.SIZE + Integer.SIZE) * intBuffer[0]];
                MPI.COMM_WORLD.recv(buffer, buffer.length, MPI.PACKED, recvStat.getSource(), recvStat.getTag());
                
                intBuffer = new int[intBuffer[0]];
                doubleBuffer = new double[intBuffer.length];
                pos = MPI.COMM_WORLD.unpack(buffer, 0, intBuffer, intBuffer.length, MPI.INT);
                MPI.COMM_WORLD.unpack(buffer, pos, doubleBuffer, doubleBuffer.length, MPI.DOUBLE);
                
                
                
                List<Integer> to_add_vars_ex = new ArrayList<>();
                List<Double> to_add_probs_ex = new ArrayList<>();
                for (int i = 0; i < intBuffer.length; i++) {
                    to_add_vars_ex.add(intBuffer[i]);
                    to_add_probs_ex.add(doubleBuffer[i]);
                    usedAxioms[intBuffer[i]] = true;
                }
                vars_ex.set(pos, to_add_vars_ex);
                probs_ex.set(pos, to_add_probs_ex);
                
            } catch (MPIException ex) {
                // provo a reinviare??
                java.util.logging.Logger.getLogger(EDGE.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private double expectationMaster(List<double[]> example_weight, List<double[]> eta, int lenNodes, boolean firstTime) {
        // send
        for (int j = 0; j < slaves; j++) {
            if (firstTime) {
                try {
                    MPI.COMM_WORLD.send(example_weight.get(j),numberOfExamples4Slaves[j],MPI.DOUBLE,j,0);
                } catch (MPIException ex) {
                    java.util.logging.Logger.getLogger(EDGE.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
     
        double LL = 0;
        
        int pos = 0;
        for (int j = 0; j < slaves; j++) {
            
            // eta + LLj
            double[] doubleBuffer = new double[(2 * numberOfExamples4Slaves[j]) + 1];
            try {
                MPI.COMM_WORLD.recv(doubleBuffer,(2 * numberOfExamples4Slaves[j]) + 1,MPI.DOUBLE,j,0);
            } catch (MPIException ex) {
                java.util.logging.Logger.getLogger(EDGE.class.getName()).log(Level.SEVERE, null, ex);
            }
            for (int i = 0; i < numberOfExamples4Slaves[j] * 2; i = i + 2, pos++) {
                eta.get(pos)[0] = doubleBuffer[i];
                eta.get(pos)[1] = doubleBuffer[i + 1];
            }
            
            LL = LL + doubleBuffer[(numberOfExamples4Slaves[j] * 2) + 1];
            
        }
        
        return LL;
        
    }

    private void receiveExamples() {
        int[] dim = new int[1];
        
        try {
            MPI.COMM_WORLD.recv(dim, 1, MPI.INT, 0, 0);
            queryTags = new int[dim[0]];
            
            OWLAxiom query = null;
            for (int i = 0; i < dim[0]; i++) {
                Status stat = MPIUtilities.recvObject((OWLAxiom)query, 0, MPI.ANY_TAG);
                if (stat.getTag() > 0) {
                    positiveExamples.add(query);
                } else {
                    negativeExamples.add(query);
                }
                queryTags[i] = stat.getTag();
            }
        } catch (MPIException ex) {
            java.util.logging.Logger.getLogger(EDGE.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(EDGE.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void sendExamplesResults(double[] probs) {
        
        byte[] buffer = new byte[Double.SIZE + Integer.SIZE];
        for (int i = 0; i < numberOfExamples; i++) {
            try {
                double[] doubleBuffer = {probs[i]};
                int[] intBuffer = {vars_ex.get(i).size()};
                int pos = MPI.COMM_WORLD.pack(doubleBuffer,1,MPI.DOUBLE,buffer,0);
                MPI.COMM_WORLD.pack(intBuffer, 1, MPI.INT, buffer, pos);
                
                MPI.COMM_WORLD.send(buffer, buffer.length, MPI.PACKED, 0, queryTags[i]);
                
                intBuffer = new int[vars_ex.get(i).size()];
                doubleBuffer = new double[vars_ex.get(i).size()];
                for (int j = 0; j < vars_ex.get(i).size(); j++) {
                    intBuffer[j] = vars_ex.get(i).get(j);
                    doubleBuffer[j] = probs_ex.get(i).get(j);
                }
                pos = MPI.COMM_WORLD.pack(intBuffer, intBuffer.length, MPI.INT, buffer, 0);
                MPI.COMM_WORLD.pack(doubleBuffer, doubleBuffer.length, MPI.DOUBLE, buffer, pos);
                
                MPI.COMM_WORLD.send(buffer, buffer.length, MPI.PACKED, 0, queryTags[i]);
                
            } catch (MPIException ex) {
                java.util.logging.Logger.getLogger(EDGE.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }

}

