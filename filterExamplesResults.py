#!/usr/bin/python
# -*- coding: iso-8859-1 -*-

from sys import argv
import re

if len(argv) != 3:
    print "usage: filterExamplesResult.py <inputFile> <outputFile>"

examples = {}
regex = re.compile(".*(prob: \d+.\d+.*tag: (\d+.\d+).*#vars: \d+)") 
with open(argv[1], "r") as fi:
    for line in fi:
        p = regex.match(line)
        if p:
            num = float(p.group(2))
            examples[num] = p.group(1) + "\n"


fo = open(argv[2], "w")
for key in sorted(examples):
    fo.write(examples[key])

fo.close()



