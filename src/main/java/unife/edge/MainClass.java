/**
 *  This file is part of EDGE.
 * 
 *  EDGE is a probabilistic parameter learner for OWL 2 ontologies. 
 *  It learns the probabilistic values of the probabilistic axioms starting from a set of positive and negative axioms. 
 * 
 *  EDGE can be used both as module and as standalone.
 *  
 *  EDGE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package unife.edge;

import org.apache.log4j.PropertyConfigurator;

/**
 * Main class of EDGE application.
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class MainClass {

    public static void main(String[] args) {
        //PropertyConfigurator.configure("config/log4j.properties");
        String logPropertiesPath = "/config/log4j.properties";
        PropertyConfigurator.configure(MainClass.class.getResourceAsStream(logPropertiesPath));
        EDGEUI ui = EDGEStart.getUI(args);
        ui.run(args);
    }

}
