/**
 *  This file is part of EDGE.
 * 
 *  EDGE is a probabilistic parameter learner for OWL 2 ontologies. 
 *  It learns the probabilistic values of the probabilistic axioms starting from a set of positive and negative axioms. 
 * 
 *  EDGE can be used both as module and as standalone.
 *  
 *  EDGE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package unife.edge;

import mpi.MPI;
import mpi.MPIException;
import org.apache.log4j.PropertyConfigurator;
import unife.edge.mpi.MPIUtilities;
import unife.edge.mpi.UnexpectedSignalValueException;

/**
 * Most likely this class will be canceled.
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>, Riccardo Zese
 * <riccardo.zese@unife.it>
 */
public class EDGEMainMPI {

    private static final String LOG_FILENAME_ID = "slaveId";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //System.out.println(System.getProperty("java.library.path"));
        try {
            args = MPI.Init(args);
        } catch (MPIException mpiEx) {
            String msg = "Impossible to initialize MPI: " + mpiEx.getMessage();
            System.err.print(msg);
        }
        boolean parallel = true;
        // Setting output file for logging
        if (System.getProperty(LOG_FILENAME_ID) != null) {
            throw new RuntimeException("slaveId property already defined somewhere else.");
        }
        String logPropertiesPath = "/config/log4j.properties";
        /*
         // set the index for the log file: log/edge${slaveId}.log
         int myRank;
         try {
         myRank = MPI.COMM_WORLD.getRank();
         } catch (MPIException mpiEx) {
         String msg = "Impossible to get the Rank: " + mpiEx.getMessage();
         System.err.printf(msg);
         throw new RuntimeException(msg);
         }
         // create log files
         System.setProperty("slaveId", "" + myRank);
         try {
         int numTotProc = MPI.COMM_WORLD.getSize();
         if (myRank != MPIUtilities.MASTER) {
         int[] signal = new int[1];
         MPI.COMM_WORLD.recv(signal, 1, MPI.INT, (myRank - 1), 0);
         // control check
         if (signal[0] != MPIUtilities.START) {
         throw new UnexpectedSignalValueException("UnexpectedSignalValueException: "
         + "value of the signal " + signal[0]);
         }
         }
         PropertyConfigurator.configure(EDGEMainMPI.class.getResourceAsStream(logPropertiesPath));
         //PropertyConfigurator.configure("config/log4j.properties");
         if (myRank != numTotProc - 1) {
         MPIUtilities.sendSignal((myRank + 1), MPIUtilities.START);
         }

         } catch (MPIException mpiEx) {
         System.err.print(myRank + " - NOT able to configure logger: " + mpiEx.getMessage());
         throw new RuntimeException(mpiEx);
         }
         */
        MPIUtilities.createLogFile(LOG_FILENAME_ID, logPropertiesPath);
        EDGEUI ui = EDGEStart.getUI(args, parallel);
        ui.run(args);
        try {
            MPI.Finalize();
        } catch (MPIException mpiEx) {
            String msg = "Cannot finalize MPI";
            System.err.print(msg);
        }
        System.out.println("Success");
    }

}
