/**
 *  This file is part of EDGE.
 * 
 *  EDGE is a probabilistic parameter learner for OWL 2 ontologies. 
 *  It learns the probabilistic values of the probabilistic axioms starting from a set of positive and negative axioms. 
 * 
 *  EDGE can be used both as module and as standalone.
 *  
 *  EDGE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package unife.edge;

import mpi.MPIException;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>, Riccardo Zese
 * <riccardo.zese@unife.it>
 */
public interface EDGEController {

    /**
     * Parse the options.
     *
     * @param options
     * @param args
     * @throws ParseException
     * @throws OWLOntologyCreationException
     * @throws mpi.MPIException
     */
    public void parseArgs(Options options, String[] args) throws ParseException, OWLOntologyCreationException, MPIException;

    /**
     * Execute the EDGE algorithm.
     *
     * @throws mpi.MPIException
     */
    public void run() throws MPIException;

    void showHelp();

}
