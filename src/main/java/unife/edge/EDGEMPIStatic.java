/**
 *  This file is part of EDGE.
 * 
 *  EDGE is a probabilistic parameter learner for OWL 2 ontologies. 
 *  It learns the probabilistic values of the probabilistic axioms starting from a set of positive and negative axioms. 
 * 
 *  EDGE can be used both as module and as standalone.
 *  
 *  EDGE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package unife.edge;

import mpi.MPI;
import mpi.MPIException;
import org.apache.log4j.Logger;
import unife.bundle.logging.BundleLoggerFactory;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>, Riccardo Zese
 * <riccardo.zese@unife.it>
 */
public class EDGEMPIStatic {
    
    private boolean MASTER;
    private int myRank;
    private final Logger logger = Logger.getLogger(EDGEMPISingleStep.class.getName(), 
            new BundleLoggerFactory());
    
    
    public EDGEMPIStatic() {
        try {
            myRank = MPI.COMM_WORLD.getRank();
            MASTER = (myRank == 0);
        } catch (MPIException mpiEx) {
            String msg = "Impossible to get the Rank: " + mpiEx.getMessage();
            System.err.println(msg);
            logger.error(msg);
            throw new RuntimeException(msg);
        }
    }
}
