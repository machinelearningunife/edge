/**
 *  This file is part of EDGE.
 * 
 *  EDGE is a probabilistic parameter learner for OWL 2 ontologies. 
 *  It learns the probabilistic values of the probabilistic axioms starting from a set of positive and negative axioms. 
 * 
 *  EDGE can be used both as module and as standalone.
 *  
 *  EDGE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package unife.edge;

import com.clarkparsia.pellet.owlapiv3.OWLAPILoader;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import mpi.MPI;
import mpi.MPIException;
import org.apache.log4j.Logger;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.mindswap.pellet.utils.Timer;
import org.mindswap.pellet.utils.Timers;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.formats.FunctionalSyntaxDocumentFormat;
import org.semanticweb.owlapi.formats.OWLXMLDocumentFormat;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLDocumentFormat;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import unife.bundle.logging.BundleLoggerFactory;
import static unife.edge.EDGECLI.options;
import unife.edge.mpi.MPIUtilities;
import unife.exception.IllegalValueException;

/**
 *
 * @author Riccardo Zese <riccardo.zese@unife.it>, Giuseppe Cota
 * <giuseppe.cota@unife.it>
 */
public class EDGEControllerImpl implements EDGEController {

    private final Logger logger = Logger.getLogger(EDGEControllerImpl.class.getName(), new BundleLoggerFactory());

    private EDGE edge;

    private CommandLine cmd;

    private String outputFile = System.getProperty("user.home") + "/probability.owl";

    private String outFormat = "DEFAULT";

    List<String> possibleFormat;

    private boolean test = false;
    private boolean parallel = false; // it is true if MPI is running

    public EDGEControllerImpl() {
        //edge = new EDGE();
        possibleFormat = new ArrayList<>();
        possibleFormat.add("DEFAULT");
        possibleFormat.add("OWLXML");
        possibleFormat.add("OWLFUNCTIONAL");
    }

    public EDGEControllerImpl(boolean parallel) {
        this();
        this.parallel = parallel;
    }

    @Override
    public void parseArgs(Options options, String[] args) throws ParseException, OWLOntologyCreationException, MPIException {
        CommandLineParser parser = new PosixParser();
        // parse the command line arguments
        cmd = parser.parse(options, args);
        if (cmd.hasOption("file")) {
            applyArgs(cmd.getOptionValue("file"));
        } else if (cmd.getOptions().length >= 1) {
            options.getOption("ont").setRequired(true);
            applyArgs();
        } else {
            String msg = "Wrong number of arguments. Type 'edge -h' for usage";
            logger.error(msg);
            System.err.println(msg);
            System.exit(-1);
        }
    }

    private void applyArgs(String file) throws OWLOntologyCreationException, MPIException {
        File name = new File(file);
        String key = "";
        if (name.isFile()) {
            try {
                BufferedReader input = new BufferedReader(new FileReader(name));
                String text, value;

                if (!parallel) {
                    edge = new EDGE();
                }

                int pos, posComm, posSpace;
//                edge.setPositiveExamples(new ArrayList<OWLAxiom>());
//                edge.setNegativeExamples(new ArrayList<OWLAxiom>());
                while ((text = input.readLine()) != null) {
                    if (!text.equals("")) {
                        pos = text.indexOf(":");
                        posComm = text.indexOf("#");
                        posSpace = text.indexOf(" ");

                        if (posSpace != -1 && posComm != -1 && posSpace < posComm) {
                            logger.fatal("Error in input file...\n");
                            logger.fatal("Uncorrect Row " + text);

                            logger.error("Blank are not permitted in row before comment character '#'!");
                            System.err.println("Blank are not permitted in row before comment character '#'!");
                            throw new RuntimeException("Impossible to parse input file");

                        }

                        if (posSpace != -1 && posComm == -1) {
                            logger.fatal("Error in input file...\n");
                            logger.fatal("Uncorrect Row " + text);

                            logger.error("Blank are not permitted in row without comments!");
                            System.err.println("Blank are not permitted in row without comments!");
                            throw new RuntimeException("Impossible to parse input file");
                        }

                        if (posComm != -1 && posComm < pos) {
                            pos = -1;
                        }

                        if (pos != -1) {
                            key = text.substring(0, pos);
                            if (posComm == -1) {
                                value = text.substring(pos + 1);
                            } else {
                                value = text.substring(pos + 1, posComm);
                            }

                            if (!key.startsWith("#")) {
                                switch (key) {
                                    case "scheduling":
                                        if (parallel) {
                                            switch (value.toUpperCase()) {
                                                case "SINGLE":
                                                    edge = new EDGEMPISingleStep();
                                                    break;
                                                case "STATIC":
                                                    throw new UnsupportedOperationException("STATIC MPI non supported yet!");
                                                case "DYNAMIC_OLD":
                                                    logger.warn("This option will be deleted in future!");
                                                    edge = new EDGEMPIDynamicOld();
                                                    break;
                                                case "DYNAMIC":
                                                    edge = new EDGEMPIDynamic();
                                                    break;
                                                default:
                                                    throw new IllegalArgumentException("Illegal argument type: -mpi " + value);
                                            }
                                        }
                                        break;
                                    case "chunk_dim":
                                        if (edge instanceof EDGEMPIDynamic) {
                                            ((EDGEMPIDynamic) edge).setChunkDim(Integer.parseInt(value));
                                        }
                                        break;
                                    case "positive":
                                        if (!value.equals("none")) {
                                            if (!parallel || MPIUtilities.isMaster(MPI.COMM_WORLD)) {
                                                edge.setPositiveExamples(getExamples("file:" + value));
                                            }
                                        }
                                        break;
                                    case "negative":
                                        if (!value.equals("none")) {
                                            if (!parallel || MPIUtilities.isMaster(MPI.COMM_WORLD)) {
                                                edge.setNegativeExamples(getExamples("file:" + value));
                                            }
                                        }
                                        break;
                                    case "randomize":
                                        if (value.equals("true")) {
                                            edge.setRandomize(true);
                                        } else if (value.equals("all")) {
                                            edge.setRandomize(true);
                                            edge.setProbabilizeAll(true);
                                        } else if (!value.equals("false")) {
                                            logger.fatal("Error in input file...\n");

                                            logger.error("Error in parameter randomize");
                                            System.err.println("Error in parameter randomize, check the file");
                                            throw new RuntimeException("Impossible to parse input file");
                                        }
                                        break;
                                    case "ontology":
                                        //edge.setOntologies(OWLManager.createOWLOntologyManager().loadOntologyFromOntologyDocument(IRI.create("file:" + value)));
                                        edge.setOntologyFileName("file:" + value);
                                        break;
                                    case "probability":
                                        //edge.setProbOntology(OWLManager.createOWLOntologyManager().loadOntologyFromOntologyDocument(IRI.create("file:" + value)));
                                        if (!value.equals(edge.getOntologyFileName())) {
                                            edge.setProbOntologyFileName("file:" + value);
                                        }
                                        break;
                                    case "random_seed":
                                        if (Integer.valueOf(value) != null) {
                                            edge.setSeed(Integer.parseInt(value));
                                        }
                                        break;
                                    case "diff_ll":
                                        if (value != null) {
                                            edge.setDiffLL(value);
                                        }
                                        break;
                                    case "ratio_ll":
                                        if (value != null) {
                                            edge.setRatioLL(value);
                                        }
                                        break;
                                    case "max_iter":
                                        if (Long.decode(value) != null) {
                                            edge.setMaxIterations(Long.decode(value));
                                        }
                                        break;
                                    case "max_expl":
                                        if (Integer.decode(value) != null) {
                                            edge.setMaxExplanations(Integer.parseInt(value));
                                        }
                                        break;
                                    case "timeout":
                                        edge.setTimeOut(value);
                                        break;
                                    case "test":
                                        if (value.equals("true")) {
                                            test = true;
                                        } else if (!value.equals("false")) {
                                            logger.fatal("Error in input file...\n");

                                            logger.error("Error in parameter test");
                                            System.err.println("Error in parameter test, check the file");
                                            throw new RuntimeException("Impossible to parse input file");
                                        }
                                        break;
                                    case "merge":
                                        if (value.equals("true")) {
                                            edge.setMerge(true);
                                        } else if (!value.equals("false")) {
                                            logger.fatal("Error in input file...\n");

                                            logger.error("Error in parameter merge");
                                            System.err.println("Error in parameter merge, check the file");
                                            throw new RuntimeException("Impossible to parse input file");
                                        }
                                        break;
                                    case "show_all":
                                        if (value.equals("true")) {
                                            edge.setShowAll(true);
                                        } else if (value.equals("false")) {
                                            edge.setShowAll(false);
                                        } else {
                                            logger.fatal("Error in input file...\n");

                                            logger.error("Error in parameter show_all");
                                            System.err.println("Error in parameter show_all, check the file");
                                            throw new RuntimeException("Impossible to parse input file");
                                        }
                                        break;
                                    case "output":
                                        if (value != null && !value.equals("none")) {
                                            outputFile = value;
                                        }
                                        break;
                                    case "format":
                                        if (possibleFormat(value)) {
                                            outFormat = value.toUpperCase();
                                        } else {
                                            logger.fatal("Error in input file...\n");

                                            logger.error("Error in parameter output format");
                                            logger.error("Unknown Format " + value);
                                            System.err.println("Error in parameter output format, check the file");
                                            throw new RuntimeException("Impossible to parse input file");
                                        }
                                        break;
                                    default:
                                        logger.fatal("Error in input file...\n");
                                        logger.error("Unknown Parameter " + key);
                                        System.err.println("Unknown Parameter " + key + ", check the file");
                                        throw new RuntimeException("Impossible to parse input file");
                                }
                            }
                        } else {
                            if (!text.startsWith("#")) {
                                logger.fatal("Error in input file...\n");

                                logger.error("Uncorrect Row " + text);
                                System.err.println("Uncorrect Row " + text + ", check the file");
                                throw new RuntimeException("Impossible to parse input file");
                            }
                        }
                    }
                }
                input.close();

            } catch (NumberFormatException e) {
                logger.fatal("Error in input file...\nPlease check the numeric values of " + key + "parameter");
                logger.fatal(e.getMessage());
                logger.trace(e.getStackTrace());

                System.err.println("Terminated because of an error in input file. See the log for details");
                throw new RuntimeException(e);
            } catch (IOException e) {
                logger.fatal("Error in input file...\nRead failure");
                logger.fatal(e.getMessage());
                logger.trace(e.getStackTrace());

                System.err.println("Terminated because of an error during the reading of input file. See the log for details");
                throw new RuntimeException(e);
            } catch (IllegalValueException ex) {
                logger.fatal("Error in input file...\nRead failure");
                logger.fatal(ex.getMessage());
                logger.trace(ex.getStackTrace());

                System.err.println("Terminated because of an error during the reading of input file. See the log for details");
                throw new RuntimeException(ex);
            }
        } else {
            logger.fatal("Error in input file...\nRead failure: impossible to open input file");

            System.err.println("Terminated because of an error during the reading of input file. See the log for details");
            throw new RuntimeException("Read failure: impossible to open input file");
        }

    }

    private void applyArgs() throws OWLOntologyCreationException, ParseException {

        String temp;

        if (cmd.hasOption("help")) {
            showHelp();
            System.exit(0);
        }

        if (isParallel()) {
            temp = cmd.getOptionValue("scheduling");

            if (temp != null) {
                switch (temp.toUpperCase()) {
                    case "SINGLE":
                        edge = new EDGEMPISingleStep();
                        break;
                    case "STATIC":
                        throw new UnsupportedOperationException("STATIC MPI non supported yet!");
                    case "DYNAMIC":
                        edge = new EDGEMPIDynamicOld();
                        break;
                    case "DYNAMIC_NEW":
                        edge = new EDGEMPIDynamic();
                        break;
                    default:
                        throw new IllegalArgumentException("Illegal argument type: -mpi " + temp);
                }

            } else {
                edge = new EDGEMPISingleStep();
            }

            temp = cmd.getOptionValue("chunk_dim");
            if (temp != null && edge instanceof EDGEMPIDynamic) {
                ((EDGEMPIDynamic) edge).setChunkDim(Integer.parseInt(temp));
            }
        } else {
            edge = new EDGE();
        }

        boolean MASTER = true;
        try {
            MASTER = isParallel() ? MPI.COMM_WORLD.getRank() == 0 : true;
        } catch (MPIException mpiEx) {
            String msg = "Impossible to get the Rank: " + mpiEx.getMessage();
            System.err.println(msg);
            logger.error(msg);
            throw new RuntimeException(msg);
        }
        if (MASTER) {
            //positive examples
            temp = cmd.getOptionValue("positive");
            if (temp != null) {
                edge.setPositiveExamples(getExamples(temp));
            } else {
                edge.setPositiveExamples(new ArrayList<OWLAxiom>());
            }

            // negative examples
            temp = cmd.getOptionValue("negative");
            if (temp != null) {
                edge.setNegativeExamples(getExamples(temp));
            } else {
                edge.setNegativeExamples(new ArrayList<OWLAxiom>());
            }
        }
        // starting ontology
        //OWLAPILoader loader = new OWLAPILoader();
        //OWLOntologyManager manager = loader.getManager();
        temp = cmd.getOptionValue("ontology");
        if (temp == null) {
            // ERRORE
            logger.fatal("At least one ontology must be given");
            throw new ParseException("At least one ontology must be given");
        }
//        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
//        OWLOntology ontology = manager.loadOntologyFromOntologyDocument(IRI.create(temp));
//        edge.setOntology(ontology);
        edge.setOntologyFileName(temp);

        // probabilistic ontology
        temp = cmd.getOptionValue("probability");
        if (temp != null) {
            //edge.setProbOntology(manager.loadOntologyFromOntologyDocument(IRI.create(temp)));
            edge.setProbOntologyFileName(temp);
        } //else {
        //edge.setProbOntology(ontology);
        //}

//        ontology = null;
//        manager = null;
        //loader = null;
        // randomize
        edge.setRandomize(cmd.hasOption("r"));

        // randomize all
        edge.setProbabilizeAll(cmd.hasOption("ra"));

        // random seed
        if (cmd.hasOption("r") || cmd.hasOption("ra")) {
            temp = cmd.getOptionValue("rs");
            try {
                edge.setSeed(Integer.parseInt(temp));
            } catch (NumberFormatException ex) {
                logger.fatal("Error in random seed value...\nPlease check the numeric values");
                logger.fatal(ex.getMessage());
                logger.trace(ex.getStackTrace());

                System.err.println("Terminated because of an error in random seed value. See the log for details");
                throw new RuntimeException(ex);
            }
        }

        // diffLL
        temp = cmd.getOptionValue("diff_ll");
        if (temp != null) {
            try {
                edge.setDiffLL(temp);
            } catch (NumberFormatException ex) {
                logger.fatal("Error in LL's difference value...\nPlease check the numeric values");
                logger.fatal(ex.getMessage());
                logger.trace(ex.getStackTrace());

                System.err.println("Terminated because of an error in LL's difference value. See the log for details");
                throw new RuntimeException(ex);
            } catch (IllegalValueException ex) {
                logger.fatal("Error in input file...\nRead failure");
                logger.fatal(ex.getMessage());
                logger.trace(ex.getStackTrace());

                System.err.println("Terminated because of an error during the reading of input file. See the log for details");
                throw new RuntimeException(ex);
            }
        }

        // ratioLL
        temp = cmd.getOptionValue("ratio_ll");
        if (temp != null) {
            try {
                edge.setRatioLL(temp);
            } catch (NumberFormatException ex) {
                logger.fatal("Error in LL's ratio value...\nPlease check the numeric values");
                logger.fatal(ex.getMessage());
                logger.trace(ex.getStackTrace());

                System.err.println("Terminated because of an error in LL's ratio value. See the log for details");
                throw new RuntimeException(ex);
            } catch (IllegalValueException ex) {
                logger.fatal("Error in input file...\nRead failure");
                logger.fatal(ex.getMessage());
                logger.trace(ex.getStackTrace());

                System.err.println("Terminated because of an error during the reading of input file. See the log for details");
                throw new RuntimeException(ex);
            }
        }

        // maxIteration
        temp = cmd.getOptionValue("max_iter");
        if (temp != null) {
            try {
                edge.setMaxIterations(Integer.parseInt(temp));
            } catch (NumberFormatException ex) {
                logger.fatal("Error in maximum number of iteration value...\nPlease check the numeric values");
                logger.fatal(ex.getMessage());
                logger.trace(ex.getStackTrace());

                System.err.println("Terminated because of an error in maximum number of iteration value. See the log for details");
                throw new RuntimeException(ex);
            }
        }

        // maxExplanation
        temp = cmd.getOptionValue("max_expl");
        if (temp != null) {
            try {
                edge.setMaxExplanations(Integer.parseInt(temp));
            } catch (NumberFormatException ex) {
                logger.fatal("Error in maximum number of explanations to find...\nPlease check the numeric values");
                logger.fatal(ex.getMessage());
                logger.trace(ex.getStackTrace());

                System.err.println("Terminated because of an error in maximum number of explanations to find. See the log for details");
                throw new RuntimeException(ex);
            }
        }

        //timeout
        temp = cmd.getOptionValue("timeout");
        if (temp != null) {
            edge.setTimeOut(temp);
        }

        edge.setShowAll(cmd.hasOption("show_all"));
        // test
        test = (cmd.hasOption("test"));

        // merge
        edge.setMerge(cmd.hasOption("merge"));

        // output filepath
        temp = cmd.getOptionValue("output");
        if (temp != null && !temp.equals("none")) {
            outputFile = temp;
        }

        // output format
        temp = cmd.getOptionValue("format");
        if (temp != null) {
            outFormat = temp;
        }

    }

    @Override
    public void run() throws MPIException {
        if (test) {
            edge.computeTest();
        } else {

            EDGEStat result = edge.computeLearning();
            if (outputFile != null && (!parallel || MPIUtilities.isMaster(MPI.COMM_WORLD))) {

                OWLOntology resultOntology = edge.getLearnedOntology();
                logger.info("");
                logger.info("Writing of the learned ontology...");

                Timers timers = new Timers();
                Timer timer = timers.createTimer("OntologyWriting");
                timer.start();
                try {
                    saveOntology(resultOntology);

                } catch (OWLOntologyStorageException ex) {
                    logger.error("It was impossible to write the learned ontology");
                    logger.error(ex.getMessage());
                    logger.trace(ex.getStackTrace());

                    logger.info("Try to write in default position using default format...");
                    logger.info("Writing in " + System.getProperty("user.home") + "probability.owl");
                    outputFile = System.getProperty("user.home") + "probability.owl";
                    outFormat = "DEFAULT";
                    try {
                        saveOntology(resultOntology);
                    } catch (OWLOntologyStorageException ex1) {
                        logger.fatal("Recovering failed. Quit...");
                        logger.fatal(ex1.getMessage());
                        logger.trace(ex1.getStackTrace());

                        System.err.println("Terminated because of an internal error. See the log for details");
                        throw new RuntimeException(ex1);
                    }
                }

                timer.stop();

                logger.info("Successful writing of the learned ontology");
                logger.info("Ontology written in " + timer.getAverage() + " (ms)");
            }
        }
//        if (isMpi()) {
//            try {
//                logger.info("Finalizing MPI...");
//                MPI.Finalize();
//                logger.info("Ok");
//            } catch (MPIException ex) {
//                logger.error("Error in finalizing MPI. Reason: " + ex.getMessage());
//                System.exit(-1);
//            }
//        }
    }

    private boolean possibleFormat(String format) {
        return possibleFormat.contains(format.toUpperCase());
    }

    private List<OWLAxiom> getExamples(String ontologyPath) {

        OWLAPILoader loader;
        OWLOntologyManager manager;
        OWLOntology ontology = null;

        manager = OWLManager.createOWLOntologyManager();

        List<OWLAxiom> to_return = new ArrayList<OWLAxiom>();

        try {
            ontology = manager.loadOntologyFromOntologyDocument(IRI.create(ontologyPath));
        } catch (OWLOntologyCreationException ex) {
            logger.fatal("Error in loading " + ontologyPath + " file...");
            logger.fatal(ex.getMessage());
            logger.trace(ex.getStackTrace());

            System.err.println("Terminated because of an error in loading " + ontologyPath + " file. See the log for details");
            throw new RuntimeException(ex);
        }

        for (OWLAxiom ax : ontology.getAxioms()) {
            if (ax.isOfType(AxiomType.CLASS_ASSERTION)) {
                to_return.add(ax);
            } else if (ax.isOfType(AxiomType.OBJECT_PROPERTY_ASSERTION)) {
                to_return.add(ax);
            } else if (ax.isOfType(AxiomType.SUBCLASS_OF)) {
                to_return.add(ax);
            }
        }

        Collections.sort(to_return);
        return to_return;
    }

    private void saveOntology(OWLOntology resultOntology) throws OWLOntologyStorageException {
        OWLDocumentFormat formatter;
        switch (outFormat) {
            case "OWLXML":
                formatter = new OWLXMLDocumentFormat();
                break;
            case "OWLFUNCTIONAL":
                formatter = new FunctionalSyntaxDocumentFormat();
                break;
            default:
                formatter = new OWLXMLDocumentFormat();
                break;
        }
        resultOntology.getOWLOntologyManager().saveOntology(resultOntology, formatter, IRI.create(new File(outputFile)));
    }

    /**
     * @return
     */
    public boolean isParallel() {
        return parallel;
    }

    @Override
    public void showHelp() {
        // automatically generate the help statement
        HelpFormatter formatter = new HelpFormatter();
        String footer = "\nPlease report issues at https://sites.google.com/a/unife.it/ml/edge";
        formatter.printHelp("edge", EDGECLIImpl.header, options, footer, true);
    }

}
