/**
 *  This file is part of EDGE.
 * 
 *  EDGE is a probabilistic parameter learner for OWL 2 ontologies. 
 *  It learns the probabilistic values of the probabilistic axioms starting from a set of positive and negative axioms. 
 * 
 *  EDGE can be used both as module and as standalone.
 *  
 *  EDGE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package unife.edge;

//import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.mindswap.pellet.utils.Timer;
import org.semanticweb.owlapi.model.OWLAxiom;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>, Riccardo Zese
 * <riccardo.zese@unife.it>
 */
public class EDGEStatImpl implements EDGEStat {

    // number of iterations of the Expectation-Maximization algorithm
    private final long iterations;

    /**
     * Log-likelihood
     */
    private final BigDecimal LL;
    /**
     * Map of probabilistic axioms with their probabilities
     */
    transient private List<OWLAxiom> axioms;
    private List<BigDecimal> probabilities;

    transient private List<Timer> timers;
    private TreeMap<String, Long> tMap;

    public EDGEStatImpl(long iterations, BigDecimal LL, List<OWLAxiom> axioms, List<BigDecimal> probabilities, Collection<Timer> timings) {
        this.iterations = iterations;
        this.LL = LL;
        this.axioms = axioms;
        this.probabilities = probabilities;
        this.timers = new ArrayList<>(timings);
        tMap = new TreeMap<>();
        for (Timer t : timers) {
            tMap.put(t.getName(), t.getCount());
        }
    }

    public EDGEStatImpl(long iterations, BigDecimal LL, List<OWLAxiom> axioms, List<BigDecimal> probabilities) {
        this.iterations = iterations;
        this.LL = LL;
        this.axioms = axioms;
        this.probabilities = probabilities;
        this.timers = null;
    }

    public EDGEStatImpl(long iterations, BigDecimal LL, Map<OWLAxiom, BigDecimal> probAxioms, Collection<Timer> timings) {
        this.iterations = iterations;
        this.LL = LL;
        for (OWLAxiom axiom : probAxioms.keySet()) {
            this.axioms.add(axiom);
            this.probabilities.add(probAxioms.get(axiom));
        }
        this.timers = new ArrayList<>(timings);
        tMap = new TreeMap<String, Long>();
        for (Timer t : timers) {
            tMap.put(t.getName(), t.getCount());
        }
    }

    public EDGEStatImpl(long iterations, BigDecimal LL, Map<OWLAxiom, BigDecimal> probAxioms) {
        this.iterations = iterations;
        this.LL = LL;
        for (OWLAxiom axiom : probAxioms.keySet()) {
            this.axioms.add(axiom);
            this.probabilities.add(probAxioms.get(axiom));
        }
        this.timers = null;
    }

    /**
     * @return the iterations
     */
    @Override
    public long getIterations() {
        return iterations;
    }

    /**
     * @return the LL
     */
    @Override
    public BigDecimal getLL() {
        return LL;
    }

    /**
     * @return the probAxioms
     */
    @Override
    public Map<OWLAxiom, BigDecimal> getProbAxioms() {
        Map<OWLAxiom, BigDecimal> map = new HashMap<>();
        int size = axioms.size();
        for (int i = 0; i < size; i++) {
            map.put(axioms.get(i), probabilities.get(i));
        }
        return map;
    }

    /**
     * @return the timings
     */
    @Override
    public TreeMap<String, Long> getTimers() {
        return tMap;
    }

    public void setTimers(Collection<Timer> timers) {
        this.timers = new ArrayList<>(timers);
        tMap = new TreeMap<String, Long>(); 
        for (Timer t : timers) {
            if (t.getName().equals("main")) {
                tMap.put("EDGE", t.getTotal());
            } else {
                tMap.put("EDGE." + t.getName(), t.getTotal());
            }
        }
    }

}
