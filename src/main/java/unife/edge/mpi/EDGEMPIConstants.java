/**
 *  This file is part of EDGE.
 * 
 *  EDGE is a probabilistic parameter learner for OWL 2 ontologies. 
 *  It learns the probabilistic values of the probabilistic axioms starting from a set of positive and negative axioms. 
 * 
 *  EDGE can be used both as module and as standalone.
 *  
 *  EDGE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package unife.edge.mpi;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>, Riccardo Zese
 * <riccardo.zese@unife.it>
 */
public class EDGEMPIConstants {

    public static final int TERMINATE = -1;
    public static final int STOP = 0;
    public static final int CONTINUE = 1;
    public static final int START = 2;

    public static final int ALL = -1;
    public static final int ALL_BCAST = -2;

    public static final int MASTER = 0;

    public static final int METHOD1 = 1;
    public static final int METHOD2 = 2;
}
