/**
 *  This file is part of EDGE.
 * 
 *  EDGE is a probabilistic parameter learner for OWL 2 ontologies. 
 *  It learns the probabilistic values of the probabilistic axioms starting from a set of positive and negative axioms. 
 * 
 *  EDGE can be used both as module and as standalone.
 *  
 *  EDGE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package unife.edge.mpi;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import mpi.Intracomm;
import mpi.MPI;
import mpi.MPIException;
import mpi.Status;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import unife.bundle.logging.BundleLoggerFactory;
import static unife.edge.mpi.EDGEMPIConstants.*;

/**
 *
 * @author Riccardo Zese <riccardo.zese@unife.it>
 */
public class MPIUtilities {

    //private static final Logger logger = Logger.getLogger(MPIUtilities.class.getName(), new BundleLoggerFactory());
    // Translate a generic SERIALIZABLE object into a byte array
    public static byte[] objectToByteArray(Object obj) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutput out = null;
        byte[] yourBytes = null;
        try {
            out = new ObjectOutputStream(bos);
            out.writeObject(obj);
            yourBytes = bos.toByteArray();
        } catch (Exception ex) {
            Logger.getLogger(MPIUtilities.class.getName(), new BundleLoggerFactory()).fatal(ex);
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(MPIUtilities.class.getName(), new BundleLoggerFactory()).fatal(ex);
            }
            try {
                bos.close();
            } catch (IOException ex) {
                Logger.getLogger(MPIUtilities.class.getName(), new BundleLoggerFactory()).fatal(ex);
            }
        }
        return yourBytes;
    }

// Translate a byte array into a java.lang.Object object
    public static Object byteArrayToObject(byte bytes[]) {
        ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
        ObjectInput in = null;
        Object o = null;
        try {
            in = new ObjectInputStream(bis);
            o = in.readObject();
            Logger.getLogger(MPIUtilities.class.getName(), new BundleLoggerFactory()).debug("object readed");
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(MPIUtilities.class.getName(), new BundleLoggerFactory()).fatal("Cannot read object", ex);
            throw new RuntimeException(ex);
        } finally {
            try {
                bis.close();
            } catch (IOException ex) {
                // ignore close exception
            }
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                // ignore close exception
            }
        }
        return o;
    }

    /**
     * Send a generic object to dest with tag
     *
     */
    public static void sendObject(Object obj, int dest, int tag, Intracomm comm) throws MPIException {
        // Translate the object into byte array
        byte[] bytes = objectToByteArray(obj);
        // Save length of byte array
        //int[] sizePack = {bytes.length};
        // Send size of byte array
        //MPI.COMM_WORLD.send(sizePack, 1, MPI.INT, dest, tag);
        // Send byte array
        if (dest == ALL) {
            for (int i = 1; i < comm.getSize(); i++) {
                comm.send(bytes, bytes.length, MPI.BYTE, i, tag);
            }
//        } else if (dest == ALL_BCAST) {
//            comm.bcast(bytes.length, 1, MPI.INT, tag);
//            comm.bcast(bytes, bytes.length, MPI.BYTE, tag);
        } else {
            comm.send(bytes, bytes.length, MPI.BYTE, dest, tag);
        }
    }

    public static int sendBCastObject(Object obj, Intracomm comm) throws MPIException {
        // Translate the object into byte array
        byte[] bytes = objectToByteArray(obj);
        // Send byte array
        comm.bcast(new int[] {bytes.length}, 1, MPI.INT, comm.getRank());
        comm.bcast(bytes, bytes.length, MPI.BYTE, comm.getRank());
        return bytes.length;
    }

//    public static void sendObject(Object obj, int dest, int tag) throws MPIException {
//        sendObject(obj, dest, tag, MPI.COMM_WORLD);
//    }
    /**
     * Receive a generic object from sender belonging to a given group with a
     * given tag.
     *
     * @param sender sender of the packet.
     * @param tag tag of the packet
     * @param comm intracommunicator of the sender's group.
     * @return the received object
     * @throws mpi.MPIException
     */
    public static RecvObjectStatus recvObject(int sender, int tag, Intracomm comm) throws MPIException {
        Object obj;
        byte[] bytes;
        int[] i = new int[1];
        int size;
        // Receive the length of the byte array
        //MPI.COMM_WORLD.recv(i, 1, MPI.INT, dest, tag);
        Status status = comm.probe(sender, tag);
        // Create the correct buffer
        size = status.getCount(MPI.BYTE);
        //size = i[0];
        bytes = new byte[size];
        // Receive the byte array
        Status stat = comm.recv(bytes, size, MPI.BYTE, sender, tag);
        // Translate the byte array into object
        obj = byteArrayToObject(bytes);
        return new RecvObjectStatus(stat, obj);
    }

    public static Object recvBCastObject(int root, Intracomm comm) throws MPIException {
        Logger logger = Logger.getLogger(MPIUtilities.class.getName(), new BundleLoggerFactory());
        Object obj;
        byte[] bytes;
        int[] size = new int[1];
        // Receive the length of the byte array
        comm.bcast(size, 1, MPI.INT, root);
        String sender = "" + root;
        if (root == MASTER) {
            sender = "MASTER";
        }
        logger.debug("Receiving from " + sender + " an object of " + size[0] + " bytes.");
        // Create the correct buffer
        bytes = new byte[size[0]];
        // Receive the byte array
        comm.bcast(bytes, size[0], MPI.BYTE, root);
        logger.debug("Received from " + sender + " an object of " + bytes.length + " bytes.");
        // Translate the byte array into object
        obj = byteArrayToObject(bytes);
        return obj;
    }

//    public static RecvObjectStatus recvObject(int sender, int tag) throws MPIException {
//        return recvObject(sender, tag, MPI.COMM_WORLD);
//    }
    public static void sendSignal(int id, int msg, Intracomm comm) throws MPIException {
        int[] signal = {msg};

        if (id == ALL) {
            for (int i = 1; i < comm.getSize(); i++) {
//                    Logger.getLogger(MPIUtilities.class.getName(), new BundleLoggerFactory()).debug("Sending " + msg + " to " + i);
                comm.send(signal, 1, MPI.INT, i, 0);
            }

        } else if (id == ALL_BCAST) {
            comm.bcast(signal, 1, MPI.INT, 0);
        } else {
            Logger.getLogger(MPIUtilities.class.getName(), new BundleLoggerFactory()).debug(comm.getRank() + " - Sending " + msg + " to " + id);
            comm.send(signal, 1, MPI.INT, id, 0);

        }
    }

    public static void sendBCastSignal(int msg, Intracomm comm) throws MPIException {
        int[] signal = {msg};
        comm.bcast(signal, 1, MPI.INT, comm.getRank());
    }

    public static int recvBCastSignal(int root, Intracomm comm) throws MPIException {
        int[] signal = new int[1];
        comm.bcast(signal, 1, MPI.INT, root);
        return signal[0];
    }

//    public static void sendSignal(int id, int msg) throws MPIException {
//        sendSignal(id, msg, MPI.COMM_WORLD);
//    }
    /**
     * Return the number of slaves in the given group.
     *
     * @param comm communicator used by the group.
     * @return the number of slaves.
     * @throws mpi.MPIException
     */
    public static int getSlaves(Intracomm comm) throws MPIException {
        return comm.getSize() - 1;
    }

    /**
     * Return the number of slaves in the group WORLD.
     *
     * @return the number of slaves
     * @throws mpi.MPIException
     */
//    public static int getSlaves() throws MPIException {
//        return getSlaves(MPI.COMM_WORLD);
//    }
    /**
     * Returns true if the current process is a master
     *
     * @param comm communicator
     * @return
     */
    public static boolean isMaster(Intracomm comm) {
        try {
            return comm.getRank() == MASTER;
        } catch (MPIException mpiEx) {
            Logger.getLogger(MPIUtilities.class.getName(), new BundleLoggerFactory())
                    .error("Cannot determinate if the current process is master");
            throw new RuntimeException(mpiEx);
        }
    }

    /**
     * Returns true if the current process is a master
     *
     * @return
     */
//    public static boolean isMaster() throws MPIException {
//        return isMaster(MPI.COMM_WORLD);
//    }
    /**
     * Utility method used to create a file for each process of the distributed
     * environment.
     *
     * @param id the string that will be replaced by the rank of the process
     * relative to the MPI_COMM_WORLD group.
     * @param propertyFile logger property file
     */
    public static void createLogFile(String id, String propertyFile) {
        // set the index for the log file: ${`id`}.log
        int myRank;
        try {
            myRank = MPI.COMM_WORLD.getRank();
        } catch (MPIException mpiEx) {
            String msg = "Impossible to get the Rank: " + mpiEx.getMessage();
            System.err.printf(msg);
            throw new RuntimeException(msg);
        }
        // create log files
        System.setProperty(id, "" + myRank);
        try {
            int numTotProc = MPI.COMM_WORLD.getSize();
            if (!isMaster(MPI.COMM_WORLD)) {
                int[] signal = new int[1];
                MPI.COMM_WORLD.recv(signal, 1, MPI.INT, (myRank - 1), 0);
                // control check
                if (signal[0] != START) {
                    throw new UnexpectedSignalValueException("UnexpectedSignalValueException: "
                            + "value of the signal " + signal[0]);
                }
            }

            if (propertyFile != null) {
                PropertyConfigurator.configure(MPIUtilities.class.getResourceAsStream(propertyFile));
            }
            if (myRank != numTotProc - 1) {
                MPIUtilities.sendSignal((myRank + 1), START, MPI.COMM_WORLD);
            }

        } catch (MPIException mpiEx) {
            System.err.print(myRank + " - NOT able to configure logger: " + mpiEx.getMessage());
            throw new RuntimeException(mpiEx);
        }
    }

    public static void createLogFile(String id) {
        createLogFile(id, "/log4j.properties");
    }
}
