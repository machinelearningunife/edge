/**
 *  This file is part of EDGE.
 * 
 *  EDGE is a probabilistic parameter learner for OWL 2 ontologies. 
 *  It learns the probabilistic values of the probabilistic axioms starting from a set of positive and negative axioms. 
 * 
 *  EDGE can be used both as module and as standalone.
 *  
 *  EDGE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package unife.edge;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import net.sf.javabdd.BDD;

/**
 *
 * @author Riccardo Zese <riccardo.zese@unife.it>
 */
public class EMTable {

    private class RowEl {

        private BDD key;
        private BigDecimal value;

        public RowEl(BDD key, BigDecimal value) {
            this.key = key;
            this.value = value;
        }

        public BDD getKey() {
            return key;
        }

        public void setKey(BDD key) {
            this.key = key;
        }

        public BigDecimal getValue() {
            return value;
        }

        public void setValue(BigDecimal value) {
            this.value = value;
        }

    }
    
    
    
    List<List<RowEl>> table;
    
    /**
     * @param lineNumber  number of boolean variables.
     */
    public EMTable(int lineNumber) {
        
        table = new ArrayList<List<RowEl>>();
        for (int i = 0; i < lineNumber; i++){
            table.add(new ArrayList<RowEl>());
        }
    }
    
    
    
    /**
     * Adds a node in the table.
     * @param node the node to add
     * @param value the probability of node
     */
    public void add_node(BDD node, BigDecimal value) {
        int index = node.var();
        table.get(index).add(new RowEl(node, value));
        
    }
    
    /**
     * Adds or replaces a node in the table. If the node is not contained 
     * in the input table call add_node, else replace the old value with the new ones.
     * 
     * @param node the node to add
     * @param value the probability of node
     */
    public void add_or_replace_node(BDD node, BigDecimal value) {
        int index = node.var();
        for (int i = 0; i < table.get(index).size(); i++){
            if (table.get(index).get(i).key.equals(node)){
                table.get(index).get(i).value = value;
                return;
            }
        }
        add_node(node, value);
    }

    /**
     * Returns the probability of a node in a table.
     * 
     * @param node the node to extract its probability
     * @return a BigDecimal containing the probability of node or null if node
     * is not contained in table
     */
    public BigDecimal get_value(BDD node) {
        int index = node.var();
        for (int i = 0; i < table.get(index).size(); i++){
            if (table.get(index).get(i).key.equals(node)){
                return table.get(index).get(i).value;
            }
        }
        return null;
    }
    
    /**
     * Sets the probability of a node in a table.
     * @param node the node to modify its probability
     * @param value probability to set
     * @return true if success, false if the node is not contained in table
     */
    public boolean set_value(BDD node, BigDecimal value) {
        int index = node.var();
        for (int i = 0; i < table.get(index).size(); i++){
            if (table.get(index).get(i).key.equals(node)){
                table.get(index).get(i).setValue(value);
                return true;
            }
        }
        return false;
    }
    
    public void clear(boolean clearRowsBefore) {
        if (clearRowsBefore) {
            for (List<RowEl> table1 : table) {
                table1.clear();
            }
        }
        table.clear();
    }
    
    
    public void clear() {
        clear(false);
    }

}
