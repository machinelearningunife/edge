/**
 *  This file is part of EDGE.
 * 
 *  EDGE is a probabilistic parameter learner for OWL 2 ontologies. 
 *  It learns the probabilistic values of the probabilistic axioms starting from a set of positive and negative axioms. 
 * 
 *  EDGE can be used both as module and as standalone.
 *  
 *  EDGE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package unife.edge.utilities;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import unife.bundle.utilities.BundleUtilities;

/**
 *
 * @author Riccardo Zese <riccardo.zese@unife.it>, Giuseppe Cota
 * <giuseppe.cota@unife.it>
 */
public class EDGEUtilities {

    public static double avgProbs(List<Double> probs) {
        double avg = 0;
        int num = 0;
        for (Double prob : probs) {
            avg += prob;
            num++;
        }

        return avg / num;
    }

    public static SortedSet<OWLAxiom> get_ax_filtered(OWLOntology probOntology) {
        return EDGEUtilities.get_ax_filtered(probOntology.getAxioms());
//        Set<OWLAxiom> to_return = probOntology.getAxioms();
//        for (OWLAxiom ax : to_return) {
//            if (!(ax.isOfType(AxiomType.ANNOTATION_ASSERTION)
//                    || (ax.isOfType(AxiomType.DECLARATION))
//                    || (ax.isOfType(AxiomType.DATA_PROPERTY_ASSERTION))
//                    || (ax.isOfType(AxiomType.OBJECT_PROPERTY_ASSERTION))
//                    || (ax.isOfType(AxiomType.CLASS_ASSERTION))
//                    || (ax.isOfType(AxiomType.DATATYPE_DEFINITION)))) {
//                to_return.remove(ax);
//            }
//        }
//        return to_return;

    }

    public static SortedSet<OWLAxiom> get_ax_filtered(Set<OWLAxiom> probAxioms) {
        SortedSet<OWLAxiom> to_return = new TreeSet<>();
        for (OWLAxiom ax : probAxioms) {
            if (!(ax.isOfType(AxiomType.ANNOTATION_ASSERTION)
                    || (ax.isOfType(AxiomType.DECLARATION))
                    || (ax.isOfType(AxiomType.DATA_PROPERTY_ASSERTION))
                    || (ax.isOfType(AxiomType.OBJECT_PROPERTY_ASSERTION))
                    || (ax.isOfType(AxiomType.CLASS_ASSERTION))
                    || (ax.isOfType(AxiomType.DATATYPE_DEFINITION)))) {
                to_return.add(ax);
            }
        }
        return to_return;
    }

    public static List<OWLAxiom> get_listOf_ax_filtered(OWLOntology probOntology) {
        return EDGEUtilities.get_listOf_ax_filtered(probOntology.getAxioms());
//        Set<OWLAxiom> to_return = probOntology.getAxioms();
//        for (OWLAxiom ax : to_return) {
//            if (!(ax.isOfType(AxiomType.ANNOTATION_ASSERTION)
//                    || (ax.isOfType(AxiomType.DECLARATION))
//                    || (ax.isOfType(AxiomType.DATA_PROPERTY_ASSERTION))
//                    || (ax.isOfType(AxiomType.OBJECT_PROPERTY_ASSERTION))
//                    || (ax.isOfType(AxiomType.CLASS_ASSERTION))
//                    || (ax.isOfType(AxiomType.DATATYPE_DEFINITION)))) {
//                to_return.remove(ax);
//            }
//        }
//        return to_return;

    }

    public static List<OWLAxiom> get_listOf_ax_filtered(Set<OWLAxiom> probAxioms) {

//        Comparator<OWLAxiom> comparator = new Comparator<OWLAxiom>() {
//
//            @Override
//            public int compare(OWLAxiom o1, OWLAxiom o2) {
//                String strAx1 = BundleUtilities.getManchesterSyntaxString(o1);
//                String strAx2 = BundleUtilities.getManchesterSyntaxString(o2);
//                return strAx1.compareTo(strAx2);
//            }
//
//        };

//        SortedSet<OWLAxiom> sortedSet = new TreeSet<>(comparator);
        SortedSet<OWLAxiom> sortedSet = new TreeSet<>();
        sortedSet.addAll(probAxioms);
        List<OWLAxiom> to_return = new ArrayList<>();
        for (OWLAxiom ax : sortedSet) {
            if (!(ax.isOfType(AxiomType.ANNOTATION_ASSERTION)
                    || (ax.isOfType(AxiomType.DECLARATION))
                    || (ax.isOfType(AxiomType.DATA_PROPERTY_ASSERTION))
                    || (ax.isOfType(AxiomType.OBJECT_PROPERTY_ASSERTION))
                    || (ax.isOfType(AxiomType.CLASS_ASSERTION))
                    || (ax.isOfType(AxiomType.DATATYPE_DEFINITION)))) {
                to_return.add(ax);
            }
        }
        return to_return;
    }

}
