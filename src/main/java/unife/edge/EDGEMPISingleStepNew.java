/**
 *  This file is part of EDGE.
 * 
 *  EDGE is a probabilistic parameter learner for OWL 2 ontologies. 
 *  It learns the probabilistic values of the probabilistic axioms starting from a set of positive and negative axioms. 
 * 
 *  EDGE can be used both as module and as standalone.
 *  
 *  EDGE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package unife.edge;

import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import mpi.Intracomm;
import mpi.MPI;
import mpi.MPIException;
import net.sf.javabdd.BDD;
import org.apache.log4j.Logger;
import org.mindswap.pellet.utils.Timers;
import org.semanticweb.owlapi.model.OWLAxiom;
import unife.bundle.logging.BundleLoggerFactory;
import unife.bundle.utilities.BundleUtilities;
import unife.edge.mpi.EDGEMPIConstants;
import static unife.edge.mpi.EDGEMPIConstants.*;
import unife.edge.mpi.MPIUtilities;
import unife.edge.mpi.RecvObjectStatus;
import unife.edge.mpi.UnexpectedSignalValueException;
import unife.edge.utilities.EDGEConstants.Type;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>, Riccardo Zese
 * <riccardo.zese@unife.it>
 */
public class EDGEMPISingleStepNew extends EDGE {

    private static Logger logger = Logger.getLogger(EDGEMPISingleStep.class.getName(),
            new BundleLoggerFactory());

    private final List<Map.Entry<OWLAxiom, Type>> initialExamples = new ArrayList<>(); // all the examples
    private List<List<Map.Entry<OWLAxiom, Type>>> examplesDistribution;
    // communicator of the MPI group
    private Intracomm comm = null;
    // rank inside the group
    private int myRank;
    // it's true if this process is master
    private boolean MASTER;

    public EDGEMPISingleStepNew() {

    }

    @Override
    public void init() {
        super.init();
        if (getComm() == null) {
            setComm(MPI.COMM_WORLD);
        }
        try {
            myRank = getComm().getRank();
            MASTER = MPIUtilities.isMaster(getComm());
        } catch (MPIException mpiEx) {
            String msg = "Impossible to get the Rank: " + mpiEx.getMessage();
            logger.error(msg);
            throw new RuntimeException(msg);
        }
    }

    protected void printTestResult(Timers timers, BigDecimal[] probs) {
        if (MASTER) {
            logger.info("");
            logger.info("============ Result ============");
            logger.info("");
            int index = 0;
            for (Map.Entry<OWLAxiom, Type> query : initialExamples) {
                logger.info("Query " + (index + 1) + " of " + initialExamples.size()
                        + " (" + (int) (((index + 1) * 100) / initialExamples.size()) + "%)");
                if (query.getValue() == Type.POSITIVE) {
                    logger.info("Positive Example: " + query.getKey());
                } else {
                    logger.info("Negative Example: " + query.getKey());
                }
                logger.info("Prob: " + probs[index]);
                index++;
            }
            logger.info("");
            logger.info("================================");
            logger.info("");
            StringWriter sw = new StringWriter();
            timers.print(sw, true, null);
            logger.info(sw);
        } else { // SLAVE
            super.printTestResult(timers, probs);
        }
    }

    public BigDecimal[] computeExamples(Map<OWLAxiom, BigDecimal> PMap,
            List<BDD> BDDs, boolean test) {
        if (MASTER) {
            // populate initialExamples list
            for (OWLAxiom pEx : getPositiveExamples()) {
                initialExamples.add(new AbstractMap.SimpleEntry<>(pEx, Type.POSITIVE));
            }
            for (OWLAxiom nEx : getNegativeExamples()) {
                initialExamples.add(new AbstractMap.SimpleEntry<>(nEx, Type.NEGATIVE));
            }

            logger.info(myRank + " - Start finding explanations for every example...");
            // Invio esempi in blocchi
            logger.info(myRank + " - Sending examples...");
            sendExamples();
            // compute examples (The master is a slave of himself)
            List<OWLAxiom> masterPositiveExamples = new ArrayList<>();
            List<OWLAxiom> masterNegativeExamples = new ArrayList<>();
            for (Map.Entry<OWLAxiom, Type> example : examplesDistribution.get(0)) {
                if (example.getValue() == Type.POSITIVE) {
                    masterPositiveExamples.add(example.getKey());
                } else {
                    masterNegativeExamples.add(example.getKey());
                }
            }
            setPositiveExamples(masterPositiveExamples);
            setNegativeExamples(masterNegativeExamples);
            int sum = getExamples().size();
            for (int i = 1; i < examplesDistribution.size(); i++) {
                sum += examplesDistribution.get(i).size();
            }
            if (sum != initialExamples.size()) {
                logger.error("Problem in examples: number of examples mismatch ( " + sum + " / " + initialExamples.size() + " )");
                throw new RuntimeException("Problem in examples: number of examples mismatch ( " + sum + " / " + initialExamples.size() + " )");
            }

            //double masterProbs[] = new double[examplesDistribution.get(0).size()];
            BigDecimal masterProbs[] = super.computeExamples(PMap, BDDs, test);
            for (int i = 0; i < masterProbs.length; i++) {
                logger.debug(myRank
                        + " - " + BundleUtilities.getManchesterSyntaxString(initialExamples.get(i).getKey())
                        + " - prob: " + masterProbs[i] + " - tag: "
                        + (i + 1) + " - #vars: " + boolVars_ex.get(i));
            }
            // ricevo i risultati
            BigDecimal[] probs = new BigDecimal[initialExamples.size()];
            receiveExamplesResults(probs, test);
            System.arraycopy(masterProbs, 0, probs, 0, masterProbs.length);
            return probs;
        } else { // SLAVE
            logger.info(myRank + " - Start finding explanations for every example...");
            // receive the queries from the master
            int[] queryTags = receiveExamples();
            logger.info(myRank + " - Example received...");
            BigDecimal[] probs = super.computeExamples(PMap, BDDs, test);
            // send probabilities of the queries to the master
            sendExamplesResults(queryTags, probs, test);
            return probs;
        }
    }

    private void sendExamples() {
        int numSlaves = 0;
        try {
            numSlaves = MPIUtilities.getSlaves(comm);
            logger.debug(myRank + " - I have " + numSlaves + " slaves..");
        } catch (MPIException ex) {
            logger.error(myRank + " - Error: " + ex);
            System.exit(-1);
        }
        // setting the distribution of the examples
        logger.debug(myRank + " - I have " + initialExamples.size() + " examples");
        examplesDistribution = new ArrayList<>(numSlaves + 1);
        List<Integer> numOfPosExamplesPerSlave = new ArrayList<>(numSlaves + 1);
        int chunkDim = initialExamples.size() / (numSlaves + 1);

        List<OWLAxiom> positiveExamples = getPositiveExamples();
        List<OWLAxiom> negativeExamples = getNegativeExamples();

        int numExtraLastSlaveExamples = initialExamples.size() % (numSlaves + 1);
        int pos = 0;
        for (int i = 0; i <= numSlaves; i++) {
            int numPosExamples = 0;
            List<Entry<OWLAxiom, Type>> nodeMapExamples = new ArrayList<>();
            for (int j = 0; j < chunkDim; j++) {
                if (pos < positiveExamples.size()) {
                    nodeMapExamples.add(new AbstractMap.SimpleEntry<>(positiveExamples.get(pos), Type.POSITIVE));
                    numPosExamples++;
                } else {
                    nodeMapExamples.add(new AbstractMap.SimpleEntry<>(negativeExamples.get(pos - positiveExamples.size()), Type.NEGATIVE));
                }
                pos++;
            }
            if (i == numSlaves) {
                for (int j = 0; j < numExtraLastSlaveExamples; j++) {
                    if (pos < positiveExamples.size()) {
                        nodeMapExamples.add(new AbstractMap.SimpleEntry<>(positiveExamples.get(pos), Type.POSITIVE));
                        numPosExamples++;
                    } else {
                        nodeMapExamples.add(new AbstractMap.SimpleEntry<>(negativeExamples.get(pos - positiveExamples.size()), Type.NEGATIVE));
                    }
                    pos++;
                }
            }
            numOfPosExamplesPerSlave.add(numPosExamples);
            examplesDistribution.add(nodeMapExamples);
        }
        logger.debug(myRank + " - setting up for " + (numSlaves + 1) + " chunk dimensions...");
        String array = "";
        for (int i = 0; i < examplesDistribution.size(); i++) {
            array += " " + examplesDistribution.get(i).size() + " ";
        }
        logger.info(myRank + " - [" + array + "]");
        List<byte[]> packets = new ArrayList<>(numSlaves);
        int[] dimPackets = new int[numSlaves];
        long totalBytes = 0;
        for (int i = 1; i <= numSlaves; i++) {
            List<Entry<OWLAxiom, Type>> slaveExamples = examplesDistribution.get(i);
            byte[] byteObj = MPIUtilities.objectToByteArray(slaveExamples);
            packets.add(byteObj);
            totalBytes += byteObj.length;
            dimPackets[i - 1] = byteObj.length;
        }

        if (totalBytes > Integer.MAX_VALUE) {
            // use alternative method send the examples
            dimPackets = null; // free some memory
            try {
                // set method to send examples
                MPIUtilities.sendSignal(ALL_BCAST, METHOD1, comm);
            } catch (MPIException mpiEx) {
                String msg = "Impossible to send the examples to the slaves: " + mpiEx.getMessage();
                logger.error(msg);
                throw new RuntimeException(msg);
            }
            // send the examples 
            for (int j = 1; j <= numSlaves; j++) {
                (new Thread(new ExamplesSender(j))).start();
            }
        } else {
            // create byte stream
            byte[] multiPackets = new byte[(int) totalBytes];
            int displ = 0;
            int[] displs = new int[numSlaves];
            for (int i = 0; i < numSlaves; i++) {
                byte[] byteObj = packets.get(i);
                System.arraycopy(byteObj, 0, multiPackets, displ, byteObj.length);
                displs[i] = displ;
                displ += byteObj.length;
            }
            // send the dimensions of each byte array to the slaves
            try {
                // set method to send examples
                MPIUtilities.sendSignal(ALL_BCAST, METHOD2, comm);
                // send the dimensions of each byte array to the slaves
                comm.scatter(dimPackets, dimPackets.length, MPI.INT, myRank);
                // send the examples
                comm.scatterv(multiPackets, dimPackets, displs, MPI.BYTE, myRank);
            } catch (MPIException mpiEx) {
                String msg = "Impossible to send the examples to the slaves: " + mpiEx.getMessage();
                logger.error(msg);
                throw new RuntimeException(msg);
            }
        }
    }

    private int[] receiveExamples() {
        logger.debug(myRank + " - Receiving examples...");
        int[] signal = new int[1];
        try {
            comm.bcast(signal, 1, MPI.INT, EDGEMPIConstants.MASTER);
            switch (signal[0]) {
                case METHOD1:
                    logger.info("Using method 1 to receive examples.");
                    return receiveExamplesMethod1();
                case METHOD2:
                    logger.info("Using method 2 to receive examples.");
                    return receiveExamplesMethod2();
                default:
                    throw new UnexpectedSignalValueException("Not a valid method to "
                            + "receive examples");
            }
        } catch (MPIException mpiEx) {
            String msg = "Cannot receive examples: " + mpiEx.getMessage();
            logger.error(msg);
            throw new RuntimeException(msg);
        }
    }

    private int[] receiveExamplesMethod1() throws MPIException {
        int[] dim = new int[2];
        comm.recv(dim, 2, MPI.INT, 0, 0);
        int[] queryTags = new int[dim[0]];
        logger.debug(myRank + " - I have " + dim[0] + " examples (" + dim[1] + " positives)...");

        OWLAxiom query;
        int numPosEx = dim[1];
        List<OWLAxiom> slavePositiveExamples = new ArrayList<>(numPosEx);
        List<OWLAxiom> slaveNegativeExamples = new ArrayList<>(dim[0] - numPosEx);
        for (int i = 0; i < dim[0]; i++) {
            logger.debug(myRank + " - Receiving " + (i + 1) + "-th example...");
            RecvObjectStatus stat = MPIUtilities.recvObject(0, MPI.ANY_TAG, comm);
            query = (OWLAxiom) stat.getObj();
            logger.debug(myRank + " - " + query);
            if (i < numPosEx) {
                slavePositiveExamples.add(query);
            } else {
                slaveNegativeExamples.add(query);
            }
            logger.debug(myRank + " - " + stat.getStatus().getTag() + ": " + query);
            queryTags[i] = stat.getStatus().getTag();
//                vars_ex.add(new ArrayList<Integer>());
//                probs_ex.add(new ArrayList<Double>());
//                boolVars_ex.add(0);
            //MPIUtilities.sendSignal(0, MPIUtilities.CONTINUE);
        }
        setPositiveExamples(slavePositiveExamples);
        setNegativeExamples(slaveNegativeExamples);
        return queryTags;
    }

    private int[] receiveExamplesMethod2() throws MPIException {
        int[] dimPacket = new int[1];
        comm.scatter(dimPacket, 1, MPI.INT, EDGEMPIConstants.MASTER);
        byte[] byteObj = new byte[dimPacket[0]];
        comm.scatterv(byteObj, dimPacket[0], MPI.BYTE, EDGEMPIConstants.MASTER);
    }

    /**
     * @return the comm
     */
    public Intracomm getComm() {
        return comm;
    }

    /**
     * @param comm the comm to set
     */
    public void setComm(Intracomm comm) {
        this.comm = comm;
    }

    @Override
    protected boolean checkEMConditions(BigDecimal diffLL, BigDecimal ratioLL, long iter) {
        if (MASTER) {
            if ((diffLL.compareTo(this.getDiffLL()) > 0) && (ratioLL.compareTo(this.getRatioLL()) > 0)
                    && (iter < this.getMaxIterations())) {
                try {
                    logger.debug(myRank + " - Cycle " + iter + " : Continue...");
                    MPIUtilities.sendSignal(ALL_BCAST, CONTINUE, comm);

                } catch (MPIException ex) {
                    logger.fatal(ex);
                    System.exit(-2);
                }
                return true;
            } else {
                try {
                    logger.debug(myRank + " - Cycle " + iter + " : Stop...");
                    MPIUtilities.sendSignal(ALL_BCAST, STOP, comm);

                } catch (MPIException ex) {
                    logger.fatal(ex);
                    System.exit(-2);
                }
                return false;
            }
        } else { // slave
            int[] signal = new int[1];
            try {
                logger.debug(myRank + " - Waiting for signal...");
                // MAYBE NOT BROADCAST
                //comm.recv(signal, 1, MPI.INT, 0, 0);
                comm.bcast(signal, 1, MPI.INT, 0);
                logger.debug(myRank + " - Received: " + signal[0]);
            } catch (MPIException ex) {
                logger.error(myRank + " - Error: " + ex);
                throw new RuntimeException(ex);
            }
            boolean continueEM = (signal[0] == CONTINUE);
            logger.debug(myRank + " - run flag: " + continueEM);
            return continueEM;
        }
    }

    @Override
    protected void printLearningResult(Timers timers) { //, BDDFactory.CacheStats fCacheStats, BDDFactory.GCStats fGCStats, BDDFactory.ReorderStats fReorderStats) {
        if (MASTER) {
            super.printLearningResult(timers); //, fCacheStats, fGCStats, fReorderStats);
        }
    }

    private class ExamplesSender implements Runnable {

        private final int slave;

        public ExamplesSender(int slave) {
            super();
            this.slave = slave;
        }

        @Override
        public void run() {
            try {
                //int posExamples = positiveExamples.size();
                int pos = 0;
                int numPosExamples = 0;
                for (int i = 0; i < slave; i++) {
                    pos += examplesDistribution.get(i).size();
                }

                for (Map.Entry<OWLAxiom, Type> entry : examplesDistribution.get(slave)) {
                    if (entry.getValue() == Type.POSITIVE) {
                        numPosExamples++;
                    }
                }

                int numSlaveExamples = examplesDistribution.get(slave).size();
                logger.debug(myRank + " - send to " + slave + " " + numSlaveExamples
                        + " examples (positive " + numPosExamples + " negative " + (numSlaveExamples - numPosExamples) + "):");
                int[] dimt = {numSlaveExamples, numPosExamples};
                comm.send(dimt, 2, MPI.INT, slave, 0);

                for (Map.Entry<OWLAxiom, Type> entry : examplesDistribution.get(slave)) {
                    logger.debug(myRank + " - example (array pos.: " + pos + " - tag: " + (pos + 1) + " )");
                    MPIUtilities.sendObject(entry.getKey(), slave, pos + 1, comm);
                    pos++;

//                    int[] signal = new int[1];
//                    try {
//                        comm.recv(signal, 1, MPI.INT, j, 0);
//                    } catch (MPIException ex) {
//                        logger.error("");
//                    }
//                    boolean runFlag = (signal[0] == MPIUtilities.CONTINUE);
//                    logger.debug(myRank + " - runFlag = " + runFlag);
                }

            } catch (MPIException ex) {
                logger.fatal(myRank + " - Error: " + ex);
                throw new RuntimeException(ex);
            } catch (Exception ex) {
                logger.error(myRank + " - Error: " + ex);
                throw new RuntimeException(ex);
            }
        }

    }

}
