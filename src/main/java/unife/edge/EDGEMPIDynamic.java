/**
 *  This file is part of EDGE.
 * 
 *  EDGE is a probabilistic parameter learner for OWL 2 ontologies. 
 *  It learns the probabilistic values of the probabilistic axioms starting from a set of positive and negative axioms. 
 * 
 *  EDGE can be used both as module and as standalone.
 *  
 *  EDGE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package unife.edge;

import com.clarkparsia.owlapiv3.OWL;
import static com.google.common.primitives.Ints.toArray;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import mpi.Intracomm;
import mpi.MPI;
import mpi.MPIException;
import mpi.Status;
import net.sf.javabdd.BDD;
import org.apache.log4j.Logger;
import org.mindswap.pellet.utils.Timers;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClassAssertionAxiom;
import org.semanticweb.owlapi.model.OWLException;
import unife.bundle.Bundle;
import unife.bundle.QueryResult;
import unife.bundle.logging.BundleLoggerFactory;
import unife.bundle.utilities.BundleUtilities;
import unife.edge.mpi.EDGEMPIConstants;
import static unife.edge.mpi.EDGEMPIConstants.*;
import unife.edge.mpi.MPIUtilities;
import unife.edge.mpi.RecvObjectStatus;
import unife.edge.mpi.UnexpectedSignalValueException;
import unife.math.utilities.MathUtilities;

/**
 * This file is part of LEAP.
 *
 * LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, but
 * some components can be used as stand-alone.
 *
 * LEAP is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * LEAP is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>, Riccardo Zese
 * <riccardo.zese@unife.it>
 */
public class EDGEMPIDynamic extends EDGE {

    // TO DO inviare chunk di dimensione variabile
    // number of example for chunk
    private int chunkDim = 1;
    // max number of concurrent threads which send examples to the slaves
    private int maxSenderThreads = 0;
    private ExecutorService executor;
    private int numSlaves = 0;

    private Intracomm comm = MPI.COMM_WORLD;

    /**
     * @return the maxSenderThreads
     */
    public int getMaxSenderThreads() {
        return maxSenderThreads;
    }

    /**
     * @param maxSenderThreads the maxSenderThreads to set
     */
    public void setMaxSenderThreads(int maxSenderThreads) {
        this.maxSenderThreads = maxSenderThreads;
    }

    private enum Type {

        POSITIVE, NEGATIVE
    }

    // max number of attempts to send an example
    private final int maxNumAttempts = 3;
    private int countExamples = 0;
    //private int countComputedExamples = 0; // forse si può rimuovere
    private final Object countExamplesLock = new Object();

    private final Object countActuallySentExamplesLock = new Object();
    private int countActuallySentExamples = 0;

    private boolean MASTER; // if true the current process is the master
    private int myRank; // rank of the current process (0 is master)
    private static Logger logger = Logger.getLogger(EDGEMPIDynamic.class.getName(),
            new BundleLoggerFactory());

    private boolean[] usedAxiomsOld;
    //private final List<Map.Entry<OWLAxiom, Type>> initialExamples = new ArrayList<>();
    private List<Map.Entry<OWLAxiom, Type>> initialExamples;// = Collections.synchronizedList(new ArrayList<Map.Entry<OWLAxiom, Type>>()); // all the starting examples
    private List<List<Map.Entry<OWLAxiom, Type>>> examplesDistribution; // examples assiociated to each slave

    public EDGEMPIDynamic() {
    }

    @Override
    public void init() {
        super.init();
//        if (getComm() == null) {
//            setComm(MPI.COMM_WORLD);
//        }
        countExamples = 0;
        countActuallySentExamples = 0;
        try {
            myRank = getComm().getRank();
            MASTER = MPIUtilities.isMaster(getComm());
        } catch (MPIException mpiEx) {
            String msg = "Impossible to get the Rank: " + mpiEx.getMessage();
            logger.error(msg);
            throw new RuntimeException(msg);
        }
        usedAxiomsOld = new boolean[usedAxioms.length];
        System.arraycopy(usedAxioms, 0, usedAxiomsOld, 0, usedAxioms.length);
        if (MASTER) {

            if (getMaxSenderThreads() == 0) {
                setMaxSenderThreads(Runtime.getRuntime().availableProcessors() - 1);
                if (getMaxSenderThreads() == 0) {
                    setMaxSenderThreads(1);
                }
            }
            executor = Executors.newFixedThreadPool(maxSenderThreads);
            initialExamples = Collections.synchronizedList(new ArrayList<Map.Entry<OWLAxiom, Type>>());
//            // populate initalExamples list
            for (OWLAxiom pEx : getPositiveExamples()) {
                initialExamples.add(new AbstractMap.SimpleEntry<>(pEx, Type.POSITIVE));
            }
            for (OWLAxiom nEx : getNegativeExamples()) {
                initialExamples.add(new AbstractMap.SimpleEntry<>(nEx, Type.NEGATIVE));
            }
            try {
                numSlaves = MPIUtilities.getSlaves(comm);
            } catch (MPIException mpiEx) {
                String msg = "Cannot get the current number of slaves";
                logger.error(msg);
                throw new RuntimeException(mpiEx);
            }
            int minNumSlaves = (int) Math.ceil((double) getExamples().size() / chunkDim) - 1;
            logger.debug(myRank + " - Sending signal to the slaves");
            if (numSlaves > minNumSlaves) {
                String msg = "The number of slave exceeds the number"
                        + " Aborting...";
                logger.warn(myRank + " - " + msg);
                try {
                    MPIUtilities.sendSignal(ALL, TERMINATE, comm);
                    MPI.Finalize();
                } catch (MPIException ex) {
                    msg = "Impossible to kill the unnecessary slaves.";
                    logger.error(msg);
                    throw new RuntimeException(msg);
                }
                System.exit(1);
//                logger.warn(myRank + " - The number of slave exceeds the number"
//                        + " of examples. Killing some slaves.");
//                try {
//                    int i = 1;
//                    for (; i <= minNumSlaves; i++) {
//                        MPIUtilities.sendSignal(i, START, comm);
//                    }
//                    for (; i <= numSlaves; i++) {
//                        MPIUtilities.sendSignal(i, TERMINATE, comm);
//                    }
//                    numSlaves = minNumSlaves;
//                } catch (MPIException mpiEx) {
//                    String msg = "Impossible to kill the unnecessary slaves.";
//                    logger.error(msg);
//                    throw new RuntimeException(msg);
//                }
            } else {
                try {
                    logger.debug(myRank + " - Sending START signal to all the slaves.");
                    MPIUtilities.sendSignal(ALL, START, comm);
                } catch (MPIException mpiEx) {
                    logger.error(myRank + " - " + mpiEx.getMessage());
                    throw new RuntimeException(mpiEx);
                }
            }
            logger.debug(myRank + " - signal sent");
        } else { // SLAVE
            int[] signal = new int[1];
            try {
                logger.debug(myRank + " - Waiting for START signal...");
                comm.recv(signal, 1, MPI.INT, EDGEMPIConstants.MASTER, 0);
            } catch (MPIException mpiEx) {
                logger.fatal(myRank + " - Some problem during the receiving of the signal: " + mpiEx.getMessage());
                throw new RuntimeException(mpiEx);
            }
            if (signal[0] == TERMINATE) {
                logger.info(myRank + " - Unnecessary slave. Terminate.");
                try {
                    MPI.Finalize();
                } catch (MPIException mpiEx) {
                    logger.error(mpiEx.getMessage());
                    throw new RuntimeException(mpiEx.getMessage());
                }
                System.exit(0);
            } else if (signal[0] == START) {
                logger.info(myRank + " - Starting...");
            } else {
                String msg = "Unexpected signal value " + signal[0];
                logger.fatal(msg);
                throw new UnexpectedSignalValueException(msg);
            }
        }
    }

    @Override
    protected void printTestResult(Timers timers, BigDecimal[] probs) {
        if (MASTER) {
            logger.info("");
            logger.info("============ Result ============");
            logger.info("");
            int index = 0;
            for (Map.Entry<OWLAxiom, Type> query : initialExamples) {
                logger.info("Query " + (index + 1) + " of " + initialExamples.size()
                        + " (" + (int) (((index + 1) * 100) / initialExamples.size()) + "%)");
                if (query.getValue() == Type.POSITIVE) {
                    logger.info("Positive Example: " + query.getKey());
                } else {
                    logger.info("Negative Example: " + query.getKey());
                }
                logger.info("Prob: " + probs[index]);
                index++;
            }
            logger.info("");
            logger.info("================================");
            logger.info("");
            StringWriter sw = new StringWriter();
            timers.print(sw, true, null);
            logger.info(sw);
        } else { // SLAVE
//            super.printTestResult(timers, probs);
        }
    }

    // TO DO: AGIRE QUI
    @Override
    public BigDecimal[] computeExamples(Map<OWLAxiom, BigDecimal> PMap, List<BDD> BDDs, boolean test) {
        BigDecimal[] probs;
        if (MASTER) {
//            // populate initalExamples list
//            for (OWLAxiom pEx : getPositiveExamples()) {
//                initialExamples.add(new AbstractMap.SimpleEntry<>(pEx, Type.POSITIVE));
//            }
//            for (OWLAxiom nEx : getNegativeExamples()) {
//                initialExamples.add(new AbstractMap.SimpleEntry<>(nEx, Type.NEGATIVE));
//            }

            try {
                logger.debug(myRank + " - Start finding explanations for every example...");
                // Send an example to each slave + itself
                examplesDistribution = new ArrayList<>(numSlaves + 1);
                examplesDistribution.add(new ArrayList<Map.Entry<OWLAxiom, Type>>());
                List<Map.Entry<OWLAxiom, Type>> examplesChunk;
//                int minNumSlaves = (int) Math.ceil((double) initialExamples.size() / chunkDim) - 1;
//                if (numSlaves > minNumSlaves) {
//                    logger.warn(myRank + " - The number of slave exceeds the number"
//                            + " of examples");
//                    // soluzione inefficiente, bisognerebbe trovare un modo alternativo
//                    // per uccidere i processi in eccesso, magari con un signal
//                    int i = 1;
//                    for (; i <= minNumSlaves; i++) {
//                        MPIUtilities.sendSignal(i, START, comm);
//                    }
//                    for (; i <= numSlaves; i++) {
//                        MPIUtilities.sendSignal(i, TERMINATE, comm);
//                    }
//                    numSlaves = minNumSlaves;
//                }
                // send an example to each slave
                for (int i = 1; i <= numSlaves; i++) {
                    examplesChunk = new ArrayList<>(chunkDim);
                    for (int j = 0; j < chunkDim; j++) {
                        Map.Entry<OWLAxiom, Type> example = nextExample();
                        if (example != null) {
                            examplesChunk.add(example);
                        } else {
                            break;
                        }
                    }
                    //countComputedExamples++;
                    executor.execute(new Thread(new ExampleSender(i, examplesChunk, i)));
                    examplesDistribution.add(new ArrayList<Map.Entry<OWLAxiom, Type>>());
                    examplesDistribution.get(i).addAll(examplesChunk);
                }
                // create array of the example probabilities
                probs = new BigDecimal[initialExamples.size()];
                // num of examples sent to the slaves
                int sentExamples = countExamples;
                // reserve a chunk of examples for the current thread
                examplesChunk = new ArrayList<>(chunkDim);
                for (int j = 0; j < chunkDim; j++) {
                    Map.Entry<OWLAxiom, Type> example = nextExample();
                    if (example != null) {
                        examplesChunk.add(example);
                    } else {
                        break;
                    }
                }
                examplesDistribution.get(EDGEMPIConstants.MASTER).addAll(examplesChunk);
                Thread exampleServer = null;
                if (numSlaves > 0) {
                    // create server
                    Runnable listener = new ExamplesListener(probs, sentExamples, test);
                    exampleServer = new Thread(listener);
                    // start sever
                    exampleServer.start();
                }
                // Master local computation of the examples
                while (!examplesChunk.isEmpty()) {
                    // computo esempi
                    for (Map.Entry<OWLAxiom, Type> example : examplesChunk) {
                        int idxExample = initialExamples.indexOf(example);
                        logger.debug(myRank + " - Computing query " + (idxExample + 1)
                                + " of " + initialExamples.size());
                        QueryResult queryResult = computeExample(PMap, example);
                        logger.debug(myRank + " - " + BundleUtilities.getManchesterSyntaxString(example.getKey())
                                + " - prob: " + queryResult.getQueryProbability()
                                + " - tag: " + (idxExample + 1)
                                + " - #vars: " + queryResult.getProbAxioms().size());
                        // set results
                        probs[idxExample] = queryResult.getQueryProbability();
//                    vars_ex.add(new ArrayList<Integer>());
//                    probs_ex.add(new ArrayList<Double>());
//                    boolVars_ex.add(0);
                        if (!test) {
                            BDDs.add(queryResult.getBDD());
                            super.init_bdd(queryResult);
                        }
                    }
                    synchronized (countExamplesLock) {
                        logger.debug(myRank + " - T_1");
                        logger.debug(myRank + " - Computed queries: " + countExamples
                                + " of " + initialExamples.size()
                                + " (" + ((int) (countExamples * 100 / initialExamples.size())) + "%)");
//                        if (countExamples < initialExamples.size()) {
                        //countComputedExamples++;
                        examplesChunk = new ArrayList<>(chunkDim);
                        for (int j = 0; j < chunkDim; j++) {
                            Map.Entry<OWLAxiom, Type> example = nextExample();
                            if (example != null) {
                                examplesChunk.add(example);
                            } else {
                                break;
                            }
                        }
                        if (!examplesChunk.isEmpty()) {
                            examplesDistribution.get(EDGEMPIConstants.MASTER).addAll(examplesChunk);
                        }
//                        }
                    }
                }
                if (exampleServer != null) {
                    exampleServer.join();
                    executor.shutdown();
                }
//            } catch (MPIException mpiEx) {
//                logger.error("Some problem during the sending of the examples: " + mpiEx);
//                throw new RuntimeException(mpiEx);
            } catch (InterruptedException iEx) {
                logger.error("Some problem during the sending of the examples: " + iEx);
                throw new RuntimeException(iEx);
            }

        } else { // SLAVE
            List<BigDecimal> probExamples = new ArrayList<>();
            //List<Integer> queryTags = new ArrayList<>();
            int receivedExamples = 0;
            boolean stop = false;
            int firstExampleTag = -1;

            int attempt = 1;
            do {
                List<BigDecimal> queryProbabilities = new ArrayList<>(chunkDim);
                try {
                    // listen to the MASTER (of puppets)
                    logger.debug(myRank + " - Receiving example...");
                    // check if is a stop
                    Status recvStat = comm.probe(EDGEMPIConstants.MASTER, MPI.ANY_TAG);
                    if (recvStat.getTag() == 0) {
                        int[] signal = new int[1];
                        try {
                            comm.recv(signal, 1, MPI.INT, EDGEMPIConstants.MASTER, 0);
                        } catch (MPIException mpiEx) {
                            logger.fatal("Some problem during the receiving of the signal: " + mpiEx.getMessage());
                            throw new RuntimeException(mpiEx);
                        }
                        if (signal[0] == STOP) {
                            stop = true;
//                        } else if (signal[0] == TERMINATE) {
//                            logger.info(myRank + " - Unnecessary slave. Terminate.");
//                            System.exit(0);
//                        } else if (signal[0] == START) {
//                            logger.info(myRank + " - Waiting to receive examples...");
                        } else {
                            String msg = "Unexpected signal value " + signal[0];
                            logger.fatal(msg);
                            throw new UnexpectedSignalValueException(msg);
                        }

                    } else if (!stop) {
                        // receive the example
                        RecvObjectStatus stat = MPIUtilities.recvObject(EDGEMPIConstants.MASTER, MPI.ANY_TAG, comm);
                        List<AbstractMap.SimpleEntry<OWLAxiom, Type>> examplesChunk = (List<AbstractMap.SimpleEntry<OWLAxiom, Type>>) stat.getObj();

                        // compute example/query
                        firstExampleTag = stat.getStatus().getTag();
                        //queryTags.add(tag);
                        logger.debug(myRank + " - Received queries chunk: " + examplesChunk
                                //            + " Type " + (examplesChunk.getValue() == Type.POSITIVE ? "positive" : "negative")
                                + " Local: " + (receivedExamples + 1) + " Global: " + firstExampleTag);
                        receivedExamples += examplesChunk.size();
                        for (Map.Entry<OWLAxiom, Type> example : examplesChunk) {
                            QueryResult queryResult = computeExample(PMap, example);
                            // set results
                            probExamples.add(queryResult.getQueryProbability());
                            queryProbabilities.add(queryResult.getQueryProbability());
                            if (!test) {
                                BDDs.add(queryResult.getBDD());
                                super.init_bdd(queryResult);
                            }
                        }
                        try {
                            // send probability of the query to the master
                            sendExampleResults(firstExampleTag, queryProbabilities, test);
                        } catch (MPIException mpiEx) {
                            logger.error("Some problem during the sending of the results: " + mpiEx.getMessage());
                            throw new RuntimeException(mpiEx);
                        } catch (Exception ex) {
                            logger.error("Some problem during the sending of the results: " + ex.getMessage());
                            throw new RuntimeException(ex);
                        }
                    }

                } catch (MPIException mpiEx) {
                    logger.error("problem during the receiving of the example. Attempt: " + attempt);
                    if (attempt > maxNumAttempts) {
                        logger.fatal("Not recoverable problem during the receiving of the example: " + mpiEx.getMessage());
                        throw new RuntimeException(mpiEx);
                    }
                    logger.error("Retrying receiving the example");
                    attempt++;
                }

            } while (!stop);

            // convert List to array of BigDecimal
            probs = new BigDecimal[probExamples.size()];
            for (int i = 0; i < probExamples.size(); i++) {
                probs[i] = probExamples.get(i);
            }
        }
        return probs; //temp, just for removing error
    }

    private QueryResult computeExample(Map<OWLAxiom, BigDecimal> PMap, Map.Entry<OWLAxiom, Type> example) {
        Bundle bundle = startBundle();
        bundle.setPMap(PMap);
        QueryResult queryResult = null;
        OWLAxiom query = example.getKey();
        try {
            if (example.getValue() == Type.POSITIVE) {
                logger.debug(myRank + " - Positive Example: " + BundleUtilities.getManchesterSyntaxString(query));
                queryResult = bundle.computeQuery(query);
            } else { // Type.NEGATIVE
                // converting axiom
                OWLAxiom notQuery = OWL.classAssertion(((OWLClassAssertionAxiom) query).getIndividual(),
                        OWL.not(((OWLClassAssertionAxiom) query).getClassExpression()));
                // computing not axiom
                logger.debug(myRank + " - Negative Example: " + BundleUtilities.getManchesterSyntaxString(notQuery));
                queryResult = bundle.computeQuery(notQuery);
                // second method
                if (queryResult == null || queryResult.getExplanations().isEmpty()) {
                    logger.debug("Trying the second method...");
                    queryResult = bundle.computeQuery(query);
                    queryResult.setBDD(queryResult.getBDD().not());
                    queryResult.setQueryProbability(MathUtilities.getBigDecimal("1", getAccuracy()).subtract(queryResult.getQueryProbability()));
                }
            }
        } catch (OWLException owle) {
            logger.error("It was impossible to compute the probability of the query");
            logger.error(owle.getMessage());
            logger.trace(owle.getStackTrace());

            System.err.println("Terminated because of an internal error. See the log for details");
            throw new RuntimeException(owle);
        }

        if (queryResult == null) {
            logger.fatal("The result of BUNDLE algorithm is null");
            throw new RuntimeException("Impossible to compute the query");
        }
        bundle.finish();
        bundle = null;
        return queryResult;
    }

    private void sendExampleResults(int firstExampleTag, List<BigDecimal> queryProbabilities, boolean test) throws MPIException, Exception {
        // send example probability and the number of used variables
        // packing
        byte[] bufferProb = MPIUtilities.objectToByteArray(queryProbabilities);
        byte[] buffer = new byte[bufferProb.length + Integer.SIZE / 8];
        int pos = comm.pack(bufferProb, bufferProb.length, MPI.BYTE, buffer, 0);
        if (!test) {
            // generate the list of indices of used axioms to compute the chunk
            // of examples
            List<Integer> chunkVars_ex = new ArrayList<>();
            for (int i = 0; i < usedAxioms.length; i++) {
                if (usedAxioms[i] != usedAxiomsOld[i]) {
                    chunkVars_ex.add(i);
                    usedAxiomsOld[i] = usedAxioms[i];
                }
            }

            int numUsedAxioms = chunkVars_ex.size();
            comm.pack(new int[]{numUsedAxioms}, 1, MPI.INT, buffer, pos);
            logger.debug(myRank
                    + " - probs: " + queryProbabilities
                    + " - number of vars: " + numUsedAxioms
                    + " - tag: " + firstExampleTag);
            // sending
            comm.send(buffer, buffer.length, MPI.PACKED, EDGEMPIConstants.MASTER, firstExampleTag);
            // send indices and probabilities of the used vars (boolean variables <-> axioms)
            if (numUsedAxioms > 0) {
                logger.debug(myRank + " - Sending vars values...");
                // creating and populating buffers
                //int[] indexVars = new int[numUsedAxioms];
                int[] indexVars = toArray(chunkVars_ex);
//                for (int i = 0; i < chunkVars_ex.size(); i++) {
//                    indexVars[i] = chunkVars_ex.get(i);
//                }
                // sending indices of the used axioms
                comm.send(indexVars, indexVars.length, MPI.INT, EDGEMPIConstants.MASTER, firstExampleTag);
            }
        } else {
            logger.debug(myRank + "- probs: " + queryProbabilities + " - tag: " + firstExampleTag);
            // sending only the probabilities
            comm.send(buffer, buffer.length, MPI.PACKED, EDGEMPIConstants.MASTER, firstExampleTag);
        }

    }

    @Override
    protected EDGEStatImpl EM(List<BDD> BDDs, BigDecimal[] probs) {
        logger.info(myRank + " - Start EM Algorithm\n\t- n. of probabilistic axioms:\t" + probAxioms.size()
                + "\n\t- n. of examples:\t\t" + getExamples().size());
        //+ "\n\t- n. of examples:\t\t" + initialExamples.size());
        int lenNodes = BDDs.size();
        int nRules = probAxioms.size();
        BigDecimal CLL0; //= MathUtilities.getBigDecimal(-2.2 * Math.pow(10, 10), getAccuracy());  // -inf
        BigDecimal CLL1 = MathUtilities.getBigDecimal(-1.7 * Math.pow(10, 8), getAccuracy());    // +inf
        BigDecimal ratio, diff;
        int cycle = 0;

        List<BigDecimal[]> eta = new ArrayList<>();
        for (int i = 0; i < nRules; i++) {
            BigDecimal[] etaIntTemp = new BigDecimal[2];
            etaIntTemp[0] = MathUtilities.getBigDecimal("0", getAccuracy());
            etaIntTemp[1] = MathUtilities.getBigDecimal("0", getAccuracy());
            eta.add(etaIntTemp);
        }

        if (getExample_weights() == null || getExample_weights().length == 0 
                || getExample_weights().length != lenNodes) {
            if (getExample_weights() != null && getExample_weights().length != 0
                    && getExample_weights().length != lenNodes) {
                logger.warn("The size of example weights is diffrent from the number of BDDs...");
                logger.warn("Creating a new array of examples weigths and setting all the values to 1");
            }
            BigDecimal[] example_weights_local = new BigDecimal[lenNodes];
            for (int i = 0; i < lenNodes; i++) {
                example_weights_local[i] = MathUtilities.getBigDecimal("1.0", getAccuracy());
            }
            setExample_weights(example_weights_local);
        }

//        diff = CLL1.subtract(CLL0);
//        ratio = diff.divide(CLL0.abs(), getAccuracy(),  RoundingMode.HALF_UP);
//        logger.debug(myRank + " - probabilistic values before EM cycle"
//                + ": " + arrayProb);
        // EM cycle
        logger.debug(myRank + " - Starting EM cycle");
        do {
            List<BigDecimal[]> eta_local = new ArrayList<>(eta.size());
            cycle++;
            logger.debug(myRank + " - EM cycle: " + cycle);
            CLL0 = CLL1;
            CLL1 = expectation(BDDs, getExample_weights(), eta, lenNodes, eta_local, cycle);
            maximization(eta_local);
            diff = CLL1.subtract(CLL0);
            ratio = diff.divide(CLL0.abs(), getAccuracy(), RoundingMode.HALF_UP);
            logger.debug("Log-likelihood: " + CLL1 + " cycle: " + cycle);
        } while (checkEMConditions(diff, ratio, cycle));

        logger.info("EM completed.");
        logger.info("\n Final Log-likelihood: " + CLL1);

        return new EDGEStatImpl(cycle, CLL1, probAxioms, arrayProb);
    }

    protected BigDecimal expectation(List<BDD> nodes_ex, BigDecimal[] example_weights,
            List<BigDecimal[]> eta, int lenNodes, List<BigDecimal[]> eta_local, int cycle) {
        logger.debug(myRank + " - Computing Expectation");
        int nRules = probAxioms.size();
        // slaves and master compute the expectetion step
        logger.debug(myRank + " - Computing Expectation step locally. #BDDs: " + nodes_ex.size() + " Cycle: " + cycle);
        BigDecimal LL = super.expectation(nodes_ex, example_weights, eta, lenNodes);

        if (MASTER) {
            if (cycle == 1) {
                try {
                    // in the first iteration the master must send the START signal
                    // to the slaves in order to start to receive the expectation results
                    MPIUtilities.sendSignal(ALL_BCAST, START, comm);
                } catch (MPIException mpiEx) {
                    logger.error(myRank + " - Error: " + mpiEx.getMessage());
                    throw new RuntimeException(mpiEx);
                }
            }
            logger.debug(myRank + " - Receiving expectation results...");

            // set local etas
            for (BigDecimal[] eta_i : eta) {
                BigDecimal[] eta_i_local = {eta_i[0], eta_i[1]};
                eta_local.add(eta_i_local);
            }
            //receive the results
            for (int i = 1; i <= numSlaves; i++) {
                try {
                    // eta + LL_i
                    Status status = comm.probe(i, 0);
                    byte[] bufferRecvTot = new byte[status.getCount(MPI.PACKED)];
                    comm.recv(bufferRecvTot, bufferRecvTot.length,
                            MPI.PACKED, i, 0);
                    int[] dimEtasBuffer = new int[1];
                    int pos = comm.unpack(bufferRecvTot, 0, dimEtasBuffer, 1, MPI.INT);
                    byte[] etasBuffer = new byte[dimEtasBuffer[0]];
                    pos = comm.unpack(bufferRecvTot, pos,
                            etasBuffer, dimEtasBuffer[0], MPI.BYTE);
                    List<List<BigDecimal>> etasRemote = (List<List<BigDecimal>>) MPIUtilities.byteArrayToObject(etasBuffer);
                    byte[] bufferLL = new byte[bufferRecvTot.length - etasBuffer.length - (Integer.SIZE / 8)];
                    comm.unpack(bufferRecvTot, pos, bufferLL, bufferLL.length, MPI.BYTE);
                    BigDecimal recvLL = (BigDecimal) MPIUtilities.byteArrayToObject(bufferLL);

                    for (int j = 0; j < nRules; j++) {
                        eta_local.get(j)[0] = eta_local.get(j)[0].add(etasRemote.get(j).get(0));
                        eta_local.get(j)[1] = eta_local.get(j)[1].add(etasRemote.get(j).get(1));
                    }

                    logger.debug(myRank + " - LL from " + i + "-th slave: " + recvLL);
                    LL = LL.add(recvLL);
                    logger.debug(myRank + " - LL after " + i + "-th slave: " + LL);

                } catch (MPIException mpiEx) {
                    logger.error(myRank + " - Error: " + mpiEx.getMessage());
                    throw new RuntimeException(mpiEx);
                }

            }
        } else { // SLAVE
            try {
                // in the first iteration the slave must wait the START signal
                // before sending the results of the expectation step
                if (cycle == 1) {
                    int[] signal = new int[1];
                    comm.bcast(signal, 1, MPI.INT, EDGEMPIConstants.MASTER);
                    if (signal[0] != START) {
                        String msg = "Unexpected signal value " + signal[0];
                        logger.fatal(msg);
                        throw new UnexpectedSignalValueException(msg);
                    }
                }

                int nEtas = nRules * 2;
                List<List<BigDecimal>> etas = new ArrayList<>();
                // setting buffer to be sent
                for (int idxAxiom = 0; idxAxiom < nRules; idxAxiom++) {
                    List<BigDecimal> etasj = new ArrayList<>();
                    etasj.add(eta.get(idxAxiom)[0]);
                    etasj.add(eta.get(idxAxiom)[1]);
                    etas.add(etasj);
                }
                logger.debug(myRank + " - Sending expectation results...");
                byte[] etasBuffer = MPIUtilities.objectToByteArray(etas);
                byte[] LLBuffer = MPIUtilities.objectToByteArray(LL);
                byte[] sendBuffer = new byte[(Integer.SIZE / 8) + etasBuffer.length + LLBuffer.length];
                int pos = comm.pack(new int[]{etasBuffer.length}, 1, MPI.INT, sendBuffer, 0);
                pos = comm.pack(etasBuffer, etasBuffer.length, MPI.BYTE, sendBuffer, pos);
                comm.pack(LLBuffer, LLBuffer.length, MPI.BYTE, sendBuffer, pos);
                logger.debug(myRank + " - Sending etas + LL (" + (nEtas + 1) + " values)");
                comm.send(sendBuffer, sendBuffer.length, MPI.PACKED, 0, 0);
            } catch (MPIException mpiEx) {
                logger.error(myRank + " - Error: " + mpiEx.getMessage());
                throw new RuntimeException(mpiEx);
            }
        }
        return LL;
    }

    @Override
    protected void maximization(List<BigDecimal[]> eta) {
        logger.debug(myRank + " - maximization...");
        if (MASTER) {
            super.maximization(eta);
            // send axiomsProbBuffer to the slave. arrayProb contains the probabilities 
            // of the probabilistic axioms
            byte[] axiomsProbsBuffer = MPIUtilities.objectToByteArray(arrayProb);

//            logger.debug(myRank + " - sending to slaves new probabilistic "
//                    + "values for the axioms: [" + arrayProb + "]");
            try {
                logger.debug("Sending new probabilistic parameters");
                comm.bcast(new int[]{axiomsProbsBuffer.length}, 1, MPI.INT, myRank);
                comm.bcast(axiomsProbsBuffer, axiomsProbsBuffer.length, MPI.BYTE, myRank);
            } catch (MPIException mpiEx) {
                logger.error(myRank + " - Error: " + mpiEx);
                throw new RuntimeException(mpiEx);
            }
        } else { //SLAVE
            // receive array of probabilities of the probabilistic axioms
            try {
                logger.debug(myRank + " - Receiving new parameters for probabilistic axioms");
                int axiomsProbsBufferDim[] = new int[1];
                comm.bcast(axiomsProbsBufferDim, 1, MPI.INT, EDGEMPIConstants.MASTER);
                logger.debug("Receiving packet of " + axiomsProbsBufferDim[0] + " bytes");
                byte[] axiomsProbsBuffer = new byte[axiomsProbsBufferDim[0]];
                comm.bcast(axiomsProbsBuffer, axiomsProbsBuffer.length, MPI.BYTE, EDGEMPIConstants.MASTER);
                logger.debug(myRank + " - new paremeters received.");
                //
                arrayProb = (List<BigDecimal>) MPIUtilities.byteArrayToObject(axiomsProbsBuffer);
//                logger.debug(myRank + " - new values: [" + arrayProb + "]");
            } catch (MPIException mpiEx) {
                logger.error(myRank + " - Error: " + mpiEx);
                throw new RuntimeException(mpiEx);
            } catch (Exception ex) {
                logger.error(myRank + " - Error: " + ex);
                throw new RuntimeException(ex);
            }
            // set probs_ex
            for (int i = 0; i < probs_ex.size(); i++) {
                for (int j = 0; j < probs_ex.get(i).size(); j++) {
                    BigDecimal prob = arrayProb.get(vars_ex.get(i).get(j));
                    probs_ex.get(i).set(j, prob);
                }
            }
        }
    }

    @Override
    protected boolean checkEMConditions(BigDecimal diffLL, BigDecimal ratioLL, long iter) {
        if (MASTER) {
            if ((diffLL.compareTo(this.getDiffLL()) > 0) && (ratioLL.compareTo(this.getRatioLL()) > 0)
                    && iter < getMaxIterations()) {
                try {
                    logger.debug(myRank + " - Cycle " + iter + ": Continue...");
                    MPIUtilities.sendSignal(ALL_BCAST, CONTINUE, comm);
                } catch (MPIException mpiEx) {
                    logger.error(myRank + " - Error: " + mpiEx.getMessage());
                    throw new RuntimeException(mpiEx);
                }
                return true;
            } else {
                logger.debug(myRank + " - Cycle " + iter + ": Stop...");
                try {
                    MPIUtilities.sendSignal(ALL_BCAST, STOP, comm);
                } catch (MPIException mpiEx) {
                    logger.error(myRank + " - Error: " + mpiEx.getMessage());
                    throw new RuntimeException(mpiEx);
                }
                return false;
            }
        } else { // SLAVE
            int[] signal = new int[1];
            try {
                logger.debug(myRank + " - Waiting for signal...");
                comm.bcast(signal, 1, MPI.INT, 0);
                logger.debug(myRank + " - Received: " + signal[0]);
            } catch (MPIException ex) {
                logger.error(myRank + " - Error: " + ex);
                throw new RuntimeException(ex);
            }
            boolean continueEM = signal[0] == CONTINUE;
            logger.debug(myRank + " - run flag: " + continueEM);
            return continueEM;
        }
    }

    private Map.Entry<OWLAxiom, Type> nextExample() {
        if (countExamples < initialExamples.size()) {
            countExamples++;
            return initialExamples.get(countExamples - 1);
        } else {
            return null;
        }
    }

    /**
     * Class used to send an example to a slave.
     */
    class ExampleSender implements Runnable {

        /**
         * Rank of the slave
         */
        private final int slave;
        /**
         * example to send
         */
        private final List<Map.Entry<OWLAxiom, Type>> examplesChunk;
        /**
         * Number of examples to send
         */
        private final int numExamplesToSend;

        /**
         * Constructor of ExampleSender.
         *
         * @param slave rank of the slave to which the example should be sent.
         * @param example example to send.
         */
        public ExampleSender(int slave, List<Map.Entry<OWLAxiom, Type>> examplesChunk, int numExamplesToSend) {
            super();
            this.slave = slave;
            this.examplesChunk = examplesChunk;
            this.numExamplesToSend = numExamplesToSend;
        }

        @Override
        public void run() {
            // get the position of the first element of the chunk
            int pos = initialExamples.indexOf(examplesChunk.get(0));
            boolean sent = false;
            int attempt = 1;
            while (!sent) {
                try {
                    logger.debug(myRank + " - Sending to slave: " + slave
                            + " chunk of examples: " + examplesChunk
                            + " (array pos.: " + pos + "-" + (pos + examplesChunk.size() - 1)
                            + " - tag: " + (pos + 1) + "). Attempt: " + attempt);
                    MPIUtilities.sendObject(examplesChunk, slave, pos + 1, comm);
                    sent = true;
                    logger.debug(myRank + " - Sent to slave: " + slave
                            + " chunk of examples "
                            + " (array pos.: " + pos + "-" + (pos + examplesChunk.size())
                            + " - tag: " + (pos + 1) + ").");
                    synchronized (countActuallySentExamplesLock) {
                        countActuallySentExamples += examplesChunk.size();
                        if (numExamplesToSend == countActuallySentExamples) {
                            countActuallySentExamplesLock.notify();
                        }
                    }
                } catch (MPIException ex) {
                    logger.error("Problem during the sending of the example. Attempt: " + attempt);
                    if (attempt > maxNumAttempts) {
                        logger.error(myRank + " - Error: " + ex.getMessage());
                        throw new RuntimeException(ex);
                    }
                    logger.error("Retrying sending the example");
                    attempt++;
                }
            }
        }
    }

    /**
     * @return the comm
     */
    public Intracomm getComm() {
        return comm;
    }

    /**
     * @param comm the comm to set
     */
    public void setComm(Intracomm comm) {
        this.comm = comm;
    }

    /**
     * @return the dimension of the chunk of examples, i.e. the number of
     * examples in each chunk.
     */
    public int getChunkDim() {
        return chunkDim;
    }

    /**
     * @param chunkDim the dimension of the chunk, i.e. the number of examples
     * in each chunk.
     */
    public void setChunkDim(int chunkDim) {
        this.chunkDim = chunkDim;
    }

    /**
     * This class implements a thread that will be in listening and when a slave
     * is ready will send it an example.
     */
    class ExamplesListener implements Runnable {

        // TO DO: AGIRE QUI
        private BigDecimal[] probs;
        private int sentExamples;
        int recvExamples = 0;
        boolean test = false;

        public ExamplesListener(BigDecimal[] probs, int sentExamples, boolean test) {
            super();
            this.probs = probs;
            this.sentExamples = sentExamples;
            this.test = test;
        }

        @Override
        public void run() {
            try {
//                boolean allExamplesComputed = false;
                boolean stopRequest = false;

                int slaveId = 0;
                while (sentExamples != recvExamples) {
                    List<Map.Entry<OWLAxiom, Type>> examplesChunk = new ArrayList<>(chunkDim);
                    synchronized (countExamplesLock) {
                        logger.debug(myRank + " - T_2");

                        if (!examplesChunk.isEmpty()) {
                            examplesDistribution.get(slaveId).addAll(examplesChunk);
                        }
//                        if (countExamples < initialExamples.size()) {
                        //countComputedExamples++;
                        Map.Entry<OWLAxiom, Type> example = null;
                        for (int i = 0; i < chunkDim; i++) {
                            example = nextExample();
                            if (example != null) {
                                examplesChunk.add(example);
                            } else {
                                break;
                            }
                        }
//                        }
//                        else {
//                            allExamplesComputed = true;
//                        }
                    }
                    Status recvStat = comm.probe(MPI.ANY_SOURCE, MPI.ANY_TAG);
                    byte[] buffer = new byte[recvStat.getCount(MPI.PACKED)];
                    logger.debug(myRank + " - Waiting for an example result...");
                    // receive
                    comm.recv(buffer, buffer.length, MPI.PACKED, MPI.ANY_SOURCE, MPI.ANY_TAG);

                    // get example probability
                    byte[] exChunkProbsBuff = new byte[buffer.length - (Integer.SIZE / 8)];
                    int pos = comm.unpack(buffer, 0, exChunkProbsBuff, exChunkProbsBuff.length, MPI.BYTE);
                    int indexEx = recvStat.getTag() - 1;
                    slaveId = recvStat.getSource();
                    List<BigDecimal> exChunkProbs = (List<BigDecimal>) MPIUtilities.byteArrayToObject(exChunkProbsBuff);
                    recvExamples += exChunkProbs.size();
                    for (int i = 0; i < exChunkProbs.size(); i++) {
                        probs[indexEx + i] = exChunkProbs.get(i);
                        logger.debug(myRank + " - RECV from " + recvStat.getSource()
                                + " " + BundleUtilities.getManchesterSyntaxString(initialExamples.get(indexEx + i).getKey())
                                + " - prob: " + exChunkProbs.get(i)
                                + " - tag: " + (indexEx + i + 1)
                                + " - #vars: " + "0");
                    }

                    String msg = myRank + " - RECV from " + recvStat.getSource()
                            + " - chunk of " + exChunkProbs.size()
                            + " examples"
                            + " - prob: " + Arrays.copyOfRange(probs, indexEx, indexEx + exChunkProbs.size())
                            + " - tag: " + recvStat.getTag();
                    if (!test) {
                        int[] numBoolVars = new int[1];
                        comm.unpack(buffer, pos, numBoolVars, 1, MPI.INT);
                        msg += " - #vars: " + numBoolVars[0];
                        // indices of the axioms used in the BDD
                        int[] axiomsIdxs = new int[numBoolVars[0]];
                        if (numBoolVars[0] > 0) {
                            // receive the buffer containing the indices of the used axioms
                            comm.recv(axiomsIdxs, axiomsIdxs.length, MPI.INT, slaveId, recvStat.getTag());
                        }

                        for (int j = 0; j < numBoolVars[0]; j++) {
                            usedAxioms[axiomsIdxs[j]] = true;
                        }
                    }

                    logger.debug(msg);
                    // I've received the query/example results, 
                    // now it's time to send another example
                    if (examplesChunk.isEmpty()) {
                        if (!stopRequest) {
                            // Send stop request to all the slaves
                            new Thread() {
                                @Override
                                public void run() {
                                    synchronized (countActuallySentExamplesLock) {
                                        while (sentExamples != countActuallySentExamples) {
                                            try {
                                                countActuallySentExamplesLock.wait();
                                            } catch (InterruptedException ex) {
                                                logger.fatal(myRank + " - Error: " + ex.getMessage());
                                                throw new RuntimeException(ex);
                                            }
                                        }
                                    }
                                    logger.debug(myRank + " - Sending stop signal to all slaves");
                                    try {
                                        MPIUtilities.sendSignal(ALL, STOP, comm);
                                    } catch (MPIException mpiEx) {
                                        logger.error(myRank + " - Error: " + mpiEx.getMessage());
                                        throw new RuntimeException(mpiEx);
                                    }
                                }
                            }.start();
                            stopRequest = true;
                        }
                    } else {
                        // send Example to the slave
                        if (slaveId == 0) {
                            throw new RuntimeException("Inconsistent slave receiver!");
                        }
                        sentExamples += examplesChunk.size();
                        executor.execute(new Thread(new ExampleSender(slaveId, examplesChunk, sentExamples)));

                    }
                }
                if (!stopRequest) {
                    logger.debug(myRank + " - Sending stop signal to all slaves");
                    MPIUtilities.sendSignal(ALL, STOP, comm);
                    stopRequest = true; // unused
                }
            } catch (MPIException mpiEx) {
                logger.error(myRank + " - Error: " + mpiEx.getMessage());
                throw new RuntimeException(mpiEx);
            } catch (Exception ex) {
                logger.error(myRank + " - Error: " + ex.getMessage());
                throw new RuntimeException(ex);
            }
        }

        /**
         * @param sentExamples the sentExamples to set
         */
        public void setSentExamples(int sentExamples) {
            this.sentExamples = sentExamples;
        }

    }

    @Override
    protected void printLearningResult(Timers timers) { //, BDDFactory.CacheStats fCacheStats, BDDFactory.GCStats fGCStats, BDDFactory.ReorderStats fReorderStats) {
        if (MASTER) {
            super.printLearningResult(timers); //, fCacheStats, fGCStats, fReorderStats);
        }
    }

    @Override
    public void reset() {
        logger.debug(myRank + " - Resetting...");
        super.reset();
        countExamples = 0;
        countActuallySentExamples = 0;
        super.init();
        usedAxiomsOld = new boolean[usedAxioms.length];
        System.arraycopy(usedAxioms, 0, usedAxiomsOld, 0, usedAxioms.length);
    }
}
