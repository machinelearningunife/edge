/**
 *  This file is part of EDGE.
 * 
 *  EDGE is a probabilistic parameter learner for OWL 2 ontologies. 
 *  It learns the probabilistic values of the probabilistic axioms starting from a set of positive and negative axioms. 
 * 
 *  EDGE can be used both as module and as standalone.
 *  
 *  EDGE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package unife.edge;

import java.io.IOException;
import java.io.InputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import mpi.MPIException;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.Logger;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.w3c.dom.Document;
import unife.bundle.logging.BundleLoggerFactory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>, Riccardo Zese
 * <riccardo.zese@unife.it>
 */
public class EDGECLIImpl implements EDGECLI {

    EDGEControllerImpl controller;
    private static Logger logger;// = Logger.getLogger(EDGECLIImpl.class.getName(), new BundleLoggerFactory());
    public static final String header = "EDGE is an approach for learning "
            + "the parameters of probabilistic ontologies from data\n\n";
    private boolean parallel; // If true we are using MPI

    public EDGECLIImpl(boolean parallel) {
        logger = Logger.getLogger(EDGECLIImpl.class.getName(), new BundleLoggerFactory());
        this.parallel = parallel;
        controller = new EDGEControllerImpl(parallel);
//        if (!options.hasOption("gui")) {
//            Option gui = new Option("gui", "run GUI");
//            options.addOption(gui);
//        }
    }

    public EDGECLIImpl() {
        this(false);
    }

    @Override
    public void run(String[] args) {
        // this XML file contains all the possible options for EDGE
        String filePath = "/config/edge-conf-info.xml";
//        File optionXMLFile = new File("config/edge-options.xml");
        InputStream optionXML = getClass().getResourceAsStream(filePath);
        try {
            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = db.parse(optionXML);
//            Document doc = db.parse(optionXMLFile);
            Node optionsEl = doc.getDocumentElement().getFirstChild();
            while (!(optionsEl instanceof Element) || !(((Element) optionsEl).getTagName().equals("options"))) {
                optionsEl = optionsEl.getNextSibling();
                if (optionsEl == null) {
                    throw new ParseException("'options' element does not exist");
                }
            }
            //Element optionsEl = (Element) doc.getDocumentElement().getElementsByTagName("options").item(2);
            NodeList optionList = ((Element) optionsEl).getElementsByTagName("option");
            for (int i = 0; i < optionList.getLength(); i++) {
                options.addOption(getOption((Element) optionList.item(i)));
            }
            System.out.println(header);
            logger.info(header);
            logger.debug("Parsing...");
            controller.parseArgs(options, args);
            logger.debug("Parsing completed");
            logger.debug("Running EDGE");
            controller.run();
        } catch (ParseException exp) {
            // oops, something went wrong
            System.err.println("Parsing failed. Wrong option: " + exp.getMessage());
            logger.error("Parsing failed. Wrong option: " + exp.getMessage());
            System.exit(-1);
        } catch (OWLOntologyCreationException ex) {
            System.err.println("Loading of Ontology files failed.");
            logger.error("Loading of Ontology files failed. Reason: " + ex.getMessage());
            System.exit(-2);
        } catch (MPIException ex) {
            logger.error(ex.getMessage());
            throw new RuntimeException(ex);
        } catch (ParserConfigurationException ex) {
            logger.error("Error parsing " + filePath);
            throw new RuntimeException(ex);
        } catch (SAXException ex) {
            logger.error("Error: " + ex.getMessage());
            throw new RuntimeException(ex);
        } catch (IOException ex) {
            logger.error("Error: " + ex.getMessage());
            throw new RuntimeException(ex);
        }
    }

    private Option getOption(Element optionEl) {
        String longOpt = null;
        String opt = null;
        Node description = null;
        String descriptionStr = "";
        String argName = optionEl.getAttribute("arg-name");
        boolean hasArg = false;
        int maxArgs = 0;
        try {
// recover information from XML element
            longOpt = optionEl.getAttribute("name");
            opt = optionEl.getAttribute("alias");

            String maxArgsAttr = optionEl.getAttribute("max-args");
            if (!maxArgsAttr.equals("0")) {
                hasArg = true;
                if (!maxArgsAttr.equals("inf")) {
                    maxArgs = Integer.parseInt(maxArgsAttr);
                } else {
                    maxArgs = Option.UNLIMITED_VALUES;
                }
            }

            description = optionEl.getElementsByTagName("description").item(0);
            NodeList descriptionChildren = description.getChildNodes();
            for (int i = 0; i < descriptionChildren.getLength(); i++) {
                Node child = descriptionChildren.item(i);
                if (child instanceof Text) {
                    descriptionStr += child.getTextContent().trim();
                } else if (child instanceof Element) {
                    if (hasArg && ((Element) child).getTagName().equals("arg-name")) {
//                        descriptionStr += " " + argName + " ";
                        descriptionStr += " <arg> ";
                    }
                }
            }

            NodeList args = optionEl.getElementsByTagName("arg");
            if (args.getLength() > 0) {
                descriptionStr += "\nPossible arguments: \n";
                for (int i = 0; i < args.getLength(); i++) {
                    descriptionStr += args.item(i).getTextContent() + "\n";
                }
            }

        } catch (Exception ex) {
            logger.error("Impossible to recover information from \"option\" element "
                    + "with attribute name: " + longOpt);
            throw new RuntimeException(ex);
        }
        // create option
        Option option = new Option(opt, longOpt, hasArg, descriptionStr);
        if (hasArg) {
            option.setArgs(maxArgs);
//            option.setType(argName);
        }
        return option;
    }

}
