/**
 *  This file is part of EDGE.
 * 
 *  EDGE is a probabilistic parameter learner for OWL 2 ontologies. 
 *  It learns the probabilistic values of the probabilistic axioms starting from a set of positive and negative axioms. 
 * 
 *  EDGE can be used both as module and as standalone.
 *  
 *  EDGE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package unife.edge;

import com.clarkparsia.owlapiv3.OWL;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import net.sf.javabdd.BDD;
import net.sf.javabdd.BDDFactory;
import org.apache.log4j.Logger;
import org.mindswap.pellet.utils.Timer;
import org.mindswap.pellet.utils.Timers;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClassAssertionAxiom;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLException;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import unife.bundle.Bundle;
import unife.bundle.QueryResult;
import unife.bundle.logging.BundleLoggerFactory;
import unife.bundle.utilities.BundleUtilities;
import unife.edge.utilities.EDGEUtilities;
import unife.exception.IllegalValueException;
import unife.math.utilities.MathUtilities;
import unife.bundle.bdd.BDDFactory2;
import unife.utilities.GeneralUtils;

/**
 *
 * @author Riccardo Zese <riccardo.zese@unife.it>, Giuseppe Cota
 * <giuseppe.cota@unife.it>
 */
public class EDGE {

    /*
     * =*********************************= 
     * User arguments
     * =*********************************=
     */
    private int maxExplanations = Integer.MAX_VALUE;
    private String timeOut = null;
    private int BDDFStartNodeNum = 100;
    private int BDDFStartCache = 10000;
    private OWLOntology ontology = null;
    private OWLOntology probOntology = null;
    private String ontologyFileName = null;
    private String probOntologyFileName = null;
    private List<OWLAxiom> positiveExamples = null;
    private List<OWLAxiom> negativeExamples = null;
    private final List<OWLAxiom> examples = new ArrayList<>();
    private int seed = 0;
    private boolean randomize = false;
    private boolean probabilizeAll = false;
    private boolean showAll = false; // if true prints all the probabilistic axioms
    //private boolean learnParametersFromScratch = false;
    private int accuracy = 5;
    private BigDecimal diffLL = MathUtilities.getBigDecimal("0.00001", accuracy); // default value of diff between the old LL and new LL
    private BigDecimal ratioLL = MathUtilities.getBigDecimal("0.00001", accuracy); // default value of ratio, dependent on old LL and new LL
    private long maxIterations = 1000; // default num of EM iterations
    private boolean merge = false;
    private BigDecimal[] example_weights; // weight of the probabilities of the examples

    /*
     * =***********************************= 
     * Internal parameters
     * =***********************************=
     */
    public final BigDecimal LOGZERO = new BigDecimal(Math.log(0.000001)).setScale(getAccuracy(), RoundingMode.HALF_UP);

    private boolean randomSeed = false;

    /*
     * Used objects
     */
    private BDDFactory mgr;
    private static final Logger logger = Logger.getLogger(EDGE.class.getName(), new BundleLoggerFactory());
    protected List<String> probAxiomsString;
    protected boolean[] usedAxioms;           // assiomi usati
    protected List<List<Integer>> vars_ex;    // Lista di liste di OWLAxiom (per ogni esempio tengo corrispondenza tra indice variabile e assioma, per ogni esempio tengo le variabili)
    protected List<Integer> boolVars_ex;      // numero di variabili per ogni esempio
    protected List<List<BigDecimal>> probs_ex;    // per ogni esempio, per ogni variabile, probabilità
    protected List<OWLAxiom> probAxioms;
    protected List<BigDecimal> arrayProb;
    protected Bundle bundle;
    private boolean initialized = false;
    //protected int numberOfExamples;
    //List<BigDecimal[]> eta;
    //private List<BigDecimal> nodes_probs_ex; 

    //BigDecimal[] example_prob;
    private final OWLOntologyManager owlManager;

    private Map<OWLAxiom, BigDecimal> pMap;

    private Timers timers;

    public EDGE() {
        owlManager = OWLManager.createOWLOntologyManager();
    }

    public EDGEStat computeLearning() {
        //Timers timers = new Timers();

        List<BDD> BDDs = new ArrayList<>();

        //BigDecimal[] probs = new BigDecimal[examples.size()];
        // Initialization
//        if (!initialized) {
//            logger.debug("EDGE has not been initialized. Starting initialization...");
        init();
        logger.debug("Initialization completed");
//        }
//        if (!initialized) {
//            String msg = "EDGE is not correctly initialized.";
//            logger.error(msg);
//            throw new ObjectNotInitializedException(msg);
//        }

        if (getPMap() == null) {
            logger.warn("I don't have probabilistic axioms in PMap! I have nothing to learn!");
            throw new RuntimeException("Nothing to do! See log for details...");
        }
        // Find explanations for each example/query
        BigDecimal[] probs = computeExamples(getPMap(), BDDs);

        // Expectation-Maximization algorithm
        Timer timeEM = this.timers.startTimer("EM");
        EDGEStatImpl result = EM(BDDs, probs);

//        CacheStats fCacheStats = mgr.getCacheStats();
//        GCStats fGCStats = mgr.getGCStats();
//        ReorderStats fReorderStats = mgr.getReorderStats();
        for (int idxBDD = 0; idxBDD < BDDs.size(); idxBDD++) {
            BDDs.get(idxBDD).free();
            BDDs.set(idxBDD, null);
        }
        BDDs.clear();

        timeEM.stop();

        this.timers.mainTimer.stop();

        // alla fine di tutto l'ontologia contenente gli assiomi probabilistici 
        // target, ossia probOntology, deve essere modificata, in maniera tale
        // da aggiornare i parametri dei suoi assiomi probabilistici
        printLearningResult(this.timers); //, fCacheStats, fGCStats, fReorderStats);

        result.setTimers(timers.getTimers());

        reset();

        return result;
    }

    protected BigDecimal[] computeExamples(Map<OWLAxiom, BigDecimal> PMap, List<BDD> BDDs) {
        return computeExamples(PMap, BDDs, false);
    }

    protected BigDecimal[] computeExamples(Map<OWLAxiom, BigDecimal> PMap, List<BDD> BDDs, boolean test) {
        BigDecimal[] probs = new BigDecimal[examples.size()];
        if (initialized) {
            logger.info("Start finding explanations for every example (Computing BDDs)...");

            int index = 0;
            // settare PMap in bundle???
            for (OWLAxiom query : this.positiveExamples) {
                logger.info("Query " + (index + 1) + " of " + examples.size() + " (" + (int) (((index + 1) * 100) / examples.size()) + "%)");
                logger.info("Positive Example: " + query);

                // Creare qui un'istanza di BUNDLE o crearla prima e qui semplicemente settarla???BundleDeprecate       
                // setting di Bundle
                //            bundle.setBddF(this.mgr);
                //            bundle.setMaxExplanations(this.maxExplanations);
                //            bundle.setMaxTime(this.timeOut);
                //            bundle.setPMap(PMap);
                //            // bisogna dare a bundle inoltre la Pmap, l'ontologia non probabilistica e l'ontologia probabilistica 
                // clear Bundle
                bundle = startBundle();
                bundle.setPMap(PMap);
                bundle.init();

                // get Bundle results
                QueryResult queryResult = null;
                try {
                    queryResult = bundle.computeQuery(query);
                    updateTimers("Bundle", queryResult.getTimers());
                } catch (OWLException owle) {
                    logger.error("It was impossible to compute the probability of the query");
                    logger.error(owle.getMessage());
                    logger.trace(owle.getStackTrace());

                    System.err.println("Terminated because of an internal error. See the log for details");
                    throw new RuntimeException(owle);
                }

                if (queryResult == null) {
                    logger.fatal("The result of BUNDLE algorithm is null");
                    // throw an exception;
                    //throw;
                    throw new RuntimeException("Impossible to compute the query");
                }
                if (!test) {
                    // get the BDD of the query/example
                    BDDs.add(queryResult.getBDD());

                    init_bdd(queryResult);
                }

                probs[index] = queryResult.getQueryProbability();
                index++;

                bundle.finish();
                bundle = null;
                //System.gc();

            }
            for (OWLAxiom query : this.negativeExamples) {
                OWLAxiom notQuery = OWL.classAssertion(((OWLClassAssertionAxiom) query).getIndividual(),
                        OWL.not(((OWLClassAssertionAxiom) query).getClassExpression()));
                logger.info("Query " + (index + 1) + " of " + examples.size() + " (" + (int) (((index + 1) * 100) / examples.size()) + "%)");
                logger.info("Negative Example: " + notQuery);

                // Creare qui un'istanza di BUNDLE o crearla prima e quBundleDeprecatelicemente sBundleDeprecatea???
                // setting di Bundle
                // clear Bundle
                bundle = startBundle();
                bundle.setPMap(PMap);
                bundle.init();

                // get Bundle results
                QueryResult queryResult = null;
                try {
                    queryResult = bundle.computeQuery(notQuery);
                    updateTimers("Bundle", queryResult.getTimers());
                } catch (OWLException owle) {
                    logger.fatal("It was impossible to compute the probability of the query");
                    logger.fatal(owle.getMessage());
                    logger.trace(owle.getStackTrace());

                    System.err.println("Terminated because of an internal error. See the log for details");
                    throw new RuntimeException(owle);
                }

                if (queryResult != null && !queryResult.getExplanations().isEmpty()) {
                    if (!test) // get the BDD of the query/example
                    {
                        BDDs.add(queryResult.getBDD());
                    }
                    probs[index] = queryResult.getQueryProbability();
                } else {
                    logger.debug("Trying the second method...");
                    try {
                        bundle.finish();
                        bundle = startBundle();
                        bundle.setPMap(PMap);
                        bundle.init();
                        queryResult = bundle.computeQuery(query);
                        updateTimers("Bundle", queryResult.getTimers());
                    } catch (OWLException owle) {
                        logger.fatal("It was impossible to compute the probability of the query");
                        logger.fatal(owle.getMessage());
                        logger.trace(owle.getStackTrace());

                        System.err.println("Terminated because of an internal error. See the log for details");
                        throw new RuntimeException(owle);
                    }

                    if (queryResult == null) {
                        logger.fatal("The result of BUNDLE algorithm is null");
                        throw new RuntimeException("Impossible to compute the query");
                    }
                    if (!test) {
                        BDDs.add(queryResult.getBDD().not());
                    }
                    probs[index] = MathUtilities.getBigDecimal("1", accuracy).subtract(queryResult.getQueryProbability());
                }

                if (!test) {
                    init_bdd(queryResult);
                }

                index++;

                bundle.finish();
                bundle = null;
                //System.gc();
            }

            logger.info("Explanations founding completed (BDDs computed)");
        }
        return probs;
    }

    // to be replaced with a factory class
    protected Bundle startBundle() {

        bundle = new Bundle();
        bundle.setBddF(this.mgr);
        bundle.setMaxExplanations(this.getMaxExplanations());
        bundle.setMaxTime(this.timeOut);
        bundle.setLog(true);
        bundle.setAccuracy(accuracy);
        if (ontologyFileName != null) {
            bundle.loadOntologies(ontologyFileName); // perché serve il filename?
        } else if (ontology != null) {
            bundle.loadOntologies(ontology);
        }
        //bundle.setTimers(timers);
        //bundle.init();
        return bundle;
    }

    /**
     * This method returns the Ontology obtained after the parameter learning.
     *
     * @return
     */
    public OWLOntology getLearnedOntology() {
        // da debuggare e testare

        logger.info("Creation of the learned ontology...");

        if (probOntology == null) {
            merge = true;
        }

        Timers timers = new Timers();
        Timer timer = timers.createTimer("OntologyCreation");

        timer.start();
        OWLOntology resultOntology = null;
        try {
            Set<OWLAxiom> ontologyAxioms;
            OWLDataFactory owlFactory = owlManager.getOWLDataFactory();
            if (merge) {
                //OWLOntology ontology = owlManager.createOntology(IRI.create(ontologyFileName));

                if (ontology == null) {
                    if (ontologyFileName == null) {
                        logger.warn("It was impossible to creating the resulting ontology");
                        logger.warn("The base ontology is null, I cannot merge with probabilitic ontology");
                        logger.warn("Output probabilistic ontology will contain only axioms from the input probabilistic ontology");
                        System.out.println("Impossible to merge ontologies. See log for details...");

                        // Use only probabilistic ontology
                        ontologyAxioms = probOntology.getAxioms();
                        owlManager.removeOntology(probOntology);
                        resultOntology = owlManager.createOntology(probOntology.getOntologyID());

                        merge = false;

                    } else {
                        if (probOntology != null) {
                            owlManager.removeOntology(probOntology);
                        }
                        ontology = owlManager.loadOntologyFromOntologyDocument(IRI.create(ontologyFileName));
                        resultOntology = ontology;
                        ontologyAxioms = resultOntology.getAxioms();
                    }
                } else {
                    resultOntology = ontology;
                    ontologyAxioms = resultOntology.getAxioms();
                }

                //owlManager.removeOntology(ontology);
            } else {
                ontologyAxioms = probOntology.getAxioms();
                owlManager.removeOntology(probOntology);
                resultOntology = owlManager.createOntology(probOntology.getOntologyID());

            }

            //Set<OWLAxiom> filteredAxioms = EDGEUtilities.get_ax_filtered(probOntology);
            for (OWLAxiom ax : ontologyAxioms) {

                for (int i = 0; i < GeneralUtils.safe(probAxioms).size(); i++) {
                    OWLAxiom pax = probAxioms.get(i);

                    //i++;
                    if (pax.equalsIgnoreAnnotations(ax)) {
                        Set<OWLAnnotation> axAnnotations = new HashSet(ax.getAnnotations());
                        if (ax.getAnnotations(BundleUtilities.PROBABILISTIC_ANNOTATION_PROPERTY).size() > 0) {
                            for (OWLAnnotation ann : axAnnotations) {
                                if (ann.getProperty().equals(BundleUtilities.PROBABILISTIC_ANNOTATION_PROPERTY)) {
                                    axAnnotations.remove(ann);
                                    break;
                                }
                            }
                        }
                        if (merge) {
                            owlManager.removeAxiom(resultOntology, ax);
                        }
                        axAnnotations.add(owlFactory.getOWLAnnotation(BundleUtilities.PROBABILISTIC_ANNOTATION_PROPERTY, owlFactory.getOWLLiteral(arrayProb.get(i).doubleValue())));
                        owlManager.addAxiom(resultOntology, pax.getAnnotatedAxiom(axAnnotations));
                        //ontologyAxioms.remove(pax);
                        break;
                    }
                }
            }

        } catch (OWLOntologyCreationException ex) {
            logger.error("It was impossible to creating the resulting ontology");
            logger.error(ex.getMessage());
            logger.trace(ex.getStackTrace());
            // lanciare un'eccezione
            System.err.println("Unsuccessful creation of the learned ontology. See the log for details");
            return null;
        }

        timer.stop();

        logger.info("Successful creation of the learned ontology");
        logger.info("Ontology created in " + timer.getAverage() + " (ms)");

        return resultOntology;

    }

    /**
     * Initializes the PMap to use for find the explanations.
     *
     * @param timer
     * @return the PMap containing the probabilities of the probabilistic axioms
     * of the input ontology
     */
    protected Map<OWLAxiom, BigDecimal> preparePMap() {

        logger.debug("Preparing Probability map...");

        probAxioms = new ArrayList<>();
        probAxiomsString = new ArrayList<>();
        arrayProb = new ArrayList<>();

        // SET ONTOLOGY OR ALREADY SET??????
        try {
            if (probOntology == null) {
                if (probOntologyFileName == null) {
                    if (ontology == null) {
                        ontology = owlManager.loadOntologyFromOntologyDocument(IRI.create(ontologyFileName));
                    }
                    Set<OWLAxiom> probAxioms = BundleUtilities.getProbabilisticAxioms(ontology);
                    if (probabilizeAll) {
                        probAxioms.addAll(EDGEUtilities.get_ax_filtered(ontology.getAxioms()));
                    }
                    probOntology = owlManager.createOntology(probAxioms);
                } else {
                    probOntology = owlManager.loadOntologyFromOntologyDocument(IRI.create(probOntologyFileName));
                }
            }
        } catch (OWLOntologyCreationException ex) {
            logger.fatal("It was impossible to create the map of the probabilistic axioms");
            logger.fatal(ex.getMessage());
            logger.trace(ex.getStackTrace());

            System.err.println("Terminated because of an internal error. See the log for details");
            throw new RuntimeException(ex);
        }
        SortedSet<OWLAxiom> axioms = EDGEUtilities.get_ax_filtered(probOntology);
        Map<OWLAxiom, BigDecimal> pMap = BundleUtilities.createPMap(axioms, showAll, randomize, probabilizeAll, accuracy, seed);

        if (pMap != null) {
            for (OWLAxiom key : pMap.keySet()) {
                probAxioms.add(key);
                probAxiomsString.add(BundleUtilities.getManchesterSyntaxString(key));
                arrayProb.add(pMap.get(key));
            }

        }

        return pMap;
    }

    /*
     * ************************************
     * Setters and getters 
     * ************************************
     */
    /**
     * @param maxExplanations the maxExplanations to set
     */
    public void setMaxExplanations(int maxExplanations) {
        this.maxExplanations = maxExplanations;
    }

    /**
     * @param timeOut the timeOut to set
     */
    public void setTimeOut(String timeOut) {
        this.timeOut = timeOut;
    }

//    public void setTimeOut(int timeOut) {
//        bundle.setMaxTime(timeOut);
//    }
    /**
     * @param BDDFStartNodeNum the BDDFStartNodeNum to set
     */
    public void setBDDFStartNodeNum(int BDDFStartNodeNum) {
        this.BDDFStartNodeNum = BDDFStartNodeNum;
    }

    /**
     * @param BDDFStartCache the BDDFStartCache to set
     */
    public void setBDDFStartCache(int BDDFStartCache) {
        this.BDDFStartCache = BDDFStartCache;
    }

    /**
     * @param ontology the ontology to set
     */
    public void setOntology(OWLOntology ontology) {
        this.ontology = ontology;
    }

    /**
     * @param ontology the ontology to set, if the probabilistic ontology is not
     * set yet, this method set it to ontology
     */
    public void setOntologies(OWLOntology ontology) {
        this.ontology = ontology;
//        if (getProbOntology() == null) {
        this.probOntology = ontology;
//        }
    }

    /**
     * @param probOntology the probOntology to set
     */
    public void setProbOntology(OWLOntology probOntology) {
        this.probOntology = probOntology;
    }

    /**
     * @param positiveExamples the positiveExamples to set
     */
    public void setPositiveExamples(List<OWLAxiom> positiveExamples) {
        if (this.positiveExamples != null && this.positiveExamples.size() > 0) {
            logger.debug("# examples: " + examples.size());
            examples.removeAll(this.positiveExamples);
        }
//        if (this.negativeExamples != null) {
//            examples.removeAll(this.negativeExamples);
//            //examples.removeAll(examples);
//            logger.debug("# examples: " + examples.size());
//        }
//        examples.addAll(positiveExamples);
        this.positiveExamples = positiveExamples;
        // there can be a example that is both positive and both negative
//        if (this.negativeExamples != null) {
//            examples.addAll(this.negativeExamples);
//        }
        examples.addAll(0, positiveExamples);
        logger.debug("# examples: " + examples.size());
        logger.debug("# positive examples: " + positiveExamples.size());
    }

    /**
     * @param negativeExamples the negativeExamples to set
     */
    public void setNegativeExamples(List<OWLAxiom> negativeExamples) {
        if (this.negativeExamples != null && this.negativeExamples.size() > 0) {
            logger.debug("# examples: " + examples.size());
            examples.removeAll(this.negativeExamples);
        }
//        if (this.positiveExamples != null) {
//            examples.removeAll(positiveExamples);
//            logger.debug("# examples: " + examples.size());
//        }
//        examples.addAll(negativeExamples);
        this.negativeExamples = negativeExamples;
//        if (this.positiveExamples != null) {
//            examples.addAll(this.positiveExamples);
//        }
        examples.addAll(negativeExamples);
        logger.debug("# examples: " + examples.size());
        logger.debug("# negative examples: " + negativeExamples.size());
    }

    /**
     * @param seed the seed to set
     */
    public void setSeed(int seed) {
        this.seed = seed;
        randomSeed = true;
    }

    /**
     * @param randomize the randomize to set
     */
    public void setRandomize(boolean randomize) {
        this.randomize = randomize;
    }

    /**
     * @param probabilizeAll the randomizeAll to set
     */
    public void setProbabilizeAll(boolean probabilizeAll) {
        this.probabilizeAll = probabilizeAll;
    }

    /**
     * @param showAll the showAll to set
     */
    public void setShowAll(boolean showAll) {
        this.showAll = showAll;
    }

    /**
     * @param learnParametersFromScratch the ldiffLLrnParametratioLLsFromScratch
     * to set
     */
//    public void setLearnParametersFromScratch(boolean learnParametersFromScratch) {
//        this.learnParametersFromScratch = learnParametersFromScratch;
//    }
    /**
     * @param ea the diffLL to set
     */
    public void setDiffLL(String ea) throws IllegalValueException {
        try {
            this.diffLL = new BigDecimal(ea);
        } catch (Exception e) {
            throw new IllegalValueException(e.getMessage());
        }
    }

    /**
     * @param er the ratioLL to set
     * @throws unife.exception.IllegalValueException
     */
    public void setRatioLL(String er) throws IllegalValueException {
        try {
            this.ratioLL = new BigDecimal(er);
        } catch (Exception e) {
            throw new IllegalValueException(e.getMessage());
        }
        if (this.ratioLL.compareTo(new BigDecimal(0)) <= 0
                || this.ratioLL.compareTo(new BigDecimal(1)) > 1) {
            String message = "Wrong ratio value: " + this.ratioLL + ".\nIt must be a real "
                    + "number greater that 0 and less or equal than 1.";
            throw new IllegalValueException(message);
        }
    }

    protected void init_bdd(QueryResult queryResult) {
        // Insert in vars_ex the index of the axioms used in the current example 
        // ans set to true the corresponding boolean.
        List<Integer> to_add = new ArrayList<>();
        for (int i = 0; i < queryResult.getProbAxioms().size(); i++) {
            int pos = probAxiomsString.indexOf(BundleUtilities.getManchesterSyntaxString(queryResult.getProbAxioms().get(i)));

            // if (!usedAxioms[pos])
            usedAxioms[pos] = true;

            to_add.add(pos);

        }
//        for (int i = 0; i < axioms.size(); i++){
//            to_add.add(rulesName.indexOf(axioms.get(i)));
//        }
        vars_ex.add(to_add);
        to_add = null;

        //nVars_ex.add(nAx);
        boolVars_ex.add(queryResult.getProbAxioms().size());

        probs_ex.add(queryResult.getProbOfAxioms());

        //nodes_probs_ex.add(0.0);
    }

    public void computeTest() {
//        Timers timers = new Timers();
//        List<BDD> BDDs = new ArrayList<>();

        // creation of probability map
        Timer timePMap = getTimers().startTimer("PMap");
        Map<OWLAxiom, BigDecimal> PMap = preparePMap();
        timePMap.stop();

//        logger.debug("Initializing...");
//        init();
//        logger.debug("Initialization completed");
//        mgr = BuDDyFactory.init(BDDFStartNodeNum, BDDFStartCache);
        mgr = BDDFactory2.init("buddy", BDDFStartNodeNum, BDDFStartCache);

        logger.info("Start test\n\t- n. of queries:\t\t" + (examples.size()));
        logger.info("");
        logger.info("");

        Timer timeBundle = getTimers().startTimer("Test");

        BigDecimal[] probs = computeExamples(PMap, null, true);
        timeBundle.stop();
        // print test results
        printTestResult(getTimers(), probs);
    }

    protected void printTestResult(Timers timers, BigDecimal[] probs) {
        logger.info("");
        logger.info("============ Result ============");
        logger.info("");

        int index = 0;
        for (OWLAxiom ax : getPositiveExamples()) {

            logger.info("Query " + (index + 1) + " of " + examples.size()
                    + " (" + (int) (((index + 1) * 100) / examples.size()) + "%)");
            logger.info("Positive Example: " + ax);
            logger.info("Prob: " + probs[index]);
            index++;
        }
        for (OWLAxiom ax : getNegativeExamples()) {

            logger.info("Query " + (index + 1) + " of " + examples.size()
                    + " (" + (int) (((index + 1) * 100) / examples.size()) + "%)");
            logger.info("Negative Example: " + ax);
            logger.info("Prob: " + probs[index]);
            index++;
        }

        System.out.println();

        logger.info("");
        logger.info("================================");
        logger.info("");
        StringWriter sw = new StringWriter();
        timers.print(sw, true, null);
        logger.info(sw);
    }

    protected void printLearningResult(Timers timers) { //, CacheStats fCacheStats, GCStats fGCStats, ReorderStats fReorderStats) {
        String axiom;

        int nRules = probAxioms.size();
        boolean show = true;

        if (showAll) {
            logger.info("");
            logger.info("============ Results ============");
            logger.info("");

            for (int j = 0; j < nRules; j++) {
//                    if (arrayprob.get(j) > 0.0){

                axiom = probAxiomsString.get(j).toString();
                logger.info("ax " + (j + 1) + "/" + nRules + " : " + axiom + "\tarrayprob: " + arrayProb.get(j) + "\n");
//                    }
            }
            logger.info("");
            logger.info("=================================");

        }

//        logger.info("");
//        logger.info("=========== BDD Stats ===========");
//        logger.info("");
//        logger.info(fCacheStats);
//        logger.info("");
//        logger.info(fGCStats);
//        logger.info("");
//        logger.info(fReorderStats);
//        logger.info("");
//        logger.info("=================================");
        logger.info("");
        StringWriter sw = new StringWriter();
        timers.print(sw, true, null);
        logger.info(sw);
    }

    public void init() {

        logger.debug("Initializing...");

        if (timers == null) {
            timers = new Timers();
        }
        Timer timeInit = timers.startTimer("init");
        // Creation of probability map
//        Timer timePMap = timers.startTimer("PMap");
        pMap = preparePMap();
//        timePMap.stop();

        int nRules = probAxioms.size();
        usedAxioms = new boolean[nRules];

        vars_ex = new ArrayList<>(); // Le variabili per esempio
        probs_ex = new ArrayList<>();
        boolVars_ex = new ArrayList<>(); // corrisponde a nVars_ex

        for (int j = 0; j < nRules; j++) {
            usedAxioms[j] = false;
        }

        if (getPositiveExamples() == null) {
            setPositiveExamples(new ArrayList<OWLAxiom>());
        }
        if (getNegativeExamples() == null) {
            setNegativeExamples(new ArrayList<OWLAxiom>());
        }

        // Initialization of the BDD manager
//        mgr = BuDDyFactory.init(BDDFStartNodeNum, BDDFStartCache);
        if (mgr == null || !mgr.isInitialized()) {
//            try {
                mgr = BDDFactory2.init("buddy", BDDFStartNodeNum, BDDFStartCache);
//            } catch (Exception ex) {
//                logger.warn("It is not possible to use the requested BDD factory"
//                        + "type BUDDY. Maybe it was impossible to load"
//                        + "the libraries. Trying to use \"J\" (100% Java "
//                        + "implementation of the BDD factory).");
//                mgr = BDDFactory2.init(BDDType.J.toString().toLowerCase(),
//                        BDDFStartNodeNum, BDDFStartCache);
//            }
        }

        initialized = true;
        timeInit.stop();
    }

    protected EDGEStatImpl EM(List<BDD> BDDs, BigDecimal[] probs) {
        if (initialized) {
            logger.info("Start EM Algorithm\n\t- n. of probabilistic axioms:\t" + probAxioms.size()
                    + "\n\t- n. of examples:\t\t" + (examples.size()));

            int lenNodes = BDDs.size(), nRules = probAxioms.size();
            BigDecimal CLL0 = MathUtilities.getBigDecimal(-2.2 * Math.pow(10, 10), accuracy);  // -inf
            BigDecimal CLL1 = MathUtilities.getBigDecimal(-1.7 * Math.pow(10, 8), accuracy);    // +inf
            BigDecimal ratio, diff;
            int cycle = 0;
            for (int i = 0; i < probs.length; i++) {
                logger.debug(BundleUtilities.getManchesterSyntaxString(examples.get(i))
                        + " - prob: " + probs[i] + " - tag: " + (i + 1)
                        + " - #vars: " + boolVars_ex.get(i));
            }

            logger.debug("EM cycle: " + cycle);
            List<BigDecimal[]> eta = new ArrayList<>();
            for (int i = 0; i < nRules; i++) {
                logger.trace("probabilistic values before EM cycle"
                        + ": " + arrayProb);
                BigDecimal[] eta_int_temp = new BigDecimal[2];
                eta_int_temp[0] = MathUtilities.getBigDecimal("0", accuracy);
                eta_int_temp[1] = MathUtilities.getBigDecimal("0", accuracy);
                eta.add(eta_int_temp);

            }

            //vettore di BDD (nodi radice) e sua lunghezza
            if (example_weights == null || example_weights.length == 0) {
                example_weights = new BigDecimal[lenNodes];
                for (int i = 0; i < lenNodes; i++) {
                    example_weights[i] = MathUtilities.getBigDecimal("1.0", accuracy);
                }
            }
            //System.arraycopy(example_prob_calc, 0, example_prob, 0, lenNodes);

            diff = CLL1.subtract(CLL0);
            ratio = diff.divide(CLL0.abs(), accuracy, BigDecimal.ROUND_HALF_UP);

            while (checkEMConditions(diff, ratio, cycle)) {
                cycle++;
                logger.debug("EM cycle: " + cycle);
                CLL0 = CLL1;
                CLL1 = expectation(BDDs, getExample_weights(), eta, lenNodes);
                maximization(eta);
                diff = CLL1.subtract(CLL0);
                if (CLL0.compareTo(BigDecimal.ZERO) == 0) {
                    ratio = BigDecimal.ZERO;
                } else {
                    ratio = diff.divide(CLL0.abs(), accuracy, BigDecimal.ROUND_HALF_UP);
                }
                logger.debug("Log-likelihood: " + CLL1 + " cycle: " + cycle);
                //System.out.println("Log-Likelyhood ciclo " + cycle + " : " + CLL1);
            }

            logger.info("EM completed.");
            logger.info("\n  Final Log-Likelihood: " + CLL1);

            return new EDGEStatImpl(cycle, CLL1, probAxioms, arrayProb);
        } else {
            return null;
        }

    }

    /**
     * Calculates the expectation of a list of examples.
     *
     * @param nodes_ex a list of BDD corresponding to the examples
     * @param example_prob a list of query probabilities
     * @param lenNodes number of BDD contained in nodes_ex
     * @param eta
     * @return the calculated Log-Likelihood
     */
    protected BigDecimal expectation(List<BDD> nodes_ex, BigDecimal[] example_prob,
            List<BigDecimal[]> eta, int lenNodes) {
        BigDecimal rootProb;
        BigDecimal CLL = MathUtilities.getBigDecimal("0", accuracy);

        for (int i = 0; i < lenNodes; i++) {
            if (!(nodes_ex.get(i).isOne() || nodes_ex.get(i).isZero())) {

                EMTable nodesB = new EMTable(boolVars_ex.get(i));
                EMTable nodesF = new EMTable(boolVars_ex.get(i));

                forward(nodes_ex.get(i), nodesF, i);

                rootProb = getOutsideExpe(nodes_ex.get(i), example_prob[i], nodesF, nodesB, eta, i);

                if (rootProb.compareTo(new BigDecimal(0)) <= 0) {
                    logger.debug("Example " + i + ": zero or negative probability -> " + rootProb + "\n");
                    CLL = CLL.add(LOGZERO.multiply(example_prob[i])).setScale(accuracy, RoundingMode.HALF_UP);
                } else {
                    BigDecimal logRootProb = MathUtilities.getBigDecimal(Math.log(rootProb.doubleValue()), accuracy);
                    CLL = CLL.add(logRootProb.multiply(example_prob[i])).setScale(accuracy, RoundingMode.HALF_UP);
                }

                //nodes_probs_ex.set(i, rootProb);
                // destroy_table
                nodesB.clear();
                nodesF.clear();
                nodesB = null;
                nodesF = null;
            } else if (nodes_ex.get(i).isZero()) {
                CLL = CLL.add(LOGZERO.multiply(example_prob[i])).setScale(accuracy, RoundingMode.HALF_UP);
            }
        }

        return CLL;
    }

    /**
     * Calculates the forward probabilities of nodes of a BDD. Builds the nodesF
     * tables containing the forward probabilities of all nodes of the given BDD
     *
     * @param root the root of a BDD
     * @param nEx number of the current example
     */
    private void forward(BDD root, EMTable nodesF, int nEx) {

        if (boolVars_ex.get(nEx) > 0) {
            List<List<BDD>> nodesToVisit = new ArrayList<>(); // da sostituire con clear ?????
            List<Integer> NnodesToVisit = new ArrayList<>();
            List<BDD> to_add = new ArrayList<>();
            to_add.add(root);
            nodesToVisit.add(to_add);
            NnodesToVisit.add(1);

            for (int i = 1; i < boolVars_ex.get(nEx); i++) {
                nodesToVisit.add(new ArrayList<BDD>());
                NnodesToVisit.add(0);
            }

            nodesF.add_node(root, MathUtilities.getBigDecimal("1", accuracy));

            for (int i = 0; i < boolVars_ex.get(nEx); i++) {
                for (int j = 0; j < NnodesToVisit.get(i); j++) {
                    updateForward(nodesToVisit, NnodesToVisit, i, j, nodesF, nEx);
                }
            }

            for (List<BDD> ithList : nodesToVisit) {
                for (int j = 0; j < ithList.size(); j++) {
                    ithList.set(j, null);
                }
            }
            nodesToVisit.clear();
            nodesToVisit = null;

        } else {
            nodesF.add_node(root, MathUtilities.getBigDecimal("1", accuracy));
        }
    }

    /**
     * Updates the forward of a node in a BDD.
     *
     * @param node node of a BDD to calculates the forward probability
     * @param nEx number of the current example
     */
    private void updateForward(List<List<BDD>> nodesToVisit,
            List<Integer> NnodesToVisit,
            int iNode, int jNode, EMTable nodesF, int nEx) {

        int index, position;
        BDD T, E, node;//, nodereg;

        BigDecimal value_p, value_p_T, value_p_F;
        BigDecimal p, notP;

        node = nodesToVisit.get(iNode).get(jNode);

        if (!node.isOne() && !node.isZero()) {

            index = node.var();

            p = probs_ex.get(nEx).get(index);
            notP = MathUtilities.getBigDecimal("1", accuracy).subtract(p);
            //nodereg = node;
            value_p = nodesF.get_value(node);//(nodereg);

            if (value_p == null) {
                System.out.println("Error");
                //return;
            } else {
                T = node.high();
                E = node.low();

                if (!(T.isOne() || T.isZero())) {
                    value_p_T = nodesF.get_value(T);

                    if (value_p_T != null) {
                        value_p_T = value_p_T.add(value_p.multiply(p)).setScale(accuracy, RoundingMode.HALF_UP);
                        nodesF.set_value(T, value_p_T);
                    } else {
                        nodesF.add_or_replace_node(T, value_p.multiply(p).setScale(accuracy, RoundingMode.HALF_UP));
                        index = T.var();
                        position = mgr.var2Level(index); //T.level(); //mgr.ithVar(index).level();
                        nodesToVisit.get(position).add(T);
                        NnodesToVisit.set(position, NnodesToVisit.get(position) + 1);
                    }
                }

                if (!(E.isOne() || E.isZero())) {
                    value_p_F = nodesF.get_value(E);

                    if (value_p_F != null) {
                        value_p_F = value_p_F.add(value_p.multiply(notP).setScale(accuracy, RoundingMode.HALF_UP));
                        nodesF.set_value(E, value_p_F);
                    } else {
                        nodesF.add_or_replace_node(E, value_p.multiply(notP).setScale(accuracy, RoundingMode.HALF_UP));
                        index = E.var();
                        position = mgr.var2Level(index);//ithVar(index).level();
                        nodesToVisit.get(position).add(E);
                        NnodesToVisit.set(position, NnodesToVisit.get(position) + 1);
                    }
                }
            }
        }
    }

    /**
     * Calculates the expectation of a single BDD.
     *
     * @param root the root node of the BDD
     * @param ex_prob probability of the BDD in the current example
     * @param nex number of the current example
     * @return expectation of the BDD (the probability of BDD)
     */
    private BigDecimal getOutsideExpe(BDD root, BigDecimal ex_prob,
            EMTable nodesF, EMTable nodesB, List<BigDecimal[]> eta, int nex) {
        int bVarIndex, nRules = probAxioms.size();
        BigDecimal[] eta_rule;
        BigDecimal theta, notTheta;
        BigDecimal rootProb;
        BigDecimal T = MathUtilities.getBigDecimal("0", accuracy);

        List<BigDecimal[]> eta_temp = new ArrayList<>(nRules);
        BigDecimal[] sigma = new BigDecimal[boolVars_ex.get(nex)];

        for (int i = 0; i < boolVars_ex.get(nex); i++) {
            sigma[i] = MathUtilities.getBigDecimal("0", accuracy);
        }

        for (int j = 0; j < nRules; j++) {
            BigDecimal[] to_add = new BigDecimal[2];
            to_add[0] = MathUtilities.getBigDecimal("0", accuracy);
            to_add[1] = MathUtilities.getBigDecimal("0", accuracy);
            eta_temp.add(to_add);
        }

        rootProb = probPath(root, sigma, eta_temp, nodesF, nodesB, nex);

        if (rootProb.compareTo(new BigDecimal(0)) > 0.0) {
            for (int j = 0; j < boolVars_ex.get(nex); j++) {
                T = T.add(sigma[j]);
                bVarIndex = mgr.ithVar(j).var();
                if (bVarIndex == -1) {
                    bVarIndex = j;
                }
//                boolAxiom[bVarIndex] = true;

                // Nel caso usa eta_temp invece che eta_rule
                eta_rule = eta_temp.get(vars_ex.get(nex).get(bVarIndex));

                theta = probs_ex.get(nex).get(bVarIndex);
                notTheta = MathUtilities.getBigDecimal("1", accuracy).subtract(theta);
                eta_rule[0] = eta_rule[0].add(T.multiply(notTheta).setScale(accuracy, RoundingMode.HALF_UP));
                eta_rule[1] = eta_rule[1].add(T.multiply(theta).setScale(accuracy, RoundingMode.HALF_UP));
            }

            for (int j = 0; j < nRules; j++) {
                eta.get(j)[0] = eta.get(j)[0].add(eta_temp.get(j)[0].multiply(ex_prob).setScale(accuracy, RoundingMode.HALF_UP)
                        .divide(rootProb, getAccuracy(), RoundingMode.HALF_UP));
                eta.get(j)[1] = eta.get(j)[1].add(eta_temp.get(j)[1].multiply(ex_prob).setScale(accuracy, RoundingMode.HALF_UP)
                        .divide(rootProb, getAccuracy(), RoundingMode.HALF_UP));
            }

        }
        sigma = null;

        return rootProb;
    }

    /**
     * Returns the probability of a BDD. During the execution it updates the
     * values contained in sigma
     *
     * @param node the input BDD
     * @param nex number of the current example
     * @return the probability of BDD
     */
    private BigDecimal probPath(BDD node, BigDecimal[] sigma,
            List<BigDecimal[]> eta_temp, EMTable nodesF, EMTable nodesB, int nex) {
        int index, position;
        BigDecimal res;
        BigDecimal p, notP, pt, pf, BChild0, BChild1, e0, e1;
        BigDecimal value_p;
        BigDecimal[] eta_rule;
        BDD T, F;

        if (node.isOne()) {
            return MathUtilities.getBigDecimal("1", accuracy);
        } else if (node.isZero()) {
            return MathUtilities.getBigDecimal("0", accuracy);
        } else {
            value_p = nodesB.get_value(node);

            if (value_p != null) {
                return value_p;
            } else {
                index = node.var();
                p = probs_ex.get(nex).get(index);
                notP = MathUtilities.getBigDecimal("1", accuracy).subtract(p);

                T = node.high();
                F = node.low();

                pf = probPath(F, sigma, eta_temp, nodesF, nodesB, nex);
                pt = probPath(T, sigma, eta_temp, nodesF, nodesB, nex);

                BChild0 = pf.multiply(notP).setScale(accuracy, RoundingMode.HALF_UP);
                BChild1 = pt.multiply(p).setScale(accuracy, RoundingMode.HALF_UP);

                value_p = nodesF.get_value(node);

                e0 = value_p.multiply(BChild0).setScale(accuracy, RoundingMode.HALF_UP);
                e1 = value_p.multiply(BChild1).setScale(accuracy, RoundingMode.HALF_UP);

                eta_rule = eta_temp.get(vars_ex.get(nex).get(index));
                eta_rule[0] = eta_rule[0].add(e0);
                eta_rule[1] = eta_rule[1].add(e1);

                res = BChild0.add(BChild1);

                nodesB.add_node(node, res);

                position = mgr.var2Level(index);//ithVar(index).level();
                position++;
                if (position < boolVars_ex.get(nex)) {
                    sigma[position] = sigma[position].add(e0).add(e1);
                }

                if (!(T.isOne() || T.isZero())) {
                    index = T.var();
                    position = mgr.var2Level(index);//ithVar(index).level();
                    sigma[position] = sigma[position].subtract(e1);
                }

                if (!(F.isOne() || F.isZero())) {
                    index = F.var();
                    position = mgr.var2Level(index);//ithVar(index).var();
                    sigma[position] = sigma[position].subtract(e0);
                }

                return res;
            }
        }
    }

    /**
     * @param merge the merge to set
     */
    public void setMerge(boolean merge) {
        this.merge = merge;
    }

    /**
     * Updates the parameters of the probabilistic axioms using the values
     * calculated by the expectation step.
     *
     * @param eta
     */
    protected void maximization(List<BigDecimal[]> eta) {

//        try {
//            Logger tempLog = Logger.getLogger("etas");
//            tempLog.setAdditivity(false);
//            tempLog.addAppender(new FileAppender(new PatternLayout(), "etas.log", true));
//            tempLog.info("etas: [");
//            for (BigDecimal[] e : eta) {
//                tempLog.info(Arrays.asList(e));
//            }
//            tempLog.info("]");
//        } catch (IOException e) {
//            logger.error(e.getMessage());
//            System.exit(-1);
//        }
        BigDecimal sum, zero = new BigDecimal(0);
        BigDecimal probs_rule;
        BigDecimal[] eta_rule;
        int nRules = probAxioms.size();

        for (int r = 0; r < nRules; r++) {
            if (usedAxioms[r]) {
                eta_rule = eta.get(r);

                sum = eta_rule[0].add(eta_rule[1]);

                if (sum.compareTo(zero) == 0) {
                    arrayProb.set(r, sum);
                } else {
                    arrayProb.set(r, eta_rule[1].divide(sum, accuracy, RoundingMode.HALF_UP));
                }
            }
        }

//        try {
//            Logger tempLog = Logger.getLogger("arrayProb");
//            tempLog.setAdditivity(false);
//            tempLog.addAppender(new FileAppender(new PatternLayout(), "probs.log", true));
//            tempLog.debug("arrayProb: [" + arrayProb + "]");
//        } catch (IOException e) {
//            logger.error(e.getMessage());
//            System.exit(-1);
//        }
        for (int e = 0; e < probs_ex.size(); e++) {

            //for (int j = 0; j < nVars_ex.get(e); j++){
            for (int j = 0; j < boolVars_ex.get(e); j++) {
                probs_rule = arrayProb.get(vars_ex.get(e).get(j));
                probs_ex.get(e).set(j, probs_rule);
            }
        }
    }

    /**
     * Check the conditions in order to stop the EM cycle
     *
     * @param diffLL current Log-likelihood difference
     * @param ratioLL current Log-likelihood ratio
     * @param iter current iteration
     * @return
     */
    protected boolean checkEMConditions(BigDecimal diffLL, BigDecimal ratioLL, long iter) {
        return ((diffLL.compareTo(this.getDiffLL()) > 0) && (ratioLL.compareTo(this.getRatioLL()) > 0)
                && (iter < this.getMaxIterations()));
    }

    /**
     * @return the ontologyFileName
     */
    public String getOntologyFileName() {
        return ontologyFileName;
    }

    /**
     * @param ontologyFileName the ontologyFileName to set
     */
    public void setOntologyFileName(String ontologyFileName) {
        this.ontologyFileName = ontologyFileName;
    }

    /**
     * @return the probOntologyFileName
     */
    public String getProbOntologyFileName() {
        return probOntologyFileName;
    }

    /**
     * @param probOntologyFileName the probOntologyFileName to set
     */
    public void setProbOntologyFileName(String probOntologyFileName) {
        this.probOntologyFileName = probOntologyFileName;
    }

    private String[] createFileNames() {
        String[] files;
        if (probOntologyFileName == null) {
            files = new String[1];
            files[0] = ontologyFileName;
        } else {
            files = new String[2];
            files[0] = ontologyFileName;
            files[1] = probOntologyFileName;
        }
        return files;
    }

    /**
     * @return the ontology
     */
    public OWLOntology getOntology() {
        return ontology;
    }

    /**
     * @param maxIterations the maxIterations to set
     */
    public void setMaxIterations(long maxIterations) {
        this.maxIterations = maxIterations;
    }

    /**
     * @return the maxIterations
     */
    public long getMaxIterations() {
        return maxIterations;
    }

    /**
     * @return the maxExplanations
     */
    public int getMaxExplanations() {
        return maxExplanations;
    }

    /**
     * @return the probOntology
     */
    public OWLOntology getProbOntology() {
        return probOntology;
    }

    /**
     * @return the positiveExamples
     */
    public List<OWLAxiom> getPositiveExamples() {
        return positiveExamples;
    }

    /**
     * @return the negativeExamples
     */
    public List<OWLAxiom> getNegativeExamples() {
        return negativeExamples;
    }

    /**
     * @return the all the examples
     */
    public List<OWLAxiom> getExamples() {
        return examples;
    }

    /**
     * @return the diffLL
     */
    public BigDecimal getDiffLL() {
        return diffLL;
    }

    /**
     * @return the ratioLL
     */
    public BigDecimal getRatioLL() {
        return ratioLL;
    }

    /**
     * @return the example_weight
     */
    public BigDecimal[] getExample_weights() {
        return example_weights;
    }

    /**
     * @param example_weights the example_weight to set
     */
    public void setExample_weights(BigDecimal[] example_weights) {
        this.example_weights = example_weights;
    }

    /**
     * @return the accuracy
     */
    public int getAccuracy() {
        return accuracy;
    }

    /**
     * @param accuracy the accuracy to set
     */
    public void setAccuracy(int accuracy) {
        this.accuracy = accuracy;
    }

    /**
     * @return the PMap
     */
    public Map<OWLAxiom, BigDecimal> getPMap() {
        return pMap;
    }

    public void reset() {
        mgr.done();
        example_weights = null;
        timers = null;
    }

    /**
     * Check whether EDGE is initialized
     *
     * @return true if EDGE is initialized, false otherwise
     */
    public boolean isInitialized() {
        return initialized;
    }

    /**
     * @return the timers
     */
    public Timers getTimers() {
        return timers;
    }

    /**
     * @param timers the timers to set
     */
    public void setTimers(Timers timers) {
        this.timers = timers;
    }

    private void updateTimers(String algo, Timers timers) {
        for (Timer timer : timers.getTimers()) {
            String timerName;
            if (timer.getName().equals("main")) {
                timerName = algo;

//                Timer t = this.timers.getTimer(algo);
//                
//                
//                
//                this.timers.createTimer(algo).add(bundleTimer);
            } else {
                timerName = algo + "." + timer.getName();
            }

            Timer t = this.timers.getTimer(timerName);
            if (t == null) {
                this.timers.createTimer(timerName).add(timer);
            } else {
                t.add(timer);
            }
        }
    }
}
