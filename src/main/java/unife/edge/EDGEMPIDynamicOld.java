/**
 *  This file is part of EDGE.
 * 
 *  EDGE is a probabilistic parameter learner for OWL 2 ontologies. 
 *  It learns the probabilistic values of the probabilistic axioms starting from a set of positive and negative axioms. 
 * 
 *  EDGE can be used both as module and as standalone.
 *  
 *  EDGE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package unife.edge;

import com.clarkparsia.owlapiv3.OWL;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import mpi.MPI;
import mpi.MPIException;
import mpi.Status;
import net.sf.javabdd.BDD;
import net.sf.javabdd.BDDFactory;
import org.apache.log4j.Logger;
import org.mindswap.pellet.utils.Timers;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClassAssertionAxiom;
import org.semanticweb.owlapi.model.OWLException;
import unife.bundle.Bundle;
import unife.bundle.QueryResult;
import unife.bundle.logging.BundleLoggerFactory;
import unife.bundle.utilities.BundleUtilities;
import unife.edge.mpi.EDGEMPIConstants;
import unife.edge.mpi.MPIUtilities;
import unife.edge.mpi.UnexpectedSignalValueException;
import unife.edge.mpi.RecvObjectStatus;
import unife.math.utilities.MathUtilities;
import static unife.edge.mpi.EDGEMPIConstants.*;
import unife.edge.utilities.EDGEConstants.Type;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>, Riccardo Zese
 * <riccardo.zese@unife.it>
 */
@Deprecated
public class EDGEMPIDynamicOld extends EDGE {

    // max number of attempts to send an example
    private final int maxNumAttempts = 3;
    private int countExamples = 0;
    //private int countComputedExamples = 0; // forse si può rimuovere
    private final Object countExamplesLock = new Object();

    private final Object countActuallySentExamplesLock = new Object();
    private int countActuallySentExamples = 0;

    private final boolean MASTER; // if true the current process is the master
    private final int myRank; // rank of the current process (0 is master)
    private static Logger logger;
    //private final List<Map.Entry<OWLAxiom, Type>> initialExamples = new ArrayList<>();
    private final List<Map.Entry<OWLAxiom, Type>> initialExamples = Collections.synchronizedList(new ArrayList<Map.Entry<OWLAxiom, Type>>()); // all the startig examples
    private List<List<Map.Entry<OWLAxiom, Type>>> examplesDistribution; // examples assiociated to each slave

    public EDGEMPIDynamicOld() {
        try {
            myRank = MPI.COMM_WORLD.getRank();
            MASTER = MPIUtilities.isMaster(MPI.COMM_WORLD);
            logger = Logger.getLogger(EDGEMPIDynamicOld.class.getName(),
                    new BundleLoggerFactory());
        } catch (MPIException mpiEx) {
            String msg = "Impossible to get the Rank: " + mpiEx.getMessage();
            logger.error(msg);
            throw new RuntimeException(mpiEx);
        }
    }

//    @Override
//    public void init() {
//        super.init();
//        if (MASTER) {
//            // populate initalExamples list
//            for (OWLAxiom pEx : getPositiveExamples()) {
//                initialExamples.add(new AbstractMap.SimpleEntry<>(pEx, Type.POSITIVE));
//            }
//            for (OWLAxiom nEx : getNegativeExamples()) {
//                initialExamples.add(new AbstractMap.SimpleEntry<>(nEx, Type.NEGATIVE));
//            }
//        }
//    }
    @Override
    protected void printTestResult(Timers timers, BigDecimal[] probs) {
        if (MASTER) {
            logger.info("");
            logger.info("============ Result ============");
            logger.info("");
            int index = 0;
            for (Map.Entry<OWLAxiom, Type> query : initialExamples) {
                logger.info("Query " + (index + 1) + " of " + initialExamples.size()
                        + " (" + (int) (((index + 1) * 100) / initialExamples.size()) + "%)");
                if (query.getValue() == Type.POSITIVE) {
                    logger.info("Positive Example: " + query.getKey());
                } else {
                    logger.info("Negative Example: " + query.getKey());
                }
                logger.info("Prob: " + probs[index]);
                index++;
            }
            logger.info("");
            logger.info("================================");
            logger.info("");
            StringWriter sw = new StringWriter();
            timers.print(sw, true, null);
            logger.info(sw);
        } else { // SLAVE
//            super.printTestResult(timers, probs);
        }
    }

    @Override
    public Map<OWLAxiom, BigDecimal> preparePMap() {
        Map<OWLAxiom, BigDecimal> PMap;
        if (MASTER) {
            PMap = super.preparePMap();
            if (PMap == null) {
                logger.warn("I don't have probabilistic axioms in PMap! I have nothing to learn!");
                throw new RuntimeException("Nothing to do! See log for details...");
            }
            sendPMap(PMap);
        } else {
            PMap = receivePMap();
        }
        return PMap;
    }

    private void sendPMap(Map<OWLAxiom, BigDecimal> PMap) {
        try {
            byte[] bufferRulesName, bufferProbs, bufferSend;
            bufferRulesName = MPIUtilities.objectToByteArray(probAxioms);
            List<BigDecimal> probabilitiesOfPMap = new ArrayList<>(probAxioms.size());
            for (OWLAxiom ax : probAxioms) {
                probabilitiesOfPMap.add(PMap.get(ax));
            }
            bufferProbs = MPIUtilities.objectToByteArray(probabilitiesOfPMap);
            int[] bufferDim = {bufferRulesName.length, bufferProbs.length};
            bufferSend = new byte[bufferRulesName.length + bufferProbs.length];
            // copy the buffer containing the probabilistic axioms into the buffer to send
            System.arraycopy(bufferRulesName, 0, bufferSend, 0, bufferRulesName.length);
            // copy the buffer containing the probability values into the buffer or send
            System.arraycopy(bufferProbs, 0, bufferSend, bufferRulesName.length, bufferProbs.length);

            byte[] bufferPack = new byte[bufferSend.length + Integer.SIZE / 8];
            int pos = MPI.COMM_WORLD.pack(bufferDim, 1, MPI.INT, bufferPack, 0);
            MPI.COMM_WORLD.pack(bufferSend, bufferSend.length, MPI.BYTE, bufferPack, pos);

            logger.debug(myRank + " - Sending PMap...");
            // send the dimension of the buffer that the slaves are going to receive
            MPI.COMM_WORLD.bcast(new int[]{bufferPack.length}, 1, MPI.INT, 0);
            // send the buffer
            MPI.COMM_WORLD.bcast(bufferPack, bufferPack.length, MPI.PACKED, 0);
            logger.debug(myRank + " - Sent " + bufferPack.length + " bytes...");
        } catch (Exception ex) {
            logger.error(myRank + " - Error: " + ex);
            throw new RuntimeException(ex);
        }
    }

    private Map<OWLAxiom, BigDecimal> receivePMap() {
        Map<OWLAxiom, BigDecimal> PMap = null;
        try {
            int[] totalBufferDim = new int[1];
            int[] buffersDim = new int[2];
            logger.debug(myRank + " - Receiving PMap...");
            // reading the size of the incoming buffer
            MPI.COMM_WORLD.bcast(totalBufferDim, 1, MPI.INT, 0);
            logger.debug(myRank + " - Receiving " + totalBufferDim[0] + " bytes...");
            // receiving the buffer
            byte[] bufferRecvTot = new byte[totalBufferDim[0]];
            MPI.COMM_WORLD.bcast(bufferRecvTot, totalBufferDim[0], MPI.PACKED, 0);
            logger.debug(myRank + " - Receiving pack OK...");
            // get the dimension in bytes of the list of axioms
            int pos = MPI.COMM_WORLD.unpack(bufferRecvTot, 0, buffersDim, 1, MPI.INT);
            // get the dimension in bytes of the list of probabilities
            buffersDim[1] = totalBufferDim[0] - pos - buffersDim[0];
            byte[] bufferRulesName = new byte[buffersDim[0]];
            byte[] bufferProbs = new byte[buffersDim[1]];
            pos = MPI.COMM_WORLD.unpack(bufferRecvTot, pos, bufferRulesName, bufferRulesName.length, MPI.BYTE);
            MPI.COMM_WORLD.unpack(bufferRecvTot, pos, bufferProbs, bufferProbs.length, MPI.BYTE);

            // downcast
            probAxioms = (List<OWLAxiom>) MPIUtilities.byteArrayToObject(bufferRulesName);
            //logger.trace(myRank + " - Received: " + rulesName);
            List<BigDecimal> obj = (List<BigDecimal>) MPIUtilities.byteArrayToObject(bufferProbs);
            logger.debug(myRank + " - Received probabilities");
            PMap = new HashMap<>();
            probAxiomsString = new ArrayList<>();
            arrayProb = new ArrayList<>();
            String array = myRank + " - [ ";
            for (int i = 0; i < probAxioms.size(); i++) {
                OWLAxiom ax = probAxioms.get(i);
                PMap.put(ax, obj.get(i));
                arrayProb.add(obj.get(i));
                probAxiomsString.add(BundleUtilities.getManchesterSyntaxString(ax));
                array += "" + ax + " => " + obj.get(i);
            }
//            logger.debug(myRank + " - Received: " + obj);
            logger.trace(array + " ]");
        } catch (Exception ex) {
            logger.error("Some problem in receiving PMap: " + ex);
            throw new RuntimeException(ex);
        }
        return PMap;
    }

    @Override
    public BigDecimal[] computeExamples(Map<OWLAxiom, BigDecimal> PMap, List<BDD> BDDs, boolean test) {
        BigDecimal[] probs;
        if (MASTER) {
            // populate initalExamples list
            for (OWLAxiom pEx : getPositiveExamples()) {
                initialExamples.add(new AbstractMap.SimpleEntry<>(pEx, Type.POSITIVE));
            }
            for (OWLAxiom nEx : getNegativeExamples()) {
                initialExamples.add(new AbstractMap.SimpleEntry<>(nEx, Type.NEGATIVE));
            }

            try {
                logger.debug(myRank + " - Start finding explanations for every example...");
                // Send an example to each slave + itself
                int numSlaves = MPIUtilities.getSlaves(MPI.COMM_WORLD);
                examplesDistribution = new ArrayList<>(numSlaves + 1);
                examplesDistribution.add(new ArrayList<Map.Entry<OWLAxiom, Type>>());
                Map.Entry<OWLAxiom, Type> example;
                if (numSlaves > initialExamples.size()) {
                    logger.warn(myRank + " - The number of slave exceeds the number"
                            + " of examples");
                    // soluzione inefficiente, bisognerebbe trovare un modo alternativo
                    // per uccidere i processi in eccesso, magari con un signal
                    numSlaves = initialExamples.size();
                }
                // send an example to each slave
                for (int i = 1; i <= numSlaves; i++) {
                    example = nextExample();
                    //countComputedExamples++;
                    (new Thread(new EDGEMPIDynamicOld.ExampleSender(i, example, i))).start();
                    examplesDistribution.add(new ArrayList<Map.Entry<OWLAxiom, Type>>());
                    examplesDistribution.get(i).add(example);
                }
                // create array of the example probabilities
                probs = new BigDecimal[initialExamples.size()];
                // create server
                Runnable listener = new EDGEMPIDynamicOld.ExamplesListener(probs, countExamples, test);
                Thread exampleServer = new Thread(listener);
                // reserve an example for the current thread
                example = nextExample();
                examplesDistribution.get(EDGEMPIConstants.MASTER).add(example);
                //countComputedExamples++;
                // start sever
                exampleServer.start();
                // Master local computation of the examples
                while (example != null) {
                    // computo esempio
                    int idxExample = initialExamples.indexOf(example);
                    logger.debug(myRank + " - Computing query " + (idxExample + 1)
                            + " of " + initialExamples.size());
                    QueryResult queryResult = computeExample(PMap, example);
                    logger.debug(myRank + " - " + BundleUtilities.getManchesterSyntaxString(example.getKey())
                            + " - prob: " + queryResult.getQueryProbability()
                            + " - tag: " + (idxExample + 1)
                            + " - #vars: " + queryResult.getProbAxioms().size());
                    // set results
                    probs[idxExample] = queryResult.getQueryProbability();
//                    vars_ex.add(new ArrayList<Integer>());
//                    probs_ex.add(new ArrayList<Double>());
//                    boolVars_ex.add(0);
                    if (!test) {
                        BDDs.add(queryResult.getBDD());
                        super.init_bdd(queryResult);
                    }
                    synchronized (countExamplesLock) {
                        logger.debug(myRank + " - T_1");
                        logger.debug(myRank + " - Sent queries: " + countExamples
                                + " of " + initialExamples.size()
                                + " (" + ((int) (countExamples * 100 / initialExamples.size())) + "%)");
//                        if (countExamples < initialExamples.size()) {
                        //countComputedExamples++;
                        example = nextExample();
                        if (example != null) {
                            examplesDistribution.get(EDGEMPIConstants.MASTER).add(example);
                        }
//                        }
                    }
                }

                exampleServer.join();
            } catch (MPIException mpiEx) {
                logger.error("Some problem during the sending of the examples: " + mpiEx);
                throw new RuntimeException(mpiEx);
            } catch (InterruptedException iEx) {
                logger.error("Some problem during the sending of the examples: " + iEx);
                throw new RuntimeException(iEx);
            }

        } else { // SLAVE
            List<BigDecimal> probExamples = new ArrayList<>();
            List<Integer> queryTags = new ArrayList<>();
            int receivedExamples = 0;
            boolean stop = false;
            int tag = -1;
            QueryResult queryResult = null;
            int attempt = 1;
            do {
                try {
                    // listen to the MASTER (of puppets)
                    logger.debug(myRank + " - Receiving example...");
                    // check if is a stop
                    Status recvStat = MPI.COMM_WORLD.probe(EDGEMPIConstants.MASTER, MPI.ANY_TAG);
                    if (recvStat.getTag() == 0) { // I know that is a STOP
                        // but I read it anyway just to be sure
                        int[] signal = new int[1];
                        try {
                            MPI.COMM_WORLD.recv(signal, 1, MPI.INT, EDGEMPIConstants.MASTER, 0);
                        } catch (MPIException mpiEx) {
                            logger.fatal("Some problem during the receiving of the signal: " + mpiEx.getMessage());
                            throw new RuntimeException(mpiEx);
                        }
                        if (STOP == signal[0]) {
                            stop = true;
                        } else {
                            String msg = "Unexpected signal value " + signal[0];
                            logger.fatal(msg);
                            throw new UnexpectedSignalValueException(msg);
                        }

                    } else if (!stop) {
                        // receive the example
                        RecvObjectStatus stat = MPIUtilities.recvObject(EDGEMPIConstants.MASTER, MPI.ANY_TAG, MPI.COMM_WORLD);
                        Map.Entry<OWLAxiom, Type> example = (AbstractMap.SimpleEntry<OWLAxiom, Type>) stat.getObj();
                        receivedExamples++;
                        // compute example/query
                        tag = stat.getStatus().getTag();
                        queryTags.add(tag);
                        logger.debug(myRank + " - Received query: " + example.getKey()
                                + " Type " + (example.getValue() == Type.POSITIVE ? "positive" : "negative")
                                + " Local: " + receivedExamples + " Global: " + tag);
                        queryResult = computeExample(PMap, example);
                        // set results
                        probExamples.add(queryResult.getQueryProbability());
                        if (!test) {
                            BDDs.add(queryResult.getBDD());
                            super.init_bdd(queryResult);
                        }
                        try {
                            // send probability of the query to the master
                            sendExampleResults(tag, queryResult.getQueryProbability(), test);
                        } catch (MPIException mpiEx) {
                            logger.error("Some problem during the sending of the results: " + mpiEx.getMessage());
                            throw new RuntimeException(mpiEx);
                        } catch (Exception ex) {
                            logger.error("Some problem during the sending of the results: " + ex.getMessage());
                            throw new RuntimeException(ex);
                        }
                    }

                } catch (MPIException mpiEx) {
                    logger.error("problem during the receiving of the example. Attempt: " + attempt);
                    if (attempt > maxNumAttempts) {
                        logger.fatal("Not recoverable problem during the receiving of the example: " + mpiEx.getMessage());
                        throw new RuntimeException(mpiEx);
                    }
                    logger.error("Retrying receiving the example");
                    attempt++;
                }

            } while (!stop);

            // convert List to array of BigDecimal
            probs = new BigDecimal[probExamples.size()];
            for (int i = 0; i < probExamples.size(); i++) {
                probs[i] = probExamples.get(i);
            }
        }
        return probs; //temp, just for removing error
    }

    private QueryResult computeExample(Map<OWLAxiom, BigDecimal> PMap, Map.Entry<OWLAxiom, Type> example) {
        Bundle bundle = startBundle();
        bundle.setPMap(PMap);
        QueryResult queryResult = null;
        OWLAxiom query = example.getKey();
        try {
            if (example.getValue() == Type.POSITIVE) {
                logger.debug(myRank + " - Positive Example: " + BundleUtilities.getManchesterSyntaxString(query));
                queryResult = bundle.computeQuery(query);
            } else { // Type.NEGATIVE
                // converting axiom
                OWLAxiom notQuery = OWL.classAssertion(((OWLClassAssertionAxiom) query).getIndividual(),
                        OWL.not(((OWLClassAssertionAxiom) query).getClassExpression()));
                // computing not axiom
                logger.debug(myRank + " - Negative Example: " + BundleUtilities.getManchesterSyntaxString(notQuery));
                queryResult = bundle.computeQuery(notQuery);
                // second method
                if (queryResult == null || queryResult.getExplanations().isEmpty()) {
                    logger.debug("Trying the second method...");
                    queryResult = bundle.computeQuery(query);
                    queryResult.setBDD(queryResult.getBDD().not());
                    queryResult.setQueryProbability(MathUtilities.getBigDecimal("1", getAccuracy()).subtract(queryResult.getQueryProbability()));
                }
            }
        } catch (OWLException owle) {
            logger.error("It was impossible to compute the probability of the query");
            logger.error(owle.getMessage());
            logger.trace(owle.getStackTrace());

            System.err.println("Terminated because of an internal error. See the log for details");
            throw new RuntimeException(owle);
        }

        if (queryResult == null) {
            logger.fatal("The result of BUNDLE algorithm is null");
            throw new RuntimeException("Impossible to compute the query");
        }
        bundle.finish();
        return queryResult;
    }

    private void sendExampleResults(int exampleTag, BigDecimal exampleProb, boolean test) throws MPIException, Exception {
        // send example probability and the number of used variables
        // packing
        byte[] bufferProb = MPIUtilities.objectToByteArray(exampleProb);
        byte[] buffer = new byte[bufferProb.length + Integer.SIZE / 8];
        int pos = MPI.COMM_WORLD.pack(bufferProb, bufferProb.length, MPI.BYTE, buffer, 0);
        if (!test) {
            int numVars = boolVars_ex.get(boolVars_ex.size() - 1);
            MPI.COMM_WORLD.pack(new int[]{numVars}, 1, MPI.INT, buffer, pos);
            logger.debug(myRank
                    + " - prob: " + exampleProb
                    + " - number of vars: " + numVars
                    + " - tag: " + exampleTag);
            // sending
            MPI.COMM_WORLD.send(buffer, buffer.length, MPI.PACKED, EDGEMPIConstants.MASTER, exampleTag);
            // send indices and probabilities of the used vars (boolean variables <-> axioms)
            if (numVars > 0) {
                logger.debug(myRank + " - Sending vars values...");
                // creating and populating buffers
                int[] indexVars = new int[numVars];
                List<Integer> lastVarsInd = vars_ex.get(vars_ex.size() - 1);
                for (int i = 0; i < numVars; i++) {
                    indexVars[i] = lastVarsInd.get(i);
                }
                // sending indices of the used axioms
                MPI.COMM_WORLD.send(indexVars, indexVars.length, MPI.INT, EDGEMPIConstants.MASTER, exampleTag);
            }
        } else {
            logger.debug(myRank + "- prob: " + exampleProb + " - tag: " + exampleTag);
            // sending only the probabiities
            MPI.COMM_WORLD.send(buffer, buffer.length, MPI.PACKED, EDGEMPIConstants.MASTER, exampleTag);
        }

    }

    @Override
    protected EDGEStatImpl EM(List<BDD> BDDs, BigDecimal[] probs) {
        logger.info(myRank + " - Start EM Algorithm\n\t- n. of probabilistic axioms:\t" + probAxioms.size()
                + "\n\t- n. of examples:\t\t" + getExamples().size());

        int lenNodes = BDDs.size();
        int nRules = probAxioms.size();
        BigDecimal CLL0; //= MathUtilities.getBigDecimal(-2.2 * Math.pow(10, 10), getAccuracy());  // -inf
        BigDecimal CLL1 = MathUtilities.getBigDecimal(-1.7 * Math.pow(10, 8), getAccuracy());    // +inf
        BigDecimal ratio, diff;
        int cycle = 0;

        List<BigDecimal[]> eta = new ArrayList<>();
        for (int i = 0; i < nRules; i++) {
            BigDecimal[] etaIntTemp = new BigDecimal[2];
            etaIntTemp[0] = MathUtilities.getBigDecimal("0", getAccuracy());
            etaIntTemp[1] = MathUtilities.getBigDecimal("0", getAccuracy());
            eta.add(etaIntTemp);
        }

        if (getExample_weights() == null || getExample_weights().length == 0) {
            BigDecimal[] example_weights_local = new BigDecimal[lenNodes];
            for (int i = 0; i < lenNodes; i++) {
                example_weights_local[i] = MathUtilities.getBigDecimal("1.0", getAccuracy());
            }
            setExample_weights(example_weights_local);
        }

//        diff = CLL1.subtract(CLL0);
//        ratio = diff.divide(CLL0.abs(), getAccuracy(),  RoundingMode.HALF_UP);
//        logger.debug(myRank + " - probabilistic values before EM cycle"
//                + ": " + arrayProb);
        // EM cycle
        do {
            List<BigDecimal[]> eta_local = new ArrayList<>(eta.size());
            cycle++;
            logger.debug(myRank + " - EM cycle: " + cycle);
            CLL0 = CLL1;
            CLL1 = expectation(BDDs, getExample_weights(), eta, lenNodes, eta_local, cycle);
            maximization(eta_local);
            diff = CLL1.subtract(CLL0);
            ratio = diff.divide(CLL0.abs(), getAccuracy(), RoundingMode.HALF_UP);
            logger.debug("Log-likelihood: " + CLL1 + " cycle: " + cycle);
        } while (checkEMConditions(diff, ratio, cycle));

        logger.info("EM completed.");
        logger.info("\n Final Log-likelihood: " + CLL1);

        return new EDGEStatImpl(cycle, CLL1, probAxioms, arrayProb);
    }

    protected BigDecimal expectation(List<BDD> nodes_ex, BigDecimal[] example_weights,
            List<BigDecimal[]> eta, int lenNodes, List<BigDecimal[]> eta_local, int cycle) {
        logger.debug(myRank + " - Computing Expectation");
        int nRules = probAxioms.size();
        // slaves and master compute the expectetion step
        logger.debug(myRank + " - Computing Expectation step locally. #BDDs: " + nodes_ex.size() + " Cycle: " + cycle);
        BigDecimal LL = super.expectation(nodes_ex, example_weights, eta, lenNodes);

        if (MASTER) {
            if (cycle == 1) {
                try {
                    // in the first iteration the master must send the START signal
                    // to the slaves in order to start to receive the expectation results
                    MPIUtilities.sendSignal(ALL_BCAST, START, MPI.COMM_WORLD);
                } catch (MPIException mpiEx) {
                    logger.error(myRank + " - Error: " + mpiEx.getMessage());
                    throw new RuntimeException(mpiEx);
                }
            }
            logger.debug(myRank + " - Receiving expectation results...");
            // get the number of slaves
            int numSlaves;
            try {
                numSlaves = MPIUtilities.getSlaves(MPI.COMM_WORLD);
            } catch (MPIException mpiEx) {
                String msg = "Cannot get the current number of slaves";
                logger.error(msg);
                throw new RuntimeException(mpiEx);
            }
            if (numSlaves > initialExamples.size()) {
                numSlaves = initialExamples.size();
            }
            // set local etas
            for (BigDecimal[] eta_i : eta) {
                BigDecimal[] eta_i_local = {eta_i[0], eta_i[1]};
                eta_local.add(eta_i_local);
            }
            //receive the results
            for (int i = 1; i <= numSlaves; i++) {
                try {
                    // eta + LL_i
                    Status status = MPI.COMM_WORLD.probe(i, 0);
                    byte[] bufferRecvTot = new byte[status.getCount(MPI.PACKED)];
                    MPI.COMM_WORLD.recv(bufferRecvTot, bufferRecvTot.length,
                            MPI.PACKED, i, 0);
                    int[] dimEtasBuffer = new int[1];
                    int pos = MPI.COMM_WORLD.unpack(bufferRecvTot, 0, dimEtasBuffer, 1, MPI.INT);
                    byte[] etasBuffer = new byte[dimEtasBuffer[0]];
                    pos = MPI.COMM_WORLD.unpack(bufferRecvTot, pos,
                            etasBuffer, dimEtasBuffer[0], MPI.BYTE);
                    List<List<BigDecimal>> etasRemote = (List<List<BigDecimal>>) MPIUtilities.byteArrayToObject(etasBuffer);
                    byte[] bufferLL = new byte[bufferRecvTot.length - etasBuffer.length - (Integer.SIZE / 8)];
                    MPI.COMM_WORLD.unpack(bufferRecvTot, pos, bufferLL, bufferLL.length, MPI.BYTE);
                    BigDecimal recvLL = (BigDecimal) MPIUtilities.byteArrayToObject(bufferLL);

                    for (int j = 0; j < nRules; j++) {
                        eta_local.get(j)[0] = eta_local.get(j)[0].add(etasRemote.get(j).get(0));
                        eta_local.get(j)[1] = eta_local.get(j)[1].add(etasRemote.get(j).get(1));
                    }

                    logger.debug(myRank + " - LL from " + i + "-th slave: " + recvLL);
                    LL = LL.add(recvLL);
                    logger.debug(myRank + " - LL after " + i + "-th slave: " + LL);

                } catch (MPIException mpiEx) {
                    logger.error(myRank + " - Error: " + mpiEx.getMessage());
                    throw new RuntimeException(mpiEx);
                }

            }
        } else { // SLAVE
            try {
                // in the first iteration the slave must wait the START signal
                // before sending the results of the expectation step
                if (cycle == 1) {
                    int[] signal = new int[1];
                    MPI.COMM_WORLD.bcast(signal, 1, MPI.INT, EDGEMPIConstants.MASTER);
                    if (signal[0] != START) {
                        String msg = "Unexpected signal value " + signal[0];
                        logger.fatal(msg);
                        throw new UnexpectedSignalValueException(msg);
                    }
                }

                int nEtas = nRules * 2;
                List<List<BigDecimal>> etas = new ArrayList<>();
                // setting buffer to be sent
                for (int idxAxiom = 0; idxAxiom < nRules; idxAxiom++) {
                    List<BigDecimal> etasj = new ArrayList<>();
                    etasj.add(eta.get(idxAxiom)[0]);
                    etasj.add(eta.get(idxAxiom)[1]);
                    etas.add(etasj);
                }
                logger.debug(myRank + " - Sending expectation results...");
                byte[] etasBuffer = MPIUtilities.objectToByteArray(etas);
                byte[] LLBuffer = MPIUtilities.objectToByteArray(LL);
                byte[] sendBuffer = new byte[(Integer.SIZE / 8) + etasBuffer.length + LLBuffer.length];
                int pos = MPI.COMM_WORLD.pack(new int[]{etasBuffer.length}, 1, MPI.INT, sendBuffer, 0);
                pos = MPI.COMM_WORLD.pack(etasBuffer, etasBuffer.length, MPI.BYTE, sendBuffer, pos);
                MPI.COMM_WORLD.pack(LLBuffer, LLBuffer.length, MPI.BYTE, sendBuffer, pos);
                logger.debug(myRank + " - Sending etas + LL (" + (nEtas + 1) + " values)");
                MPI.COMM_WORLD.send(sendBuffer, sendBuffer.length, MPI.PACKED, 0, 0);
            } catch (MPIException mpiEx) {
                logger.error(myRank + " - Error: " + mpiEx.getMessage());
                throw new RuntimeException(mpiEx);
            }
        }
        return LL;
    }

    @Override
    protected void maximization(List<BigDecimal[]> eta) {
        logger.debug(myRank + " - maximization...");
        if (MASTER) {
            super.maximization(eta);
            // send axiomsProbBuffer to the slave. arrayProb contains the probabilities 
            // of the probabilistic axioms
            byte[] axiomsProbsBuffer = MPIUtilities.objectToByteArray(arrayProb);

//            logger.debug(myRank + " - sending to slaves new probabilistic "
//                    + "values for the axioms: [" + arrayProb + "]");
            try {
                MPI.COMM_WORLD.bcast(new int[]{axiomsProbsBuffer.length}, 1, MPI.INT, myRank);
                MPI.COMM_WORLD.bcast(axiomsProbsBuffer, axiomsProbsBuffer.length, MPI.BYTE, myRank);
            } catch (MPIException mpiEx) {
                logger.error(myRank + " - Error: " + mpiEx);
                throw new RuntimeException(mpiEx);
            }
        } else { //SLAVE
            // receive array of probabilities of the probabilistic axioms
            try {
                logger.debug(myRank + " - Receiving new values for probabilistic axioms");
                int axiomsProbsBufferDim[] = new int[1];
                MPI.COMM_WORLD.bcast(axiomsProbsBufferDim, 1, MPI.INT, EDGEMPIConstants.MASTER);
                byte[] axiomsProbsBuffer = new byte[axiomsProbsBufferDim[0]];
                MPI.COMM_WORLD.bcast(axiomsProbsBuffer, axiomsProbsBuffer.length, MPI.BYTE, EDGEMPIConstants.MASTER);
                //
                arrayProb = (List<BigDecimal>) MPIUtilities.byteArrayToObject(axiomsProbsBuffer);
//                logger.debug(myRank + " - new values: [" + arrayProb + "]");
            } catch (MPIException mpiEx) {
                logger.error(myRank + " - Error: " + mpiEx);
                throw new RuntimeException(mpiEx);
            } catch (Exception ex) {
                logger.error(myRank + " - Error: " + ex);
                throw new RuntimeException(ex);
            }
            // set probs_ex
            for (int i = 0; i < probs_ex.size(); i++) {
                for (int j = 0; j < probs_ex.get(i).size(); j++) {
                    BigDecimal prob = arrayProb.get(vars_ex.get(i).get(j));
                    probs_ex.get(i).set(j, prob);
                }
            }
        }
    }

    @Override
    protected boolean checkEMConditions(BigDecimal diffLL, BigDecimal ratioLL, long iter) {
        if (MASTER) {
            if ((diffLL.compareTo(this.getDiffLL()) > 0) && (ratioLL.compareTo(this.getRatioLL()) > 0)
                    && iter < getMaxIterations()) {
                try {
                    logger.debug(myRank + " - Cycle " + iter + ": Continue...");
                    MPIUtilities.sendSignal(ALL_BCAST, CONTINUE, MPI.COMM_WORLD);
                } catch (MPIException mpiEx) {
                    logger.error(myRank + " - Error: " + mpiEx.getMessage());
                    throw new RuntimeException(mpiEx);
                }
                return true;
            } else {
                logger.debug(myRank + " - Cycle " + iter + ": Stop...");
                try {
                    MPIUtilities.sendSignal(ALL_BCAST, STOP, MPI.COMM_WORLD);
                } catch (MPIException mpiEx) {
                    logger.error(myRank + " - Error: " + mpiEx.getMessage());
                    throw new RuntimeException(mpiEx);
                }
                return false;
            }
        } else { // SLAVE
            int[] signal = new int[1];
            try {
                logger.debug(myRank + " - Waiting for signal...");
                MPI.COMM_WORLD.bcast(signal, 1, MPI.INT, 0);
                logger.debug(myRank + " - Received: " + signal[0]);
            } catch (MPIException ex) {
                logger.error(myRank + " - Error: " + ex);
                throw new RuntimeException(ex);
            }
            boolean continueEM = signal[0] == CONTINUE;
            logger.debug(myRank + " - run flag: " + continueEM);
            return continueEM;
        }
    }

    private Map.Entry<OWLAxiom, Type> nextExample() {
        if (countExamples < initialExamples.size()) {
            countExamples++;
            return initialExamples.get(countExamples - 1);
        } else {
            return null;
        }
    }

    /**
     * Class used to send an example to a slave.
     */
    class ExampleSender implements Runnable {

        /**
         * Rank of the slave
         */
        private final int slave;
        /**
         * example to send
         */
        private final Map.Entry<OWLAxiom, Type> example;
        /**
         * Number of examples to send
         */
        private final int numExamplesToSend;

        /**
         * Constructor of ExampleSender.
         *
         * @param slave rank of the slave to which the example should be sent.
         * @param example example to send.
         */
        public ExampleSender(int slave, Map.Entry<OWLAxiom, Type> example, int numExamplesToSend) {
            super();
            this.slave = slave;
            this.example = example;
            this.numExamplesToSend = numExamplesToSend;
        }

        @Override
        public void run() {
            int pos = initialExamples.indexOf(example);
            boolean sent = false;
            int attempt = 1;
            while (!sent) {
                try {
                    logger.debug(myRank + " - Sending to slave: " + slave
                            + " example: " + example
                            + " (array pos.: " + pos
                            + " - tag: " + (pos + 1) + "). Attempt: " + attempt);
                    MPIUtilities.sendObject(example, slave, pos + 1, MPI.COMM_WORLD);
                    sent = true;
                    logger.debug(myRank + " - Sent to slave: " + slave
                            + " example: " + example
                            + " (array pos.: " + pos
                            + " - tag: " + (pos + 1) + " )");
                    synchronized (countActuallySentExamplesLock) {
                        countActuallySentExamples++;
                        if (numExamplesToSend == countActuallySentExamples) {
                            countActuallySentExamplesLock.notify();
                        }
                    }
                } catch (MPIException ex) {
                    logger.error("Problem during the sending of the example. Attempt: " + attempt);
                    if (attempt > maxNumAttempts) {
                        logger.error(myRank + " - Error: " + ex.getMessage());
                        throw new RuntimeException(ex);
                    }
                    logger.error("Retrying sending the example");
                    attempt++;
                }
            }
        }
    }

    /**
     * This class implements a thread that will be in listening and when a slave
     * is ready will send it an example.
     */
    class ExamplesListener implements Runnable {

        private BigDecimal[] probs;
        private int sentExamples;
        int recvExamples = 0;
        boolean test = false;

        public ExamplesListener(BigDecimal[] probs, int sentExamples, boolean test) {
            super();
            this.probs = probs;
            this.sentExamples = sentExamples;
            this.test = test;
        }

        @Override
        public void run() {
            try {
//                boolean allExamplesComputed = false;
                boolean stopRequest = false;
                Map.Entry<OWLAxiom, Type> example = null;
                int slaveId = 0;
                while (sentExamples != recvExamples) {

                    synchronized (countExamplesLock) {
                        logger.debug(myRank + " - T_2");
                        if (example != null) {
                            examplesDistribution.get(slaveId).add(example);
                        }
//                        if (countExamples < initialExamples.size()) {
                        //countComputedExamples++;
                        example = nextExample();
//                        }
//                        else {
//                            allExamplesComputed = true;
//                        }
                    }
                    Status recvStat = MPI.COMM_WORLD.probe(MPI.ANY_SOURCE, MPI.ANY_TAG);
                    byte[] buffer = new byte[recvStat.getCount(MPI.PACKED)];
                    logger.debug(myRank + " - Waiting for an example result...");
                    // receive
                    MPI.COMM_WORLD.recv(buffer, buffer.length, MPI.PACKED, MPI.ANY_SOURCE, MPI.ANY_TAG);
                    recvExamples++;
                    // get example probability
                    byte[] exProbBuff = new byte[buffer.length - (Integer.SIZE / 8)];
                    int pos = MPI.COMM_WORLD.unpack(buffer, 0, exProbBuff, exProbBuff.length, MPI.BYTE);
                    int indexEx = recvStat.getTag() - 1;
                    slaveId = recvStat.getSource();
                    probs[indexEx] = (BigDecimal) MPIUtilities.byteArrayToObject(exProbBuff);
                    String msg = myRank + " - RECV from " + recvStat.getSource()
                            + " - " + BundleUtilities.getManchesterSyntaxString(initialExamples.get(indexEx).getKey())
                            + " - prob: " + probs[indexEx]
                            + " - tag: " + recvStat.getTag();
                    if (!test) {
                        int[] numBoolVars = new int[1];
                        MPI.COMM_WORLD.unpack(buffer, pos, numBoolVars, 1, MPI.INT);
                        msg += " - #vars: " + numBoolVars[0];
                        // indices of the axioms used in the BDD
                        int[] axiomsIdxs = new int[numBoolVars[0]];
                        if (numBoolVars[0] > 0) {
                            // receive the buffer containing the indices of the used axioms
                            MPI.COMM_WORLD.recv(axiomsIdxs, axiomsIdxs.length, MPI.INT, slaveId, recvStat.getTag());
                        }

                        for (int j = 0; j < numBoolVars[0]; j++) {
                            usedAxioms[axiomsIdxs[j]] = true;
                        }
                    }
                    logger.debug(msg);
                    // I've received the query/example results, 
                    // now it's time to send another example
                    if (example == null) {
                        if (!stopRequest) {
                            // Send stop request to all the slaves
                            new Thread() {
                                @Override
                                public void run() {
                                    synchronized (countActuallySentExamplesLock) {
                                        while (sentExamples != countActuallySentExamples) {
                                            try {
                                                countActuallySentExamplesLock.wait();
                                            } catch (InterruptedException ex) {
                                                logger.fatal(myRank + " - Error: " + ex.getMessage());
                                                throw new RuntimeException(ex);
                                            }
                                        }
                                    }
                                    logger.debug(myRank + " - Sending stop signal to all slaves");
                                    try {
                                        MPIUtilities.sendSignal(ALL, STOP, MPI.COMM_WORLD);
                                    } catch (MPIException mpiEx) {
                                        logger.error(myRank + " - Error: " + mpiEx.getMessage());
                                        throw new RuntimeException(mpiEx);
                                    }
                                }
                            }.start();
                            stopRequest = true;
                        }
                    } else {
                        // send Example to the slave
                        if (slaveId == 0) {
                            throw new RuntimeException("Inconsistent slave receiver!");
                        }
                        sentExamples++;
                        (new Thread(new EDGEMPIDynamicOld.ExampleSender(slaveId, example, sentExamples))).start();

                    }
                }
                if (!stopRequest) {
                    logger.debug(myRank + " - Sending stop signal to all slaves");
                    MPIUtilities.sendSignal(ALL, STOP, MPI.COMM_WORLD);
                    stopRequest = true; // unused
                }
            } catch (MPIException mpiEx) {
                logger.error(myRank + " - Error: " + mpiEx.getMessage());
                throw new RuntimeException(mpiEx);
            } catch (Exception ex) {
                logger.error(myRank + " - Error: " + ex.getMessage());
                throw new RuntimeException(ex);
            }
        }

        /**
         * @param sentExamples the sentExamples to set
         */
        public void setSentExamples(int sentExamples) {
            this.sentExamples = sentExamples;
        }

    }

    @Override
    protected void printLearningResult(Timers timers) { //, BDDFactory.CacheStats fCacheStats, BDDFactory.GCStats fGCStats, BDDFactory.ReorderStats fReorderStats) {
        if (MASTER) {
            super.printLearningResult(timers); //, fCacheStats, fGCStats, fReorderStats);
        }
    }

}
