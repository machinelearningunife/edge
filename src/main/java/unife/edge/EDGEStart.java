/**
 *  This file is part of EDGE.
 * 
 *  EDGE is a probabilistic parameter learner for OWL 2 ontologies. 
 *  It learns the probabilistic values of the probabilistic axioms starting from a set of positive and negative axioms. 
 * 
 *  EDGE can be used both as module and as standalone.
 *  
 *  EDGE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package unife.edge;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import unife.bundle.logging.BundleLoggerFactory;

/**
 *
 * @author Riccardo Zese <riccardo.zese@unife.it>, Giuseppe Cota
 * <giuseppe.cota@unife.it>
 */
public class EDGEStart {

    public static EDGEUI getUI(String[] args, boolean mpi) {

        Options options = new Options();
        CommandLine cmd;

//        Option help = new Option("h", "help", false, "print this message");
//        options.addOption(help);
        Option gui = new Option("gui", "run GUI");
        options.addOption(gui);

        CommandLineParser parser = new PosixParser();
        try {
            // parse the command line arguments
            cmd = parser.parse(options, args, true);
            if (cmd.hasOption("gui")) {
                return new EDGEGUI();
            } else {
                return new EDGECLIImpl(mpi);
            }
        } catch (ParseException ex) {
            org.apache.log4j.Logger.getLogger(EDGEStart.class.getName(), new BundleLoggerFactory()).error(ex.getMessage());
            return null;
        }
    }

    public static EDGEUI getUI(String[] args) {
        return getUI(args, false);
    }

}
