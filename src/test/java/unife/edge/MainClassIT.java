/**
 *  This file is part of EDGE.
 *
 *  EDGE is a probabilistic parameter learner for OWL 2 ontologies.
 *  It learns the probabilistic values of the probabilistic axioms starting from a set of positive and negative axioms.
 *
 *  EDGE is a module of the LEAP system, but can be used as standalone.
 *
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org,
 *  but some components can be used as stand-alone.
 *
 *  LEAP and all its modules are free software; you can redistribute the and/or modify
 *  them under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  LEAP and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package unife.edge;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class MainClassIT {
    
    public MainClassIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of main method, of class MainClass.
     */
//    @Test
    public void testMain() {
        System.out.println("main");
        String[] args = {"-h"};
        MainClass.main(args);
    }
    
     /**
     * Test of main method, of class MainClass.
     */
    @Test
    public void testMain2() {
        System.out.println("main");
        String[] args = {"-file", "examples/DBPedia/input"};
        try {
            MainClass.main(args);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
}
