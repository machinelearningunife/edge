/**
 *  This file is part of EDGE.
 * 
 *  EDGE is a probabilistic parameter learner for OWL 2 ontologies. 
 *  It learns the probabilistic values of the probabilistic axioms starting from a set of positive and negative axioms. 
 * 
 *  EDGE can be used both as module and as standalone.
 *  
 *  EDGE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package unife.edge;

import java.util.Collections;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLAnnotationProperty;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.PrefixManager;
import org.semanticweb.owlapi.util.DefaultPrefixManager;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class EDGETest {
    
    EDGE instance;
    OWLOntologyManager manager;
    private static final String PREFIX = "https://sites.google.com/a/unife.it/ml/bundle";
    private static final Logger logger = Logger.getLogger(EDGETest.class.getName());
    
    public EDGETest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        instance = new EDGE();
        manager = OWLManager.createOWLOntologyManager();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of computeLearning method, of class EDGE.
     */
    @Test
    public void testComputeLearning() {
    }

    /**
     * Test of getLearnedOntology method, of class EDGE.
     */
    @Test
    public void testGetLearnedOntology() throws OWLOntologyCreationException {
        OWLOntology ontology = manager.createOntology(IRI.create(PREFIX));
        OWLOntologyManager manager2 = OWLManager.createOWLOntologyManager();
        OWLOntology probOntology = manager2.createOntology(IRI.create(PREFIX));
        OWLDataFactory factory = manager.getOWLDataFactory();
        PrefixManager pm = new DefaultPrefixManager(PREFIX);
        
        OWLClass classA = factory.getOWLClass(":A", pm);
        OWLClass classB = factory.getOWLClass(":B", pm);
        OWLClass classC = factory.getOWLClass(":C", pm);
        OWLClass classD = factory.getOWLClass(":D", pm);
        
        OWLAxiom subClassAxiom1 = factory.getOWLSubClassOfAxiom(classA, classB);
        OWLAnnotationProperty prop = factory.getOWLAnnotationProperty(IRI.create(PREFIX + "#probability"));
        OWLAnnotation ann = factory.getOWLAnnotation(prop, factory.getOWLLiteral(0.2));
        OWLAxiom subClassAxiom2 = factory.getOWLSubClassOfAxiom(classC, classD, Collections.singleton(ann));
        
        manager.addAxiom(ontology, subClassAxiom1);
        manager.addAxiom(ontology, subClassAxiom2);
        manager2.addAxiom(probOntology, subClassAxiom2);
        
        instance.setOntology(ontology);
        instance.setProbOntology(probOntology);
        
        
        // simulo l'aggiornamento dei parametri
        ann = factory.getOWLAnnotation(prop, factory.getOWLLiteral(0.5));
        OWLAxiom subClassAxiom3 = factory.getOWLSubClassOfAxiom(classC, classD, Collections.singleton(ann));
        manager.removeAxiom(probOntology, subClassAxiom2);
        manager.addAxiom(probOntology, subClassAxiom3);
        instance.setProbOntology(probOntology);
        instance.setMerge(true);
        OWLOntology result = instance.getLearnedOntology();
        
    }   

    /**
     * Test of setMaxExplanations method, of class EDGE.
     */
    @Test
    public void testSetMaxExplanations() {
    }

    /**
     * Test of setTimeOut method, of class EDGE.
     */
    @Test
    public void testSetTimeOut() {
    }

    /**
     * Test of setBDDFStartNodeNum method, of class EDGE.
     */
    @Test
    public void testSetBDDFStartNodeNum() {
    }

    /**
     * Test of setBDDFStartCache method, of class EDGE.
     */
    @Test
    public void testSetBDDFStartCache() {
    }

    /**
     * Test of setOntology method, of class EDGE.
     */
    @Test
    public void testSetOntology() {
    }

    /**
     * Test of setProbOntology method, of class EDGE.
     */
    @Test
    public void testSetProbOntology() {
    }

    /**
     * Test of setPositiveExamples method, of class EDGE.
     */
    @Test
    public void testSetPositiveExamples() {
    }

    /**
     * Test of setNegativeExamples method, of class EDGE.
     */
    @Test
    public void testSetNegativeExamples() {
    }

    /**
     * Test of setSeed method, of class EDGE.
     */
    @Test
    public void testSetSeed() {
    }

    /**
     * Test of setRandomize method, of class EDGE.
     */
    @Test
    public void testSetRandomize() {
    }

    /**
     * Test of setRandomizeAll method, of class EDGE.
     */
    @Test
    public void testSetRandomizeAll() {
    }

    /**
     * Test of setTest method, of class EDGE.
     */
    @Test
    public void testSetTest() {
    }

    /**
     * Test of setShowAll method, of class EDGE.
     */
    @Test
    public void testSetShowAll_boolean() {
    }

    /**
     * Test of setShowAll method, of class EDGE.
     */
    @Test
    public void testSetShowAll_char() {
    }

    /**
     * Test of setLearnParametersFromScratch method, of class EDGE.
     */
    @Test
    public void testSetLearnParametersFromScratch() {
    }

    /**
     * Test of setDiffLL method, of class EDGE.
     */
    @Test
    public void testSetDiffLL() {
    }

    /**
     * Test of setRatioLL method, of class EDGE.
     */
    @Test
    public void testSetRatioLL() {
    }

    /**
     * Test of setIter method, of class EDGE.
     */
    @Test
    public void testSetIter() {
    }

    /**
     * Test of setMerge method, of class EDGE.
     */
    @Test
    public void testSetMerge() {
    }
    
}
